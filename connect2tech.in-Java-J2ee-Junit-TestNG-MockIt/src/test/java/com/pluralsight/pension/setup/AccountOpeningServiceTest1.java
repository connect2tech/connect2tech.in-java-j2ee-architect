package com.pluralsight.pension.setup;

import java.io.IOException;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import com.pluralsight.pension.AccountRepository;

/**
 * In order to test AccountOpeningService we need BackgroundCheckService,
 * ReferenceIdsManager, AccountRepository.
 * 
 * But development is going on and they are not ready.
 */
public class AccountOpeningServiceTest1 {

	private AccountOpeningService underTest;
	private IBackgroundCheckService backgroundCheckService = null;
	private IReferenceIdsManager referenceIdsManager = null;
	private AccountRepository accountRepository = null;
	private IAccountOpeningEventPublisher publisher = null;

	@Before
	public void setUp() {
		underTest = new AccountOpeningService(backgroundCheckService, referenceIdsManager, accountRepository,
				publisher);
	}

	@Test
	public void shouldOpenAccount() throws IOException {
		final AccountOpeningStatus accountOpeningStatus = underTest.openAccount("John", "Smith", "123XYZ9",
				LocalDate.of(1990, 1, 1));
		org.junit.Assert.assertEquals(AccountOpeningStatus.OPENED, accountOpeningStatus);

	}
}