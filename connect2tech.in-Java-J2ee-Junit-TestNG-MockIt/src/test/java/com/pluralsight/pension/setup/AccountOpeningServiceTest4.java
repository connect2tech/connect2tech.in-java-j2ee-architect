package com.pluralsight.pension.setup;

import static org.mockito.Mockito.*;
import static org.mockito.ArgumentMatcher.*;

import java.io.IOException;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import com.pluralsight.pension.AccountRepository;

/**
 * In order to test AccountOpeningService we need BackgroundCheckService,
 * ReferenceIdsManager, AccountRepository.
 * 
 * But development is going on and they are not ready. We have the interface
 * name and method names. We will mock the required services.
 */
public class AccountOpeningServiceTest4 {

	private static final String FIRST_NAME = "John";
	private static final String LAST_NAME = "Smith";
	private static final LocalDate DOB = LocalDate.of(1990, 1, 1);
	private static final String TAX_ID = "123XYZ9";
	private AccountOpeningService underTest;
	private IBackgroundCheckService backgroundCheckService = mock(IBackgroundCheckService.class);
	private IReferenceIdsManager referenceIdsManager = mock(IReferenceIdsManager.class);
	private AccountRepository accountRepository = mock(AccountRepository.class);
	private IAccountOpeningEventPublisher publisher = mock(IAccountOpeningEventPublisher.class);

	@Before
	public void setUp() {
		underTest = new AccountOpeningService(backgroundCheckService, referenceIdsManager, accountRepository,publisher);
	}

	@Test
	public void shouldOpenAccount() throws IOException {
		when(backgroundCheckService.confirm(FIRST_NAME, LAST_NAME, TAX_ID, DOB))
				.thenReturn(new BackgroundCheckResults("something_not_unacceptable", 100));

		when(referenceIdsManager.obtainId(eq(FIRST_NAME), eq(LAST_NAME), eq(TAX_ID), eq(DOB))).thenReturn("some_id");

		final AccountOpeningStatus accountOpeningStatus = underTest.openAccount("John", "Smith", "123XYZ9", DOB);

		org.junit.Assert.assertEquals(AccountOpeningStatus.OPENED, accountOpeningStatus);

	}
}