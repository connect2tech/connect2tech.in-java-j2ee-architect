package com.pluralsight.pension.setup;

import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import com.pluralsight.pension.AccountRepository;

/**
 * In order to test AccountOpeningService we need BackgroundCheckService,
 * ReferenceIdsManager, AccountRepository.
 * 
 * But development is going on and they are not ready. We have the interface
 * name and method names. We will mock the required services. In this particular
 * case, I have just created mock for interfaces.
 */
public class AccountOpeningServiceTest2 {

	private AccountOpeningService underTest;
	private IBackgroundCheckService backgroundCheckService = mock(IBackgroundCheckService.class);
	private IReferenceIdsManager referenceIdsManager = mock(IReferenceIdsManager.class);
	private AccountRepository accountRepository = mock(AccountRepository.class);
	private IAccountOpeningEventPublisher publisher = mock(IAccountOpeningEventPublisher.class);
	
	@Before
	public void setUp() {
		System.out.println("backgroundCheckService=>" + backgroundCheckService);
		underTest = new AccountOpeningService(backgroundCheckService, referenceIdsManager, accountRepository,publisher);
	}

	@Test
	public void shouldOpenAccount() throws IOException {
		final AccountOpeningStatus accountOpeningStatus = underTest.openAccount("John", "Smith", "123XYZ9",
				LocalDate.of(1990, 1, 1));
		org.junit.Assert.assertEquals(AccountOpeningStatus.OPENED, accountOpeningStatus);

	}
}