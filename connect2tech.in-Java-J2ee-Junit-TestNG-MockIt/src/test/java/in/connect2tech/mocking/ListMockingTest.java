package in.connect2tech.mocking;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.hamcrest.CoreMatchers.containsString;

import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

public class ListMockingTest {

	@Test
	public void letsMockListSize() {
		List list = mock(List.class);
		Mockito.when(list.size()).thenReturn(10);
		assertEquals(10, list.size());
	}

	@Test
	public void letsMockListSizeWithMultipleReturnValues() {
		List list = mock(List.class);
		Mockito.when(list.size()).thenReturn(10).thenReturn(20);
		assertEquals(10, list.size()); // First Call
		assertEquals(20, list.size()); // Second Call
	}

	@Test
	public void letsMockListGet() {
		List<String> list = mock(List.class);
		Mockito.when(list.get(0)).thenReturn("connect2tech");
		assertEquals("connect2tech", list.get(0));
		assertNull(list.get(1));
	}

	@Test
	public void letsMockListGetWithAny() {
		List<String> list = mock(List.class);
		Mockito.when(list.get(Mockito.anyInt())).thenReturn("connect2tech");
		// If you are using argument matchers, all arguments
		// have to be provided by matchers.
		assertEquals("connect2tech", list.get(0));
		assertEquals("connect2tech", list.get(1));
	}

	@Test(expected = RuntimeException.class)
	public void letsMockThrowException() {
		List<String> list = mock(List.class);
		Mockito.when(list.get(Mockito.anyInt())).thenThrow(RuntimeException.class);
		// If you are using argument matchers, all arguments
		// have to be provided by matchers.
		assertEquals("connect2tech", list.get(0));
	}

	@Test
	public void letsMockListGetWithAny_BDD() {
		// Given
		List<String> list = mock(List.class);
		given(list.get(Mockito.anyInt())).willReturn("connect2tech");

		// When
		String val = list.get(0);

		// Then
		assertThat(val, containsString("connect2tech"));
	}

}