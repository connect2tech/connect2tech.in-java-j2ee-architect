package in.connect2tech.testng.parallel;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestNgClass2 {

	Singleton singleton;
	List<String> list2 = new ArrayList<String>();

	@BeforeClass
	public void beforeClass() {

		list2.add("Java");
		list2.add("Python");

		System.out.println(
				"in.connect2tech.testng.parallel.TestNgClass2.beforeClass()::" + Thread.currentThread().getId());
		singleton = Singleton.getSingleton();
		list2 = singleton.getList();
		System.out.println("list2::" + list2);
	}

	@Test()
	public void method1() {
		System.out.println("in.connect2tech.testng.parallel.TestNgClass2.method1()::" + Thread.currentThread().getId());
	}

}
