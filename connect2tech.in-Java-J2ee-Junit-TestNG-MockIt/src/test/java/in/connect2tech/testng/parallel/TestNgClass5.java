package in.connect2tech.testng.parallel;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestNgClass5 {

	Singleton singleton;
	List<String> list1 = new ArrayList<String>();

	@Test()
	public void method1() {
		Singleton.displayThreadId();
		System.out.println("in.connect2tech.testng.parallel.TestNgClass5.method1()::" + Thread.currentThread().getId());
	}

}
