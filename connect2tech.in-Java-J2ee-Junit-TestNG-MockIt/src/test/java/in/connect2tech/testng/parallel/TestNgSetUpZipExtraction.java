package in.connect2tech.testng.parallel;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestNgSetUpZipExtraction {

	Singleton singleton;
	List<String> list1 = new ArrayList<String>();

	@BeforeClass
	public void beforeClass() {

		list1.add("Java");
		list1.add("Python");

		System.out.println("in.connect2tech.testng.parallel.TestNgSetUpZipExtraction.beforeClass()::"
				+ Thread.currentThread().getId());
		singleton = Singleton.getSingleton();
		singleton.setList(list1);
	}

	@Test()
	public void method1() {
		System.out.println("in.connect2tech.testng.parallel.TestNgSetUpZipExtraction.method1()::"
				+ Thread.currentThread().getId());
	}

}
