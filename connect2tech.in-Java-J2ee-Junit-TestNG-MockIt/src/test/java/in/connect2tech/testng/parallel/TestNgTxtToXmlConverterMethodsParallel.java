package in.connect2tech.testng.parallel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.Test;

public class TestNgTxtToXmlConverterMethodsParallel {
	
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
	
	@Test()
	public void txtToXml1() {
		Date date = new Date();
		System.out.println("in.connect2tech.testng.parallel.TestNgTxtToXmlConverterMethodsParallel.txtToXml1()::"+Thread.currentThread().getId()+ " , Time::"+dateFormat.format(date));
		Singleton.displayThreadId();
	}

	@Test()
	public void txtToXml2() {
		Date date = new Date();
		System.out.println("in.connect2tech.testng.parallel.TestNgTxtToXmlConverterMethodsParallel.txtToXml2()::"+Thread.currentThread().getId()+ " , Time::"+dateFormat.format(date));
		Singleton.displayThreadId();
	}

	@Test()
	public void txtToXml3() {
		Date date = new Date();
		System.out.println("in.connect2tech.testng.parallel.TestNgTxtToXmlConverterMethodsParallel.txtToXml3()::"+Thread.currentThread().getId()+ " , Time::"+dateFormat.format(date));
		Singleton.displayThreadId();
	}

	@Test()
	public void txtToXml4() {
		Date date = new Date();
		System.out.println("in.connect2tech.testng.parallel.TestNgTxtToXmlConverterMethodsParallel.txtToXml4()::"+Thread.currentThread().getId()+ " , Time::"+dateFormat.format(date));
		Singleton.displayThreadId();
	}

	@Test()
	public void txtToXml5() {
		Date date = new Date();
		System.out.println("in.connect2tech.testng.parallel.TestNgTxtToXmlConverterMethodsParallel.txtToXml5()::"+Thread.currentThread().getId()+ " , Time::"+dateFormat.format(date));
		Singleton.displayThreadId();
	}
}
