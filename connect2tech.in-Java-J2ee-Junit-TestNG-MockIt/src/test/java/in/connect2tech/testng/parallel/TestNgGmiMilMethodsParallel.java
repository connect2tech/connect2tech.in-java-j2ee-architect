package in.connect2tech.testng.parallel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.Test;

public class TestNgGmiMilMethodsParallel {
	
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
	
	@Test()
	public void mil1() {
		Date date = new Date();
		System.out.println("in.connect2tech.testng.parallel.TestNgGmiMilMethodsParallel.mil1()::"+Thread.currentThread().getId()+ " , Time::"+dateFormat.format(date));
		Singleton.displayThreadId();
	}

	@Test()
	public void mil2() {
		Date date = new Date();
		System.out.println("in.connect2tech.testng.parallel.TestNgGmiMilMethodsParallel.mil2()::"+Thread.currentThread().getId()+ " , Time::"+dateFormat.format(date));
		Singleton.displayThreadId();
	}

	@Test()
	public void mil3() {
		Date date = new Date();
		System.out.println("in.connect2tech.testng.parallel.TestNgGmiMilMethodsParallel.mil3()::"+Thread.currentThread().getId()+ " , Time::"+dateFormat.format(date));
		Singleton.displayThreadId();
	}

	@Test()
	public void mil4() {
		Date date = new Date();
		System.out.println("in.connect2tech.testng.parallel.TestNgGmiMilMethodsParallel.mil4()::"+Thread.currentThread().getId()+ " , Time::"+dateFormat.format(date));
		Singleton.displayThreadId();
	}

	@Test()
	public void mil5() {
		Date date = new Date();
		System.out.println("in.connect2tech.testng.parallel.TestNgGmiMilMethodsParallel.mil5()::"+Thread.currentThread().getId()+ " , Time::"+dateFormat.format(date));
		Singleton.displayThreadId();
	}
}
