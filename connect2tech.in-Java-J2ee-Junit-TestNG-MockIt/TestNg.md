**Problem Statement**

* How to share data between 2 TestNg classes
  * Singleton. **Ok. No Parallelism**
  * The classes are executed in the order they appear. Set data in singleton in TestNgClass1, and then get from TestNgClass2.
  * TesNgClass.xml
```
<suite name="Parallelism Suite" verbose="1">
	<test name="Parallelism Test">
		<classes>
			<class name="in.connect2tech.testng.parallel.TestNgClass1" />
			<class name="in.connect2tech.testng.parallel.TestNgClass2" />
		</classes>
	</test>
</suite>
```
 
 
**3 - testng.xml**

* By default, TestNG will run your tests in the order they are found in the XML file. If you want the classes and methods listed in this file to be run in an unpredictable order, set the preserve-order attribute to false

---

```
<suite name="My suite" parallel="methods" thread-count="5">
<suite name="My suite" parallel="tests" thread-count="5">
<suite name="My suite" parallel="classes" thread-count="5">
<suite name="My suite" parallel="instances" thread-count="5">
```