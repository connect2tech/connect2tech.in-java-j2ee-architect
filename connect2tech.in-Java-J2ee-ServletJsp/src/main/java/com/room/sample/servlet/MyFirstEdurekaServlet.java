package com.room.sample.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyFirstEdurekaServlet extends HttpServlet {

	public void init(ServletConfig config) {
		System.out.println("Initialization...");
	}

	/*public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException {
		System.out.println("I am in service method...");
	}*/

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter pw = res.getWriter();

		// I am in do get...
		// This is MyFirstEdurekaServlet
		// fname=Milesh
		// lname=Patel
		
		String hiddenField = req.getParameter("uname");
		
		pw.println("hiddenField===="+hiddenField);

		String fname = req.getParameter("firstname");
		String lname = req.getParameter("lastname");

		pw.println("I am in do get...");
		pw.println("This is MyFirstEdurekaServlet");
		pw.println("fname=" + fname);
		pw.println("lname=" + lname);

	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter pw = res.getWriter();
		pw.println("I am in do post...");
		pw.println("This is MyFirstEdurekaServlet");
	}

}
