package com.room.sample.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyBankingServlet extends HttpServlet {

	public void init() throws ServletException {
		
		System.out.println("I am getting initalized....");

	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, java.io.IOException {
		
		PrintWriter pw = resp.getWriter();
		pw.print("Welcome to first servlet get method....");
		
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, java.io.IOException {
		
		PrintWriter pw = resp.getWriter();
		pw.print("Welcome to first servlet post method....");
		
	}

}
