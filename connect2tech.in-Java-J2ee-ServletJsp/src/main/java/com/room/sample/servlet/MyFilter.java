package com.room.sample.servlet;

import java.text.SimpleDateFormat;
import java.io.*;
import java.util.Properties;
import java.net.URL;

import javax.servlet.*;

public class MyFilter implements Filter {

	public void init(FilterConfig arg0) throws ServletException {
	}

	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {

		PrintWriter out = resp.getWriter();
		// out.print("filter is invoked before");
		ServletContext context = req.getServletContext();

		//getModifiedTime(out, context);

		readProperties(out, context);

		chain.doFilter(req, resp);// sends request to next resource

		// out.print("filter is invoked after");
	}

	public void destroy() {
	}

	private void getModifiedTime(PrintWriter out, ServletContext context) {

		// Properties prop = new Properties();
		//
		// InputStream inputStream =
		// Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties");
		// //URL url = ClassLoader.getSystemResource("");
		// try {
		// out.println("inputStream==================" + prop);
		// prop.load(inputStream);
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		//
		// out.println("prop==================" + prop);

		File file = new File("config.properties");

		long lTime = file.lastModified();
		out.println("Before Format : " + lTime);

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		// String modified = sdf.format(file.lastModified());

		// out.println("After Format : " + modified);

		String modifiedContext = (String) context.getAttribute("modified") + "";

		out.println("modifiedContext : " + modifiedContext);

		if (modifiedContext != null && !modifiedContext.equals("null")) {

			if (!modifiedContext.equals(lTime + "")) {
				out.println("The values have changed...Reload...");
			}
		}

		context.setAttribute("modified", lTime + "");

	}

	private void readProperties(PrintWriter out, ServletContext context) {
		Properties prop = new Properties();
		InputStream input = null;
		
		

		try {

			/*
			 * ClassLoader classLoader =
			 * Thread.currentThread().getContextClassLoader(); InputStream is =
			 * classLoader.getResourceAsStream("foo.properties"); // ...
			 * Properties properties = new Properties(); properties.load(is);
			 */

			input = context.getResourceAsStream("/config.properties");
			

			// load a properties file
			prop.load(input);

			out.println("-----------------------------------------");
			// get the property value and print it out
			out.println(prop.getProperty("database"));
			out.println(prop.getProperty("dbuser"));
			out.println(prop.getProperty("dbpassword"));

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
}