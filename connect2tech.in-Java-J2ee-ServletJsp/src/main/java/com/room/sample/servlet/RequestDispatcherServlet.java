package com.room.sample.servlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.*;
import javax.servlet.http.*;


public class RequestDispatcherServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		PrintWriter pw = response.getWriter();
		
		String fname = request.getParameter("firstname");
		
		//when user is checking out, see if not logged in, then redirect to login page.
		if(fname.equals("Chatura")){
			 RequestDispatcher rd1 = request.getRequestDispatcher("Page1.html");
			 rd1.forward(request, response);
		}else{
			 //redirect to payment page.
			 RequestDispatcher rd2 = request.getRequestDispatcher("MM");
			 rd2.forward(request, response);
		}
		
	}

}
