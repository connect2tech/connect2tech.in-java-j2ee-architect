package com.room.sample.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter pw = res.getWriter();
		
		String fname = req.getParameter("firstname");
		String lname = req.getParameter("lastname");
		
		pw.println("fname="+fname);
		pw.println("lname="+lname);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter pw = res.getWriter();
		pw.println("My First Servelet, post !!!");
		
		String fname = req.getParameter("firstname");
		String lname = req.getParameter("lastname");
		
		pw.println("fname="+fname);
		pw.println("lname="+lname);
	}

}
