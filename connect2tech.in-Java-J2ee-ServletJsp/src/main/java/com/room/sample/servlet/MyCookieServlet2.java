package com.room.sample.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class MyCookieServlet2 extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		try {

			response.setContentType("text/html");
			PrintWriter out = response.getWriter();

			Cookie ck[] = request.getCookies();
			
			for(int i=0;i<ck.length;i++){
				out.println("Cookie Name:: " + ck[i].getName());
				out.println("Cookie Value:: " + ck[i].getValue());
				
				out.println("Cookie Path:: " + ck[i].getPath());
			}
			
			out.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}
}