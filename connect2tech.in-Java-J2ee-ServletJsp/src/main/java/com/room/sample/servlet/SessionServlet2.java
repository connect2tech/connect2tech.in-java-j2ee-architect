package com.room.sample.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class SessionServlet2 extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		try {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();

			HttpSession session = request.getSession();
			String n = (String) session.getAttribute("course");
			out.println("Hello " + n);
			
			out.println("Session Id="+session.getId());

			out.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}