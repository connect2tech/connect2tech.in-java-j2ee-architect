package com.room.sample.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class HelloServlet extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String modifiedContext = (String) request.getServletContext().getAttribute("modified");

		ServletContext context  =getServletContext();
		
		
		
		out.print("<br>welcome to servlet<br>::");
		
		//readProperties(out, context);

	}
	
	
	private void readProperties(PrintWriter out, ServletContext context) {
		Properties prop = new Properties();
		InputStream input = null;
		
		

		try {

			/*
			 * ClassLoader classLoader =
			 * Thread.currentThread().getContextClassLoader(); InputStream is =
			 * classLoader.getResourceAsStream("foo.properties"); // ...
			 * Properties properties = new Properties(); properties.load(is);
			 */

			input = context.getResourceAsStream("/config.properties");
			

			// load a properties file
			prop.load(input);

			out.println("-----------------------------------------");
			// get the property value and print it out
			out.println(prop.getProperty("database"));
			out.println(prop.getProperty("dbuser"));
			out.println(prop.getProperty("dbpassword"));

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

}