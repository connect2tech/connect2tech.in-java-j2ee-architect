package com.room.sample.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class GenServlet extends GenericServlet {

	public void service(ServletRequest arg0, ServletResponse arg1) throws ServletException, IOException {

		PrintWriter pw = arg1.getWriter();
		pw.print("I am generic servlet...");
	}
	
}