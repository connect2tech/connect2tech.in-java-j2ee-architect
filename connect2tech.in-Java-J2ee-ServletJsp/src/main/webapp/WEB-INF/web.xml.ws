<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://java.sun.com/xml/ns/javaee" xmlns:web="http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd"
	xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd"
	id="WebApp_ID" version="2.5">
<session-config>
    	<!-- The HTTP session timeout, in minutes -->
        <session-timeout>5</session-timeout>
    </session-config>

    <!-- The listener that makes HTTP request and session context available -->
    
     <context-param>
           <param-name>contextClass</param-name>
           <param-value>mks.frame.security.fedsso.config.RESTEasySpringBeansPostProcessor.RESTEasyXmlWebApplicationContext</param-value>
     </context-param>
    <listener>
        <listener-class>org.springframework.web.context.request.RequestContextListener</listener-class>
    </listener>

    <!-- A listener that supports the RESTEasy-Spring integration -->
    
    <listener>
        <listener-class>org.jboss.resteasy.plugins.server.servlet.ResteasyBootstrap</listener-class>
    </listener>

    <!-- A listener that supports the RESTEasy-Spring context initialization -->
    
    <listener>
        <listener-class>mks.frame.security.fedsso.config.RESTEasySpringContextLoaderListener</listener-class>
    </listener>

    <!-- The filter that is responsible authentication and HTTP session management -->

    <filter>
	<filter-name>auth-filter</filter-name>
	<filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
    </filter>

    <filter-mapping>
        <filter-name>auth-filter</filter-name>
	<url-pattern>/*</url-pattern>
    </filter-mapping>

    <!-- The filter that logs requests and tracks statistics -->  

    <filter>
	<filter-name>logging-filter</filter-name>
	<filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
    </filter>

    <filter-mapping>
        <filter-name>logging-filter</filter-name>
	<url-pattern>/*</url-pattern>
    </filter-mapping>
    
    <!-- The filter that makes a CIServer connection and validates all its preconditions -->  
    
    <filter>
	<filter-name>ciconnect-filter</filter-name>
	<filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
    </filter>

    <filter-mapping>
        <filter-name>ciconnect-filter</filter-name>
	<url-pattern>/*</url-pattern>
    </filter-mapping>

    <!-- The filter that dispatches requests to RESTEasy -->

    <filter>
	<filter-name>resteasy</filter-name>
	<filter-class>org.jboss.resteasy.plugins.server.servlet.FilterDispatcher</filter-class>
    </filter>

    <filter-mapping>
        <filter-name>resteasy</filter-name>
	<url-pattern>/*</url-pattern>
    </filter-mapping>
    
    <!--  disable entity expansion  -->
    <context-param>
        <param-name> resteasy.document.expand.entity.references</param-name>
        <param-value>false</param-value>
    </context-param> 
</web-app>