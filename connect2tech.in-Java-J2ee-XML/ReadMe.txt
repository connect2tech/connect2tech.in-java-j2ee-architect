namespaces
----------
file: namespaces.xml
https://www.w3schools.com/xml/xml_namespaces.asp

namespaces.xml



DTD
---

https://www.w3schools.com/xml/xml_dtd.asp

inline_dtd.xml
note_dtd.xml
dtdFile.dtd

CDATA Vs PCDATA
---------------
https://stackoverflow.com/questions/918450/difference-between-pcdata-and-cdata-in-dtd

XPath & XSLT
------------
https://www.w3schools.com/xml/xml_xpath.asp
https://www.w3schools.com/xml/xml_xslt.asp
Book.xml

XSD
---
https://www.w3schools.com/xml/schema_intro.asp

shiporder.xml
shiporder.xsd


SAX Parser
------------
XMLParserSAX

> Start with explaining com.c2t.sax.MyHandler.java
> Explain each method, using the documentation: http://www.saxproject.org/apidoc/org/xml/sax/helpers/DefaultHandler.html


DOM Parser
---------

> com.c2t.dom.ReadXMLFileDom


XSL
---
XPathFile.xml
XPathFile.xsl


Validator
---------
https://www.journaldev.com/895/how-to-validate-xml-against-xsd-in-java

com.c2t.validator.XMLValidation

