package in.connect2tech.json_path.consumer;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import com.jayway.jsonpath.JsonPath;

import in.connect2tech.json.converter.JacksonObjectMapperJsonUtility;

public class JsonPathConsumer1 {

	@Test
	public void convertJsonFileToObjectGraphBytesTest() throws IOException {
		String json = JacksonObjectMapperJsonUtility
				.convertJsonFileToObjectGraphString("src/main/resources/book_store.json");
		System.out.println(json);
		Object store_book_category = JsonPath.read(json, "$.store.book[*].category");
		System.out.println(store_book_category.getClass());
		//System.out.println(authors);
	}

}
