package in.connect2tech.json.converter;

import java.io.IOException;
import java.text.ParseException;

import org.junit.Ignore;
import org.junit.Test;

import in.connect2tech.json.models.ModelsUtil;

public class JacksonObjectMapperExampleTest {

	@Test
	public void convertJsonFileToObjectGraphBytesTest() throws IOException {
		JacksonObjectMapperJsonUtility.convertJsonFileToObjectGraphBytes("employee.json");
	}
	
	@Test
	public void convertJsonFileToObjectGraphUserBytesTest() throws IOException {
		JacksonObjectMapperJsonUtility.convertJsonFileToObjectGraphUserBytes("User.Sample.json");
	}

	@Test
	public void convertJsonFileToMapBytesTest() throws IOException {
		JacksonObjectMapperJsonUtility.convertJsonFileToMapBytes("map.json");
	}

	@Test
	public void saveObjectGraphDateJsonFileTest() throws IOException {
		JacksonObjectMapperJsonUtility.saveObjectGraphDateJsonFile();
	}

	@Test
	public void readJsonDataUsingKeyTest() throws IOException {
		JacksonObjectMapperJsonUtility.readJsonDataUsingKey("employee.json");
	}

	@Test
	public void saveObjectGraphJsonStringTest() throws IOException {
		JacksonObjectMapperJsonUtility.saveObjectGraphJsonString();
	}

	@Test
	public void saveObjectGraphJsonFileTest() throws IOException {
		Object obj = ModelsUtil.createUser();
		JacksonObjectMapperJsonUtility.saveObjectGraphJsonFile(obj);
	}

	@Test
	public void readJsonEditJsonWriteJsonFileTest() throws IOException {
		JacksonObjectMapperJsonUtility.readJsonEditJsonWriteJsonFile("employee.json");
	}

	@Test
	public void convertJsonFileToObjectGraphFileObjectTest() throws IOException, ParseException {
		JacksonObjectMapperJsonUtility.convertJsonFileToObjectGraphFileObject("Student-dd-mm-yyyy.json");
	}

	@Test
	public void convertJsonFileToObjectGraphStringTest() throws IOException, ParseException {
		JacksonObjectMapperJsonUtility.convertJsonFileToObjectGraphString("Student-dd-mm-yyyy.json");
	}

	@Ignore
	@Test
	public void jsonStreamingApiExampleTest() throws IOException {
		throw new RuntimeException("Pending");
	}
}
