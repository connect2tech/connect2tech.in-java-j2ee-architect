package in.connect2tech.jenkov.utils;

import org.junit.Test;

public class JsonUtilObjectMapperTest {

	@Test
	public void readObjectFromJsonStringTest() {
		JsonObjectMapperUtil.convertJsonStringToJavaBean();
	}

	@Test
	public void readObjectFromJsonReaderTest() {
		JsonObjectMapperUtil.convertJsonStringReaderToJavaBean();
	}

	@Test
	public void readObjectFromJsonFileTest() {
		JsonObjectMapperUtil.convertJsonFileToJavaBean();
	}

	@Test
	public void readObjectFromJsonViaUrlTest() {
		JsonObjectMapperUtil.convertJsonUrlToJavaBean();
	} 

	@Test
	public void readObjectFromJsonInputStreamTest() {
		JsonObjectMapperUtil.convertJsonFileToJavaBeanUsingStreams();
	}

	@Test
	public void readObjectFromJsonByteArrayTest() {
		JsonObjectMapperUtil.convertJsonByteArrayToJavaBean();
	}

	@Test
	public void readObjectArrayFromJsonArrayStringTest() {
		JsonObjectMapperUtil.readObjectArrayFromJsonArrayString();
	}

	@Test
	public void readObjectListFromJsonArrayStringTest() {
		JsonObjectMapperUtil.readObjectListFromJsonArrayString();
	}

	@Test
	public void readMapfromJsonStringTest() {
		JsonObjectMapperUtil.readMapfromJsonString();
	}
	
	@Test
	public void ignoreUnknownJsonFieldsTest() {
		JsonObjectMapperUtil.ignoreUnknownJsonFields();
	}
	
	@Test
	public void failOnNullJsonValuesForPrimitiveTypesTest() {
		JsonObjectMapperUtil.failOnNullJsonValuesForPrimitiveTypes();
	}
}
