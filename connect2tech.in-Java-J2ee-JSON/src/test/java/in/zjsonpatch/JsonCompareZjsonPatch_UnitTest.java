package in.zjsonpatch;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.zjsonpatch.JsonDiff;

import in.connect2tech.json.converter.JacksonObjectMapperJsonUtility;

public class JsonCompareZjsonPatch_UnitTest {

	@Test
	public void compareJsonFiles() throws IOException {

		ObjectMapper jacksonObjectMapper = new ObjectMapper();
		String json1 = JacksonObjectMapperJsonUtility.convertJsonFileToObjectGraphString("File1.json");
		String json2 = JacksonObjectMapperJsonUtility.convertJsonFileToObjectGraphString("File2.json");

		JsonNode beforeNode = jacksonObjectMapper.readTree(json1);
		JsonNode afterNode = jacksonObjectMapper.readTree(json2);
		JsonNode patch = JsonDiff.asJson(beforeNode, afterNode);
		String diffs = patch.toString();
		System.out.println("diffs=>"+diffs);
	}
}
