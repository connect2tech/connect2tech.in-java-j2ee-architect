package in.connect2tech.json_schema_java;

import java.io.IOException;

import in.connect2tech.json.converter.JacksonObjectMapperJsonUtility;

public class Main {
	public static void main(String[] args) throws IOException {
		JacksonObjectMapperJsonUtility
				.convertJsonFileToObjectGraphBytesX("src/main/resources/schema/user/user.data.json");
	}
}
