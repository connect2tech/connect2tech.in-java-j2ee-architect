
package in.connect2tech.json_schema_java;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "first_name",
    "last_name",
    "birthday",
    "address"
})
public class SchemaUser {

    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("birthday")
    private String birthday;
    @JsonProperty("address")
    private SchemaAddress address;

    @JsonProperty("first_name")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("first_name")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("last_name")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("last_name")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty("birthday")
    public String getBirthday() {
        return birthday;
    }

    @JsonProperty("birthday")
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @JsonProperty("address")
    public SchemaAddress getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(SchemaAddress address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("firstName", firstName).append("lastName", lastName).append("birthday", birthday).append("address", address).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(birthday).append(firstName).append(lastName).append(address).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SchemaUser) == false) {
            return false;
        }
        SchemaUser rhs = ((SchemaUser) other);
        return new EqualsBuilder().append(birthday, rhs.birthday).append(firstName, rhs.firstName).append(lastName, rhs.lastName).append(address, rhs.address).isEquals();
    }

}
