package in.connect2tech.json.fundamentals.consuming_with_binding;

import static java.util.stream.Collectors.joining;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import in.connect2tech.json.fundamentals.common.Job;
import in.connect2tech.json.fundamentals.common.LoanDetails;

public class ImmutableLoanApplication
{
    private final String name;
    private final String purposeOfLoan;
    private final LoanDetails loanDetails;
    private final List<Job> jobs;

    @JsonCreator
    public ImmutableLoanApplication(
        @JsonProperty("name") final String name,
        @JsonProperty("purposeOfLoan") final String purposeOfLoan,
        @JsonProperty("loanDetails") final LoanDetails loanDetails,
        @JsonProperty("jobs") final List<Job> jobs)
    {
        this.name = name;
        this.purposeOfLoan = purposeOfLoan;
        this.loanDetails = loanDetails;
        this.jobs = Collections.unmodifiableList(new ArrayList<>(jobs));
    }

    public String getName()
    {
        return name;
    }

    public String getPurposeOfLoan()
    {
        return purposeOfLoan;
    }

    public LoanDetails getLoanDetails()
    {
        return loanDetails;
    }

    public List<Job> getJobs()
    {
        return jobs;
    }

    @Override
    public String toString()
    {
        return "ImmutableLoanApplication{" +
            "name='" + name + '\'' +
            ", purposeOfLoan='" + purposeOfLoan + '\'' +
            ", loanDetails=\n\t" + loanDetails +
            ", jobs=\n\t" + jobs.stream().map(Job::toString).collect(joining("\n\t\t","[\n\t\t","\n\t]")) +
            '}';
    }
}
