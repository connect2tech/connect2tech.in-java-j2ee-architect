package in.connect2tech.json.fundamentals.consuming_with_binding;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;


public class ConsumingWithBindingApi_1
{
    private static final File BANK_LOAN_FILE = new File("src/main/resources/bank_loan.json");

    public static void main(String[] args) throws IOException
    {
        final ObjectMapper objectMapper = new ObjectMapper();

        final BasicLoanApplication loanApplication =
            objectMapper.readValue(BANK_LOAN_FILE, BasicLoanApplication.class);

        System.out.println(loanApplication);
    }
}
