package in.connect2tech.json.fundamentals.consuming_with_binding;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class AdvancedConsumingWithBindingApi_2
{
    private static final File BANK_LOAN_FILE = new File("src/main/resources/bank_loan.json");

    public static void main(String[] args) throws IOException
    {
       /* final LocalDateDeserializer localDateDeserializer =
            new LocalDateDeserializer(DateTimeFormatter.ISO_LOCAL_DATE);

        final SimpleModule module = new SimpleModule()
            .addDeserializer(LocalDate.class, localDateDeserializer);

        final ObjectMapper mapper = new ObjectMapper()
            .registerModule(module);

        final ImmutableLoanApplication loanApplication =
            mapper.readValue(BANK_LOAN_FILE, ImmutableLoanApplication.class);
        System.out.println(loanApplication + "\n\n");*/

		final ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());
		final TransformedLoanApplication transformedLoanApplication = mapper.readValue(BANK_LOAN_FILE,
				TransformedLoanApplication.class);
		System.out.println(transformedLoanApplication);
	}
}
