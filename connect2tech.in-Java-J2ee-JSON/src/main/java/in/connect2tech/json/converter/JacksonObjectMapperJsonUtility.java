package in.connect2tech.json.converter;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

import in.connect2tech.json.models.Address;
import in.connect2tech.json.models.Employee;
import in.connect2tech.json.models.Student;
import in.connect2tech.json.models.Student2;
import in.connect2tech.json.models.User;
import in.connect2tech.json_schema_java.SchemaUser;

public class JacksonObjectMapperJsonUtility {

	/**
	 * 
	 * @param jsonFile - jsonFile
	 * @return
	 * @throws IOException
	 */
	public static String convertJsonFileToObjectGraphString(String jsonFile) throws IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		// read json file data to String
		byte[] jsonData = Files.readAllBytes(Paths.get(jsonFile));

		String json = new String(jsonData);

		//System.out.println("json=>" + json);

		return json;

	}

	public static void convertJsonFileToObjectGraphBytes(String jsonFile) throws IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		// read json file data to String
		byte[] jsonData = Files.readAllBytes(Paths.get(jsonFile));

		// convert json string to object
		Employee emp = objectMapper.readValue(jsonData, Employee.class);

		System.out.println("Employee Object\n" + emp);
	}
	
	public static void convertJsonFileToObjectGraphBytesX(String jsonFile) throws IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		// read json file data to String
		byte[] jsonData = Files.readAllBytes(Paths.get(jsonFile));

		// convert json string to object
		SchemaUser user = objectMapper.readValue(jsonData, SchemaUser.class);

		System.out.println("User Object\n" + user);
	}

	public static void convertJsonFileToObjectGraphUserBytes(String jsonFile) throws IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		// read json file data to String
		byte[] jsonData = Files.readAllBytes(Paths.get(jsonFile));

		// convert json string to object
		User user = objectMapper.readValue(jsonData, User.class);

		System.out.println("User Object\n" + user);
	}

	public static void convertJsonFileToMapBytes(String jsonFile)
			throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		byte[] mapData = Files.readAllBytes(Paths.get(jsonFile));
		Map<String, String> myMap = new HashMap<String, String>();

		myMap = objectMapper.readValue(mapData, HashMap.class);
		System.out.println("Map is: " + myMap);

		// another way
		myMap = objectMapper.readValue(mapData, new TypeReference<HashMap<String, String>>() {
		});
		System.out.println("Map using TypeReference: " + myMap);
	}

	public static void readJsonDataUsingKey(String jsonFile) throws IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		// read json file data to String
		byte[] jsonData = Files.readAllBytes(Paths.get("employee.json"));

		// read JSON like DOM Parser
		JsonNode rootNode = objectMapper.readTree(jsonData);
		JsonNode idNode = rootNode.path("id");
		System.out.println("id = " + idNode.asInt());

		JsonNode phoneNosNode = rootNode.path("phoneNumbers");
		Iterator<JsonNode> elements = phoneNosNode.elements();
		while (elements.hasNext()) {
			JsonNode phone = elements.next();
			System.out.println("Phone No = " + phone.asLong());
		}
	}

	public static void readJsonEditJsonWriteJsonFile(String jsonFile) throws IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		byte[] jsonData = Files.readAllBytes(Paths.get(jsonFile));

		// create JsonNode
		JsonNode rootNode = objectMapper.readTree(jsonData);

		// update JSON data
		((ObjectNode) rootNode).put("id", 500);
		// add new key value
		((ObjectNode) rootNode).put("test", "test value");
		// remove existing key
		((ObjectNode) rootNode).remove("role");
		((ObjectNode) rootNode).remove("properties");
		// objectMapper.writeValue(new File("updated_emp.json"), rootNode);
		objectMapper.writeValue(new File("updated_emp.json"), rootNode);
	}

	public static void saveObjectGraphJsonString() throws JsonGenerationException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		// convert Object to json string
		Employee emp1 = createEmployee();
		// configure Object mapper for pretty print
		objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);

		// writing to console, can write to any output stream such as file
		StringWriter stringEmp = new StringWriter();
		objectMapper.writeValue(stringEmp, emp1);
		System.out.println("Employee JSON is\n" + stringEmp);

	}

	public static void saveObjectGraphJsonFile(Object obj)
			throws JsonGenerationException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		// convert Object to json string
		// configure Object mapper for pretty print
		objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);

		// writing to console, can write to any output stream such as file
		objectMapper.writeValue(new File("User-" + new Random().nextInt(1000) + ".json"), obj);

	}

	public static void saveObjectGraphDateJsonFile() throws JsonGenerationException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		// convert Object to json string
		Student student = createStudent();
		// configure Object mapper for pretty print
		objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);

		// writing to console, can write to any output stream such as file
		objectMapper.writeValue(new File("Student-" + new Random().nextInt(1000) + ".json"), student);

	}

	public static void convertJsonFileToObjectGraphFileObject(String jsonFile) throws IOException, ParseException {

		ObjectMapper objectMapper = new ObjectMapper();
		Student2 student = objectMapper.readValue(new File(jsonFile), Student2.class);

		/*
		 * SimpleDateFormat df = new SimpleDateFormat("dd-mm-yyyy"); Date date =
		 * df.parse(student.getJoinDate());
		 * 
		 * objectMapper.setDateFormat(df);
		 * System.out.println(objectMapper.writeValueAsString(date));
		 */
		System.out.println(student.getJoinDate());

	}

	public static Employee createEmployee() {

		Employee emp = new Employee();
		emp.setId(100);
		emp.setName("David");
		emp.setPermanent(false);
		emp.setPhoneNumbers(new long[] { 123456, 987654 });
		emp.setRole("Manager");

		Address add = new Address();
		add.setCity("Bangalore");
		add.setStreet("BTM 1st Stage");
		add.setZipcode(560100);
		emp.setAddress(add);

		List<String> cities = new ArrayList<String>();
		cities.add("Los Angeles");
		cities.add("New York");
		emp.setCities(cities);

		Map<String, String> props = new HashMap<String, String>();
		props.put("salary", "1000 Rs");
		props.put("age", "28 years");
		emp.setProperties(props);

		return emp;
	}

	private static Student createStudent() {

		Student student = new Student();

		student.setDesignation("Architect");
		student.setGender("male");
		student.setJoinDate(new Date());
		student.setName("NC");
		student.setNo(10);

		return student;
	}

}