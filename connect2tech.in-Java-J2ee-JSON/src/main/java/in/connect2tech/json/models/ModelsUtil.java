package in.connect2tech.json.models;

public class ModelsUtil {
	public static User createUser() {

		User user = new User();

		user.setPostOfficeBox("PO, Pune");
		user.setExtendedAddress("Wakad, Pune");
		user.setStreetAddress("Wakad, Street, Pune");
		user.setLocality("Pink City, Pune");
		user.setRegion("PCMC region");
		user.setPostalCode("411057");
		user.setCountryName("India");
		
		return user;
	}
}
