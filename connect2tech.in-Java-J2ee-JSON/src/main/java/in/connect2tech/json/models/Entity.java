package in.connect2tech.json.models;

import java.io.IOException;

import com.jayway.jsonpath.JsonPath;

import in.connect2tech.json.converter.JacksonObjectMapperJsonUtility;

public class Entity {
	
	public static void main(String str[]) throws IOException {
		new Entity().setField1("");
	}

	private String field1;

	public String getField1() {
		return field1;
	}

	public void setField1(String field1) throws IOException {

		String json = JacksonObjectMapperJsonUtility
				.convertJsonFileToObjectGraphString("src/main/resources/book_store.json");
		System.out.println(json);
		String store_book_category = JsonPath.read(json, "$.store.book[1].category");
		System.out.println(store_book_category);

		this.field1 = store_book_category;
	}

}
