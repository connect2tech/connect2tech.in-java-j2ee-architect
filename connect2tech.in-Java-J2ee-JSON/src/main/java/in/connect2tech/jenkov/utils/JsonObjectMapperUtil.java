package in.connect2tech.jenkov.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import in.connect2tech.jenkov.beans.Car;
import in.connect2tech.jenkov.beans.CarDeserializer;

public class JsonObjectMapperUtil {

	public static void convertJsonStringToJavaBean() {
		ObjectMapper objectMapper = new ObjectMapper();

		String carJson = "{ \"brand\" : \"Mercedes\", \"doors\" : 5 }";

		try {
			Car car = objectMapper.readValue(carJson, Car.class);

			System.out.println("car brand = " + car.getBrand());
			System.out.println("car doors = " + car.getDoors());
			System.out.println("car::" + car.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void convertJsonStringReaderToJavaBean() {
		ObjectMapper objectMapper = new ObjectMapper();

		String carJson = "{ \"brand\" : \"Mercedes\", \"doors\" : 5 }";

		Reader reader = new StringReader(carJson);

		try {
			Car car = objectMapper.readValue(reader, Car.class);

			System.out.println("car brand = " + car.getBrand());
			System.out.println("car doors = " + car.getDoors());
			System.out.println("car::" + car.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void convertJsonFileToJavaBean() {
		ObjectMapper objectMapper = new ObjectMapper();

		File file = new File("car.json");

		try {
			Car car = objectMapper.readValue(file, Car.class);
			System.out.println("car::" + car.toString());
		} catch (IOException e) {
			System.out.println("e=>" + e);
		}

	}

	public static void convertJsonUrlToJavaBean() {
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			URL url = new URL(
					"file:///D:/naresh.chaurasia/Java-Architect/connect2tech.in-Java-J2ee-Architect/connect2tech.in-Java-J2ee-JSON/car.json");
			Car car = objectMapper.readValue(url, Car.class);
			System.out.println("car::" + car.toString());
		} catch (Exception e) {
			System.out.println("e=>" + e);
		}

	}

	public static void convertJsonFileToJavaBeanUsingStreams() {
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			InputStream input = new FileInputStream("car.json");
			Car car = objectMapper.readValue(input, Car.class);
			System.out.println("car::" + car.toString());
		} catch (Exception e) {
			System.out.println("e=>" + e);
		}

	}

	public static void convertJsonByteArrayToJavaBean() {
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			String carJson = "{ \"brand\" : \"Mercedes\", \"doors\" : 5 }";

			byte[] bytes = carJson.getBytes("UTF-8");

			Car car = objectMapper.readValue(bytes, Car.class);
			System.out.println("car::" + car.toString());
		} catch (Exception e) {
			System.out.println("e=>" + e);
		}
	}

	public static void readObjectArrayFromJsonArrayString() {
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			String jsonArray = "[{\"brand\":\"ford\"}, {\"brand\":\"Fiat\"}]";

			Car[] cars2 = objectMapper.readValue(jsonArray, Car[].class);
			System.out.println("car::" + cars2[0].toString());
			System.out.println("car::" + cars2[1].toString());

		} catch (Exception e) {
			System.out.println("e=>" + e);
		}
	}

	public static void readObjectListFromJsonArrayString() {
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			String jsonArray = "[{\"brand\":\"ford\"}, {\"brand\":\"Fiat\"}]";

			List<Car> cars = objectMapper.readValue(jsonArray, new TypeReference<List<Car>>() {
			});
			System.out.println("car::" + cars.get(0).toString());
			System.out.println("car::" + cars.get(1).toString());

		} catch (Exception e) {
			System.out.println("e=>" + e);
		}
	}

	public static void readMapfromJsonString() {
		try {
			String jsonObject = "{\"brand\":\"ford\", \"doors\":5}";

			ObjectMapper objectMapper = new ObjectMapper();
			Map<String, Object> jsonMap = objectMapper.readValue(jsonObject, new TypeReference<Map<String, Object>>() {
			});
			System.out.println("jsonMap=>" + jsonMap);
		} catch (Exception e) {
			System.out.println("e=>" + e);
		}
	}

	public static void ignoreUnknownJsonFields() {

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		String carJson = "{ \"brand\" : \"Mercedes\", \"doors\" : 5 , \"windows\" : 5 }";

		Reader reader = new StringReader(carJson);

		try {
			Car car = objectMapper.readValue(reader, Car.class);

			System.out.println("car brand = " + car.getBrand());
			System.out.println("car doors = " + car.getDoors());
			System.out.println("car::" + car.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void failOnNullJsonValuesForPrimitiveTypes() {

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);

		String carJson = "{ \"brand\" : \"Mercedes\", \"doors\" : null }";

		Reader reader = new StringReader(carJson);

		try {
			Car car = objectMapper.readValue(reader, Car.class);

			System.out.println("car brand = " + car.getBrand());
			System.out.println("car doors = " + car.getDoors());
			System.out.println("car::" + car.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void customDeserializer() {
		try {
			String json = "{ \"brand\" : \"Ford\", \"doors\" : 6 }";

			SimpleModule module = new SimpleModule("CarDeserializer", new Version(3, 1, 8, null, null, null));
			module.addDeserializer(Car.class, new CarDeserializer(Car.class));

			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(module);

			Car car = mapper.readValue(json, Car.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
