package beans;

import java.lang.reflect.Field;

public class Employee {
	
	String name;
	Boolean b;
	
	public static void main(String[] args) {
		Field[] allFields = Employee.class.getDeclaredFields();
		
		for(int i=0;i<allFields.length;i++) {
			if(allFields[i].getType().equals(String.class)) {
				System.out.println("String");
			}else if (allFields[i].getType().equals(Boolean.class)) {
				System.out.println("Boolean");
			}
		}
	}

}
