# Java: JSON Fundamentals - By Richard Warburton

## Producing JSON

- ProducingWithStringFormats - This is not what i would want to use

- ProducingWithGenerator
```
JsonFactory jsonFactory = new JsonFactory();
JsonGenerator generator = jsonFactory.createGenerator(System.out);
generator.setPrettyPrinter(new DefaultPrettyPrinter());
generator.writeStartObject();
generator.writeStringField("name", loanApplication.getName());
generator.writeStringField("purposeOfLoan", loanApplication.getPurposeOfLoan());
generator.writeEndObject();
```

- ProducingWithBindingApi
```
ObjectMapper objectMapper = new ObjectMapper();
ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
String jsonString = objectWriter.writeValueAsString(loanApplication);
System.out.println(jsonString);
```

---

## Consuming JSON

- DOM API

```
private static final File BANK_LOAN_FILE = new File("src/main/resources/bank_loan.json");
final ObjectMapper objectMapper = new ObjectMapper();
JsonNode jsonNode = objectMapper.readTree(BANK_LOAN_FILE);
JsonNode loanDetails = jsonNode.get("loanDetails");
if(loanDetails!=null) {
	JsonNode amount = loanDetails.get("amount");
	System.out.println(amount.asDouble());
}
```

- ConsumingWithBindingApi
```
final ObjectMapper objectMapper = new ObjectMapper();
private static final File BANK_LOAN_FILE = 
new File("src/main/resources/bank_loan.json");
final BasicLoanApplication loanApplication = 
objectMapper.readValue(BANK_LOAN_FILE, BasicLoanApplication.class);
```

- ConsumingWithStreamingApi
```
final JsonFactory factory = new JsonFactory();
try (JsonParser parser = factory.createParser(BANK_LOAN_FILE))
{
JsonToken token;
while((token = parser.nextToken()) != null)
{
	
	if (token.isScalarValue())
	{
		final String currentName = parser.getCurrentName();
		if (currentName != null)
		{
			final String text = parser.getText();
			System.out.printf("%s: %s%n", currentName, text);
		}
	}
}
}
```

- Other Java JSON Libraries

- JSON APIs

---

**[jenkov java-json](http://tutorials.jenkov.com/java-json/index.html)**

**Jackson**
Jackson is a Java JSON API which provides several different ways to work with JSON. Jackson is one of the most popular Java JSON APIs out there. You can find Jackson here:

https://github.com/FasterXML/jackson

**Jackson contains 2 different JSON parsers:**
 - The Jackson ObjectMapper which parses JSON into custom Java objects, or into a Jackson specific tree structure (tree model).
 - The Jackson JsonParser which is Jackson's JSON pull parser, parsing JSON one token at a time.

**Jackson also contains two JSON generators:**
 - The Jackson ObjectMapper which can generate JSON from custom Java objects, or from a Jackson specific tree structure (tree model).
 - The Jackson JsonGenerator which can generate JSON one token at a time.
 
```
failOnNullJsonValuesForPrimitiveTypes
```

_So far reading string and converting it to Java Bean_

---

**[JournalDev java-json](https://www.journaldev.com/2324/jackson-json-java-parser-api-example-tutorial)**



