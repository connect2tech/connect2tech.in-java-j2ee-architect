package com.c2t.aop.proxy;

import java.lang.reflect.Proxy;

public class MainClass {
	public static void main(String[] args) {

		Example1 ex = new Example1();

		Basicfunc proxied = (Basicfunc) Proxy.newProxyInstance(MainClass.class.getClassLoader(),
				ex.getClass().getInterfaces(), new MyInvocationHandler(ex));
		proxied.method1();
	}
}