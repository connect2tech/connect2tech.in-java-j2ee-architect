package in.connect2tech.junit.spring.service;

import in.connect2tech.unit.obj.Order;

public interface SampleService {

	public String getOrderDescription();
	public String getOrderStringCode();
	public Order getOrder(int id);
	public Order createOrder(Order order);
	
	
}
