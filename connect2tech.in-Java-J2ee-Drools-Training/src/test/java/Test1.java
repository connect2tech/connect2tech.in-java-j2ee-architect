import org.junit.jupiter.api.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import connect2tech.drools.jewel.domain.Product;

public class Test1 {
	@Test
	public void test1() {

		KieContainer kieClasspathContainer = KieServices.Factory.get().getKieClasspathContainer();
		KieSession ksession = kieClasspathContainer.newKieSession("Jewel1");

		Product product = new Product();
		product.setType("gold");

		ksession.insert(product);
		ksession.fireAllRules();

		System.out.println("The discount for the product " + product.getType() + " is " + product.getDiscount());

		ksession.dispose();

	}
}
