Head First Object Oriented Analysis and Design
==============================================

## Chapter 1

* Values safety with Enum 


* This design is terrible! The  Inventory and Guitar classes  depend on each other too much, and I  cant see how this is an architecture  that you would ever be able to  build upon. We need some  restructuring. 

 
```
public Guitar search(Guitar searchGuitar) {
	for (Iterator i = guitars.iterator(); i.hasNext();) {
		Guitar guitar = (Guitar) i.next();
		// Ignore serial number since that's unique
		// Ignore price since that's unique
		String builder = searchGuitar.getBuilder();
		if ((builder != null) && (!builder.equals("")) && (!builder.equals(guitar.getBuilder())))
			continue;
		String model = searchGuitar.getModel();
		if ((model != null) && (!model.equals("")) && (!model.equals(guitar.getModel())))
			continue;
		String type = searchGuitar.getType();
		if ((type != null) && (!searchGuitar.equals("")) && (!type.equals(guitar.getType())))
			continue;
		String backWood = searchGuitar.getBackWood();
		if ((backWood != null) && (!backWood.equals("")) && (!backWood.equals(guitar.getBackWood())))
			continue;
		String topWood = searchGuitar.getTopWood();
		if ((topWood != null) && (!topWood.equals("")) && (!topWood.equals(guitar.getTopWood())))
			continue;
		return guitar;
	}
	return null;
}
```

* In the better-designed areas of Objectville, objects  are very particular about their jobs. Each object is  interested in doing its job, and **only its job**, to the best  of its ability. There's nothing a well-designed object  hates more than being used to do something that really  isn't its true purpose.
 
 
 * **Objects should do what their names indicate.**
 If an object is named Jet, it should probably takeOff()  and land(), but it shouldn't takeTicket() that's the job  of another object, and doesn't belong in Jet.  
 
 
* **Each object should represent a single concept.**  You don't want objects serving double or triple duty.  Avoid a Duck object that represents a real quacking  duck, a yellow plastic duck, and someone dropping  their head down to avoid getting hit by a baseball.  


* **Unused properties are a dead giveaway.**  If you've got an object that is being used with no-value  or null properties often, you've probably got an object  doing more than one job. If you rarely have values for a  certain property, why is that property part of the object?  Would there be a better object to use with just a subset  of those properties? 
	
	
* **Frank: Encapsulation** is also about breaking your app into logical  parts, and then keeping those parts separate. So just like you keep the  data in your classes separate from the rest of your app's behavior, we  can keep the generic properties of a guitar separate from the actual  Guitar object itself.


 * If you get stuck, think about the things that are  common between the Guitar object and what a  client would supply to the search() method.
	* **Here serial number and amount is not relevant, so encapsulation happens.**


* **Anytime you see  duplicate code, look for a  place to encapsulate! (You cannot have guitar specifications in guitarspecs and guitar)**


The idea behind encapsulation is to protect information  in one part of your application from the other parts of your  application. In its simplest form, you can protect the data  in your class from the rest of your app by making that data  private. **But sometimes the information might be an entire  set of properties like the details about a guitar or even  behavior like how a particular type of duck flies.  When you break that behavior out from a class, you can  change the behavior without the class having to change as  well.** So if you changed how properties were stored, you  wouldn't have to change your Guitar class at all, because  the properties are encapsulated away from Guitar.  That's the power of encapsulation: by breaking up the different  parts of your app, you can change one part without having to  change all the other parts. **In general, you should encapsulate  the parts of your app that might vary away from the parts that  will stay the same.** 
 
### Important !!!

* We’re adding a property to GuitarSpec, but we  have to change code in the Inventory class’s  search() method, as well as in the constructor  to the Guitar class. 


* So that’s the problem,  right? We shouldn’t have to  change code in Guitar and Inventory to  add a new property to the GuitarSpec  class. Can’t we just use more  encapsulation to fix this?


* That’s right—we need to  encapsulate the guitar  specifications and isolate  them from the rest of Rick’s  guitar search tool. 


* **The problem:**  
Adding a new property to GuitarSpec.java results in changes  to the code in Guitar.java and Inventory.java. The  application should be restructured so that adding properties to  GuitarSpec doesn’t affect the code in the rest of the application. 

 * **Your Task:**
   * Add a numStrings property and getNumStrings() method to  GuitarSpec.java.  
   * Modify Guitar.java so that the properties of GuitarSpec are  encapsulated away from the constructor of the class.  
   * Change the search() method in Inventory.java to delegate  comparing the two GuitarSpec objects to the GuitarSpec class,  instead of handling the comparison directly.  
   * Update FindGuitarTester.java to work with your new classes, and  make sure everything still works.  




## Chapter 2

- Requirements are things your  system must do to work correctly.  
- Your initial requirements usually  come from your customer.  
- To make sure you have a good  set of requirements, you should  develop use cases for your  system.  
- Use cases detail exactly what your  system should do.  
- A use case has a single goal, but  can have multiple paths to reach  that goal.  
- A good use case has a starting  and stopping condition, an  external initiator, and clear value  to the user.  
- A use case is simply a story about  how your system works.  
- You will have at least one use case  for each goal that your system  must accomplish.  
- After your use cases are complete,  you can refine and add to your  requirements.  
- A requirements list that makes all  your use cases possible is a good  set of requirements.  
- Your system must work in the real  world, not just when everything  goes as you expect it to.  
- When things go wrong, your  system must have alternate paths  to reach the system�s goals. 

## Chapter 3

## Chapter 4

- in.connect2tech.ooad.chapter4.randy
    - The problem with this approach is that the DogDoor is doing the Bark comparison.
    - What are the implications
        - The DogDoor is tightly coupled with Bark. 
        - If bark needs to be updated, the DogDoor is update with Bark.
