package com.pluralsight.pension.setup;

import com.pluralsight.pension.AccountRepository;

import java.io.IOException;
import java.time.LocalDate;

public class AccountOpeningService {

    private static final String UNACCEPTABLE_RISK_PROFILE = "HIGH";
    private IBackgroundCheckService backgroundCheckService;
    private IReferenceIdsManager referenceIdsManager;
    private AccountRepository accountRepository;
    IAccountOpeningEventPublisher publisher;


    public AccountOpeningService(IBackgroundCheckService backgroundCheckService,
                                 IReferenceIdsManager referenceIdsManager,
                                 AccountRepository accountRepository, IAccountOpeningEventPublisher publisher) {
        this.backgroundCheckService = backgroundCheckService;
        this.referenceIdsManager = referenceIdsManager;
        this.accountRepository = accountRepository;
        this.publisher = publisher;
    }


    public AccountOpeningStatus openAccount(String firstName, String lastName, String taxId, LocalDate dob)
            throws IOException {

        final BackgroundCheckResults backgroundCheckResults = backgroundCheckService.confirm(firstName,
                lastName,
                taxId,
                dob);

        if (backgroundCheckResults == null || backgroundCheckResults.getRiskProfile().equals(UNACCEPTABLE_RISK_PROFILE)) {
            return AccountOpeningStatus.DECLINED;
        } else {
            final String id = referenceIdsManager.obtainId(firstName, lastName, taxId, dob);
            if (id != null) {
                accountRepository.save(id, firstName, lastName, taxId, dob, backgroundCheckResults);
                publisher.notify(id);
                return AccountOpeningStatus.OPENED;
            } else {
                return AccountOpeningStatus.DECLINED;
            }
        }
    }
}