package com.pluralsight.pension.setup;

public interface IAccountOpeningEventPublisher {
	boolean notify(String accountId);
}