package in.connect2tech.testng.parallel;

import java.util.ArrayList;
import java.util.List;

public class ListDistribution {
	public static void main(String[] args) {

		ArrayList<String> main_arr_list = new ArrayList<String>();
		List<String> arrlist1 = new ArrayList<String>();
		List<String> arrlist2 = new ArrayList<String>();
		List<String> arrlist3 = new ArrayList<String>();
		List<String> arrlist4 = new ArrayList<String>();
		List<String> arrlist5 = new ArrayList<String>();

		// adding elements to arraylist : temporary assignment 52 elements =>
		// 1.txt....52.txt
		for (int i = 1; i <= 52; i++) {
			String filename = Integer.toString(i) + ".txt";
			main_arr_list.add(filename);
		}

		System.out.println("\n Names of all files in main arraylist - " + main_arr_list.size());
		System.out.println(main_arr_list);
		System.out.println("\n --------------------------------------------- ");

		// decide how many elements belong to single sublist
		int percount = main_arr_list.size() / 5;

		// remaining elements
		int count_remaining = main_arr_list.size() % 5;

		System.out.println("count_remaining==>" + count_remaining);

		// divide main_arr_list into 5 sub arraylist
		if (count_remaining == 0) // elements can be equally divided into sub arrlists
		{

			arrlist1 = main_arr_list.subList(0, percount);
			arrlist2 = main_arr_list.subList(percount, percount * 2);
			arrlist3 = main_arr_list.subList(percount * 2, percount * 3);
			arrlist4 = main_arr_list.subList(percount * 3, percount * 4);
			arrlist5 = main_arr_list.subList(percount * 4, percount * 5);

		} else { // first equally divide
			arrlist1 = main_arr_list.subList(0, percount);
			arrlist2 = main_arr_list.subList(percount, percount * 2);
			arrlist3 = main_arr_list.subList(percount * 2, percount * 3);
			arrlist4 = main_arr_list.subList(percount * 3, percount * 4);
			arrlist5 = main_arr_list.subList(percount * 4, percount * 5);

			int elements_completed = percount * 5 - 1; // 0-49 added

			for (int i = 1; i <= count_remaining; i++) {
				List<String> temp = new ArrayList<String>();

				if (i == 1) {
					arrlist1.add(main_arr_list.get(elements_completed + 1)); // index 50
				} else if (i == 2) {
					// if (main_arr_list.size() < elements_completed+ 1) {
					//temp.add(main_arr_list.get(elements_completed + 2));
					//temp.addAll(arrlist2);
					//arrlist2 = temp;
					//temp = null;
					// }
				} else if (i == 3) {
					// if (main_arr_list.size() < elements_completed + 2) {
					//arrlist3.add(main_arr_list.get(elements_completed + 3));
					// }
				} else {
					// if (main_arr_list.size() < elements_completed + 3) {
					//arrlist4.add(main_arr_list.get(elements_completed + 4));
					// }
				}
			}

		}

		// printing the result
		System.out.println("\n\n* Sub list 1 = \n" + arrlist1);
		System.out.println("\n\n* Sub list 2 = \n" + arrlist2);
		System.out.println("\n\n* Sub list 3 = \n" + arrlist3);
		System.out.println("\n\n* Sub list 4 = \n" + arrlist4);
		System.out.println("\n\n* Sub list 5 = \n" + arrlist5);

	}
}
