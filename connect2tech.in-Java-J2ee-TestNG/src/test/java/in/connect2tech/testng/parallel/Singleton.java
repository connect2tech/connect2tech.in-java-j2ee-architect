package in.connect2tech.testng.parallel;

import java.util.List;

public class Singleton {

	static Singleton singleton;

	List<String> list;

	private Singleton() {
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public static void setSingleton(Singleton singleton) {
		Singleton.singleton = singleton;
	}

	public static synchronized void displayThreadId() {
		
		try {
			System.out.println(
					"in.connect2tech.testng.parallel.Singleton.displayThreadId()::" + Thread.currentThread().getId() + " , going to sleep");
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(
				"in.connect2tech.testng.parallel.Singleton.displayThreadId()::" + Thread.currentThread().getId() + " , wake up");
	}

	public static Singleton getSingleton() {
		if (singleton == null) {
			singleton = new Singleton();
			return singleton;
		} else {
			return singleton;
		}
	}
}
