package in.connect2tech.testng.parallel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.Test;

public class TestNgUnZipMethodsParallel {
	
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
	
	@Test()
	public void unZip1() {
		Date date = new Date();
		System.out.println("in.connect2tech.testng.parallel.TestNgUnZipMethodsParallel.unZip1()::"+Thread.currentThread().getId()+ " , Time::"+dateFormat.format(date));
		Singleton.displayThreadId();
	}

	@Test()
	public void unZip2() {
		Date date = new Date();
		System.out.println("in.connect2tech.testng.parallel.TestNgUnZipMethodsParallel.unZip2()::"+Thread.currentThread().getId()+ " , Time::"+dateFormat.format(date));
		Singleton.displayThreadId();
	}

	@Test()
	public void unZip3() {
		Date date = new Date();
		System.out.println("in.connect2tech.testng.parallel.TestNgUnZipMethodsParallel.unZip3()::"+Thread.currentThread().getId()+ " , Time::"+dateFormat.format(date));
		Singleton.displayThreadId();
	}

	@Test()
	public void unZip4() {
		Date date = new Date();
		System.out.println("in.connect2tech.testng.parallel.TestNgUnZipMethodsParallel.unZip4()::"+Thread.currentThread().getId()+ " , Time::"+dateFormat.format(date));
		Singleton.displayThreadId();
	}

	@Test()
	public void unZip5() {
		Date date = new Date();
		System.out.println("in.connect2tech.testng.parallel.TestNgUnZipMethodsParallel.unZip1()::"+Thread.currentThread().getId()+ " , Time::"+dateFormat.format(date));
		Singleton.displayThreadId();
	}
}
