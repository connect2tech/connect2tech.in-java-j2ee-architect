package in.connect2tech.testng.parallel;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class SingletonConcept {
	public static void main(String[] args) {
	
		Singleton single = Singleton.getSingleton();
		System.out.println(single);
		
		Singleton single2 = Singleton.getSingleton();
		System.out.println(single2);
	}
}
