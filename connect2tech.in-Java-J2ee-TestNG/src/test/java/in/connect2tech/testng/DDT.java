package in.connect2tech.testng;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DDT {

	@DataProvider
	public String[][] dataMethod() {

		String data[][] = { { "10" }, { "20" }

		};

		return data;
	}

	@Test(dataProvider = "dataMethod")
	public void login(String data1) {
		System.out.println(data1);
		System.out.println("login/Thread Id:" + Thread.currentThread().getId());
	}
	
	
	@Test
	public void login1() {
		System.out.println("login1/Thread Id:" + Thread.currentThread().getId());
	}
}
