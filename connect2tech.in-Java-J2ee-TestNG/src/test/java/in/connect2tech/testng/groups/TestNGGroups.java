package in.connect2tech.testng.groups;

import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

public class TestNGGroups {

	@BeforeGroups("sanity")
	public void beforeMethod() {
		System.out.println("Before test-method.");
	}

	@Test(groups = { "sanity" })
	public void testMethodOne() {
		System.out.println("---------------testMethodOne/sanity---------------");
	}

	/**
	 * It follows intersection.
	 */
	@Test(groups = {"sanity","regression"})
	public void testMethodTwo() {
		System.out.println("---------------testMethodTwo---------------");
	}

	@Test(groups = { "regression" })
	public void testMethodThree() {
		System.out.println("---------------testMethodThree/regression---------------");
	}
}
