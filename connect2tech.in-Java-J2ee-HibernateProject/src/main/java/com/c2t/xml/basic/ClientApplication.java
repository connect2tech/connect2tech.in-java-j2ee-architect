package com.c2t.xml.basic;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ClientApplication 
{
    public static void main( String[] args )
    {
    	
    	
    	Configuration config = new Configuration().configure("com/c2t/xml/basic/hibernate.cfg.xml");
    	SessionFactory sf = config.buildSessionFactory();
    	Session session = sf.openSession();
    	
    	Session session2 = sf.openSession();
    	
    	System.out.println("session="+session);
    	System.out.println("session2="+session2);
    	
    	
    	Stock stock = new Stock();
    	stock.setStockCode("400");
    	stock.setStockName("TCS4");
    	
    	session.beginTransaction();
    	//Persistent
    	session.save(stock);
    	session.getTransaction().commit();
    	
    	
    	
    	System.out.println("Done...");
    	
    }
    
    

}
