package com.c2t.xml.one.one;

import java.util.Date;
import org.hibernate.Session;

public class ClientApplication2 {
	public static void main(String[] args) {
		System.out.println("Maven + Hibernate One-to-One example + MySQL");
		Session session = HibernateUtil.getSessionFactory().openSession();

		Stock stock = new Stock();
		stock.setStockCode("code1");
		stock.setStockName("name1");
		
		StockDetail stockDtls = new StockDetail();
		stockDtls.setCompName("comp1");
		stockDtls.setCompDesc("Desc1");
		stockDtls.setListedDate(new Date());
		
		stock.setStockDetail(stockDtls);
		stockDtls.setStock(stock);
		
		session.beginTransaction();
				
		session.save(stock);
		session.save(stockDtls);

		session.getTransaction().commit();

	}
}
