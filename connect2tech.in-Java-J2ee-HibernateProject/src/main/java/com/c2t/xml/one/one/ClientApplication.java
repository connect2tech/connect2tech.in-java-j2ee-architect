package com.c2t.xml.one.one;

import java.util.Date;
import org.hibernate.Session;

public class ClientApplication {
	public static void main(String[] args) {
		System.out.println("Maven + Hibernate One-to-One example + MySQL");
		Session session = HibernateUtil.getSessionFactory().openSession();

		Stock stock = new Stock();
		stock.setStockCode("ABC5");
		stock.setStockName("Name5");

		StockDetail sdetails = new StockDetail();
		sdetails.setCompDesc("desc");
		sdetails.setCompName("name");
		sdetails.setListedDate(new Date());
		sdetails.setRemark("remark");
		
		stock.setStockDetail(sdetails);
		sdetails.setStock(stock);
		
		session.beginTransaction();
		session.save(stock);
		//session.save(sdetails);
		session.getTransaction().commit();
		
	}
}
