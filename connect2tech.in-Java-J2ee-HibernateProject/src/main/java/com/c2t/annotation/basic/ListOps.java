package com.c2t.annotation.basic;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class ListOps {

	public static void main(String[] args) {

		SessionFactory sf = new AnnotationConfiguration().configure("com/c2t/annotation/basic/hibernate.cfg.xml")
				.buildSessionFactory();

		Session session = sf.openSession();

		Query q = session.createQuery("from Employee");
		q.setCacheable(true);
		System.out.println("---------------executing query1-----------");
		List <Employee> list = q.list();
		
		for(int i=0;	i<list.size();	i++){
			
			Employee e = list.get(i);
			System.out.println(e.getFirstname());
			System.out.println(e.getLastname());
			System.out.println("-------------------");
		
		}
		
		
		System.out.println("---------------executing query2-----------");
		list = q.list();
		
		for(int i=0;	i<list.size();	i++){
			
			Employee e = list.get(i);
			System.out.println(e.getFirstname());
			System.out.println(e.getLastname());
			System.out.println("-------------------");
		
		}
		
		
	}

}
