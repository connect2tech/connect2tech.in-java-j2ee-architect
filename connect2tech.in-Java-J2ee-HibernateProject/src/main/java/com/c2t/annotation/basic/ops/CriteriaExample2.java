package com.c2t.annotation.basic.ops;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

public class CriteriaExample2 {
	
	public static void main(String[] args) {
		
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		
		Criteria criteria = session.createCriteria(Employee.class);
		criteria.add(Restrictions.eq("id", 1l));
		
		System.out.println("-----fetching data-----");
		
		List l = criteria.list();
		
		System.out.println(l.size());
		
		for(int i=0;	i<l.size();	i++){
			Employee emp = (Employee)l.get(i);
			System.out.println(emp.getFirstname());
		}
		
	}

}
