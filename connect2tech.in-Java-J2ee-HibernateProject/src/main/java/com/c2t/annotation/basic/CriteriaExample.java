package com.c2t.annotation.basic;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.criterion.Restrictions;

public class CriteriaExample {
	
	public static void main(String[] args) {
		
		SessionFactory sf = new AnnotationConfiguration().configure("com/c2t/annotation/basic/hibernate.cfg.xml")
				.buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		
		Criteria c = session.createCriteria(Employee.class);
		
		c.add(Restrictions.eq("id", 1l));
		
		List l = c.list();
		
		for(int i=0;i<l.size();i++){
			Employee emp = (Employee)l.get(i);
			
			System.out.println(emp.getId());
			System.out.println(emp.getFirstname());
			System.out.println("--------------------");
		}
	
		session.getTransaction().commit();
	}
	

}
