package com.c2t.annotation.cache;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import com.c2t.annotation.basic.Employee;


public class ListOperationCaching {

	public static void main(String[] args) {

		SessionFactory sf = new AnnotationConfiguration().configure("com/c2t/annotation/basic/hibernate.cfg.xml")
				.buildSessionFactory();
		Session session = sf.openSession();

		// logic to verify db update
		// if true, flush session.

		Query query = null;

		query = session.createQuery("from Employee where cellphone > 912");
		query.setCacheable(true);
		

		List<Employee> list2 = query.list();

		
		
		List<Employee> list = query.list();

		

		

	}

}
