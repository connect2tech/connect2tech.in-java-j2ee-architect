package in.connect2tech.faker;

import com.github.javafaker.Faker;

public class UserName {
	public static void main(String[] args) {
		Faker faker = new Faker();
		String name = faker.name().fullName(); // Mr. Joey
		String firstName = faker.name().firstName(); // Ross
		String lastName = faker.name().lastName(); // Geller
		
		System.out.println(firstName);
	}
}
