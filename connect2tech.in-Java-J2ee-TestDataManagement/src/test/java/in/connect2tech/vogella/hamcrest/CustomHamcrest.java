package in.connect2tech.vogella.hamcrest;

import static org.hamcrest.Matchers.is;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.Test;
import static org.hamcrest.MatcherAssert.assertThat;

public class CustomHamcrest {
	@Test
	public void fellowShipOfTheRingShouldContainer7() {
		assertThat("Gandalf", length(is(7)));
	}

	// <Integer> Matcher<? super Integer>
	public static Matcher<String> length(Matcher<? super Integer> matcher) {
		return new FeatureMatcher<String, Integer>(matcher, "a String of length that", "length") {
			@Override
			protected Integer featureValueOf(String actual) {
				return actual.length();
			}
		};
	}
}
