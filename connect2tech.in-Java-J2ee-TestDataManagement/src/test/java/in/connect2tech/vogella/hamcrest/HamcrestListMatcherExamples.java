package in.connect2tech.vogella.hamcrest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayWithSize;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class HamcrestListMatcherExamples {
	@Test
	public void listShouldInitiallyBeEmpty() {
		List<Integer> list = Arrays.asList(5, 2, 4);

		assertThat(list, hasSize(3));
		// ensure the order is correct
		assertThat(list, contains(5, 2, 4));
		assertThat(list, containsInAnyOrder(2, 4, 5));
		assertThat(list, everyItem(greaterThan(1)));

	}

	@Test
	public void arrayHasSizeOf4() {
		Integer[] ints = new Integer[] { 7, 5, 12, 16 };
		assertThat(ints, arrayWithSize(4));
		assertThat(ints, arrayContaining(7, 5, 12, 16));
	}

	@Test
	public void objectHasSummaryProperty() {
		Todo todo = new Todo(1, "Learn Hamcrest", "Important");
		assertThat(todo, hasProperty("summary"));
	}

	@Test
	public void objectHasCorrectSummaryValue() {
		Todo todo = new Todo(1, "Learn Hamcrest", "Important");
		assertThat(todo, hasProperty("summary", equalTo("Learn Hamcrest")));
	}

	@Test
	public void objectHasSameProperties() {
		Todo todo1 = new Todo(1, "Learn Hamcrest", "Important");
		Todo todo2 = new Todo(1, "Learn Hamcrest", "Important");
		assertThat(todo1, samePropertyValuesAs(todo2));
	}
}