package in.connect2tech.json.converter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JacksonObjectMapperJsonUtility {

	/**
	 * 
	 * @param jsonFile - jsonFile
	 * @return
	 * @throws IOException
	 */
	public static String convertJsonFileToObjectGraphString(String jsonFile) throws IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		// read json file data to String
		byte[] jsonData = Files.readAllBytes(Paths.get(jsonFile));

		String json = new String(jsonData);

		// System.out.println("json=>" + json);

		return json;

	}

}