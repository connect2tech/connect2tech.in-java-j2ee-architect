package in.connect2tech.basic;

import java.io.IOException;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.util.JSON;

import in.connect2tech.json.converter.JacksonObjectMapperJsonUtility;

public class StoringJsonInMongoDb {

	public static void main(String str[]) throws IOException {
		MongoClient mongoClient = new MongoClient("localhost", 27017);
		mongoClient.getDatabaseNames().forEach(System.out::println);

		DB database = mongoClient.getDB("local");
		database.createCollection("jsons", null);

		DBCollection collection = database.getCollection("jsons");
		System.out.println(collection);

		String json = JacksonObjectMapperJsonUtility.convertJsonFileToObjectGraphString("employee.json");
		DBObject dbObject = (DBObject) JSON.parse(json);

		collection.insert(dbObject);

		DBCursor cursorDoc = collection.find();
		while (cursorDoc.hasNext()) {
			System.out.println(cursorDoc.next());
		}

		System.out.println("Done");

	}
}
