package in.connect2tech.basic.dockers;

import org.bson.Document;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;

public class DockerMongoDb {

	MongoClient mongoClient;
	MongoCollection<Document> collection_table;

	@BeforeClass
	public void setUp() {
		mongoClient = new MongoClient("192.168.56.101", 27017);
	}

	@Test(priority = 1)
	public void getDatabaseNamesMongo() {
		mongoClient.getDatabaseNames().forEach(System.out::println);
	}

}
