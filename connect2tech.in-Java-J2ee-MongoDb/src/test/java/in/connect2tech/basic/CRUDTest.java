package in.connect2tech.basic;

import java.util.function.Consumer;

import org.bson.Document;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class CRUDTest {

	MongoClient mongoClient;
	MongoCollection<Document> collection_table;

	@BeforeClass
	public void setUp() {
		mongoClient = new MongoClient("localhost", 27017);
	}

	@Test(priority = 1)
	public void getDatabaseNamesMongo() {
		mongoClient.getDatabaseNames().forEach(System.out::println);
	}

	@Test(priority = 2)
	public void createCollectionTable() {
		MongoDatabase database = mongoClient.getDatabase("local");

		try {
			database.createCollection("customers");
		} catch (Exception e) {
			System.out.println(e);
			database.getCollection("customers").drop();
			database.createCollection("customers");
		}

		collection_table = database.getCollection("customers");
		System.out.println(collection_table);
	}

	@Test(priority = 3)
	public void insertDocumentRow() {

		BasicDBObject document_row = new BasicDBObject();
		document_row.put("name", "Shubham");
		document_row.put("company", "Baeldung");
		collection_table.insertOne(new Document(document_row));

	}

	@Test(priority = 4)
	public void fetchDocumentRow() {

		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("name", "Shubham");
		FindIterable<Document> iterable = collection_table.find(searchQuery);
		iterable.forEach((Consumer<Document>) doc -> System.out.println(doc.toJson()));
	}

}
