# Java J2ee

## Projects ##
### D:\nc\Java-Architect\connect2tech.in-java-j2ee-architect ###

- connect2tech.in-Java-J2ee-web
- connect2tech.in-Java-J2ee-CoreJava
- connect2tech.in-Java-J2ee-OOAD
- connect2tech.in-Java-J2ee-DesignPatterns
- connect2tech.in-Java-J2ee-SOAP-RPC-Document
- connect2tech.in-Java-J2ee-TestDataManagement
- connect2tech.in-Java-J2ee-XML
- connect2tech.in-Java-J2ee-Algorithms
- connect2tech.in-Java-J2ee-JSON
- connect2tech.in-Java-J2ee-Junit-TestNG-MockIt
- connect2tech.in-Java-J2ee-Drools-Engine
- connect2tech.in-Java-J2ee-Hamcrest
- connect2tech.in-Java-J2ee-Mockito
- connect2tech.in-Java-J2ee-Apache-POI
- connect2tech.in-Java-J2ee-TestNG
- connect2tech.in-Java-J2ee-ServletJsp
- connect2tech.in-Java-J2ee-Jdbc
- connect2tech.in-Java-J2ee-HibernateProject
- connect2tech.in-Java-J2ee-HibernateAnnotation
- connect2tech.in-Java-J2ee-Blank-Project
- connect2tech.in-Java-J2ee-MongoDb
- connect2tech.in-Java-J2ee-Drooling		
- connect2tech.in-Java-J2ee-Spring3x
- connect2tech.in-Java-J2ee-Spring-Junit
- connect2tech.in-Spring-Drools-Validation
- connect2tech.in-Java-J2ee-DataScience
- connect2tech.in-Java-J2ee-Drools-Training1
 
---

### Creating New Project ###
 
D:\nc\Java-Architect\connect2tech.in-java-j2ee-architect\<name of project> 

C:\nc\Java-Architect\connect2tech.in-java-j2ee-architect\
 
---

### D:\nc\Java-Architect ###
 * connect2tech.in-drools-engine-framework
 * connect2tech.in-drools-michalbali
 * connect2tech.in-java-j2ee-architect
 * connect2tech.in-jpetstore
---
### D:\nc\eclipse-workspace ###
 * connect2tech
 * Java-Architect
 * YouTube
 * DevOps
---