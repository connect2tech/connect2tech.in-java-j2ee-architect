package com.acme.app.validation;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.acme.app.dao.ApplicantDao;
import com.acme.app.model.Applicant;

public class Collaborators {

	private final Map collaborators;

	public Collaborators() {
		collaborators = null;
	}

	public Collaborators(Map collaborators) {
		super();
		this.collaborators = collaborators;
	}

	public Map getCollaborators() {
		return Collections.unmodifiableMap(collaborators);
	}
}