package com.acme.app.dao;

import com.acme.app.model.Applicant;

public interface ApplicantDao {
	Applicant findApplicant(Integer identifier);
}
