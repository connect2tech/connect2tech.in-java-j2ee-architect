package org.example.domain.purchase;

import org.example.domain.profile.Party;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.cdi.KSession;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import junit.framework.Assert;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@ContextConfiguration(locations = "classpath:springTestContext.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class Test2 {
	@KSession("springSession")
	private StatelessKieSession orderRulesProcessor;

	private final Party buyer = new Party();
	private final Party seller = new Party();

	@Test
	public void testConsignmentOrder() {

		/*KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();

		kbuilder.add(ResourceFactory.newFileSystemResource(fileName), ResourceType.DRL);

		Assert.assertFalse(kbuilder.hasErrors());

		if (kbuilder.hasErrors()) {

			System.out.println(kbuilder.getErrors());

		}*/

		System.out.println("orderRulesProcessor==>" + orderRulesProcessor);

		final Set<String> errors = new HashSet<>();
		errors.add("WTF");

		orderRulesProcessor.execute(new String());

	}

}
