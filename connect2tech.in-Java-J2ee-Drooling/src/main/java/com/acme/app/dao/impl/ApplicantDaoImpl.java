package com.acme.app.dao.impl;

import java.util.HashMap;
import java.util.Map;

import com.acme.app.dao.ApplicantDao;
import com.acme.app.model.Applicant;




public class ApplicantDaoImpl implements ApplicantDao {

	private final Map<Integer, Applicant> applicants = new HashMap<Integer, Applicant>();

	public ApplicantDaoImpl() {

		Applicant app1 = new Applicant(1, "Mr John Smith", 16);

		applicants.put(app1.getId(), app1);

		Applicant app2 = new Applicant(2, "Mr Joe Bloggs", 21);

		applicants.put(app2.getId(), app2);

	}

	@Override

	public Applicant findApplicant(Integer identifier) {

		return applicants.get(identifier);

	}

}