package com.acme.app.form;

import java.util.Date;

public class ApplicationForm {
	private int applicantId;
	private Date date;

	public ApplicationForm(int applicantId, Date date) {
		this.applicantId = applicantId;
		this.date = date;
	}

	public int getApplicantId() {
		return applicantId;
	}

	public void setApplicantId(int applicantId) {
		this.applicantId = applicantId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
