package com.acme.app.validation.tests;

import java.util.Date;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.acme.app.form.ApplicationForm;

public class ValidationTestCase {

	private static ApplicationContext ctx;

	private static Validator validator;

	@BeforeClass
	public static void beforeClass() {

		// Create the Spring application context
		String[] paths = { "application-context.xml" };
		ctx = new ClassPathXmlApplicationContext(paths);

	}

	@Before
	public void before() {
		validator = (Validator) ctx.getBean("validator");
	}

	@Test
	public void testInvalidAge() {
		
		System.out.println("Ready...");
		/*ApplicationForm applicationForm = new ApplicationForm(1, new Date());
		Set<ConstraintViolation<ApplicationForm>> violations = validator.validate(applicationForm);
		Assert.assertNotNull(violations);
		Assert.assertEquals(Integer.valueOf(2), Integer.valueOf(violations.size()));*/
	}

}