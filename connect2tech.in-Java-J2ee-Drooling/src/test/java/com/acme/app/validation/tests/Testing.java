package com.acme.app.validation.tests;

import javax.validation.Validator;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Testing {

	private static ApplicationContext ctx;

	private static Validator validator;

	@BeforeClass
	public static void beforeClass() {

		// Create the Spring application context
		String[] paths = { "application-context.xml" };
		ctx = new ClassPathXmlApplicationContext(paths);

	}

	@Before
	public void before() {
		//validator = (Validator) ctx.getBean("validator");
	}

	@Test
	public void testing() {
		System.out.println("Test...");
	}

}
