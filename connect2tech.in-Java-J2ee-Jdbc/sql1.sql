--Ctrl + F8
DROP TABLE customer
CREATE TABLE customer (
customer_id VARCHAR(50),	
first_name VARCHAR(50),		last_name VARCHAR(50),		
email VARCHAR(50),	address VARCHAR(50),		city VARCHAR(50),		
state VARCHAR(50),zipcode VARCHAR(50))

------------------------------
INSERT INTO customer VALUES(
'1',	'George',	'Washington',	'gwashington@usa.gov',	'3200 Mt Vernon Hwy',	'Mount Vernon','VA',	'22121')

------------------------------
INSERT INTO customer VALUES(
'2',	'John',	'Adams',	'jadams@usa.gov',	'1250 Hancock St',	'Quincy',	'MA',	'02169')

------------------------------

drop table CUSTOMER_ORDER

CREATE TABLE CUSTOMER_ORDER (
order_id VARCHAR(50),	
order_date VARCHAR(50),	
order_amount VARCHAR(50),	
customer_id VARCHAR(50)
)

insert into CUSTOMER_ORDER values('1',	'07/04/1776',	'$234.56',	'1')

insert into CUSTOMER_ORDER values('2',	'07/04/1778',	'$264.56',	'3')

--inner join
select first_name, last_name, order_date, order_amount
from customer c
inner join CUSTOMER_ORDER o
on c.customer_id = o.customer_id

--left join
select c.customer_id, o.order_id, first_name, last_name, order_date, order_amount
from customer c
left join customer_order o
on c.customer_id = o.customer_id

--right join
select c.customer_id, o.order_id, first_name, last_name, order_date, order_amount
from customer c
right join customer_order o
on c.customer_id = o.customer_id

--full join
select c.customer_id, o.order_id, first_name, last_name, order_date, order_amount
from customer c
full join customer_order o
on c.customer_id = o.customer_id