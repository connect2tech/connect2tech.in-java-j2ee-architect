--delimiter is the marker for the end of each command you send to the mysql command line client.

DELIMITER //
create procedure proc_my_emp1(IN name1 varchar(20) , IN age1 int, IN salary1 int)
begin
insert into my_emp1 (name, age, salary) values (name1, age1, salary1);
end //
DELIMITER ;