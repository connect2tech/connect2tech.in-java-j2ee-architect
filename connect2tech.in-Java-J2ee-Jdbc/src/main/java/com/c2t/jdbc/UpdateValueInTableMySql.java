package com.c2t.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class UpdateValueInTableMySql {
	public static void main(String[] args) throws SQLException, ClassNotFoundException {

		Class.forName("com.mysql.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root", "password");

		//String sql = "update my_emp1 set salary=1000 where age  > 15";

		String sql = "select * from my_emp1 where salary = 0";
		
		System.out.println("Connectio !!!");

		Statement stmt = connection.createStatement();
		System.out.println("Statement !!!");

		boolean status = stmt.execute(sql);
		System.out.println("Execute !!! = "+status);

		connection.close();
		System.out.println("Close !!!");
	}
}
