package com.c2t.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateTableMySQL {

	public static void main(String[] args) throws ClassNotFoundException {
		
		try {
			

			// Create connection object/ create connection
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root", "password");

			System.out.println("Connection Successful !!!");// Query
			String sql = "create table my_emp1 (name varchar(25),  age int, salary int)";
			//String sql = "insert into my_emp1 (name, age, salary) values ('java', 10 ,20)";
			
			Statement stmt = con.createStatement();
			
			for(int i=0;i<10;i++){
				stmt.execute(sql);
			}
			
			
			System.out.println("Execution done...");
			
			con.close();
			
			
			
			
		
		} catch (SQLException e) {
			System.out.println(e.getErrorCode());
			System.out.println(e.getMessage());

		}

	}

}
