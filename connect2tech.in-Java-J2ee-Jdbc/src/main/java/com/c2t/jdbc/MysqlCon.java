package com.c2t.jdbc;

import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlCon {
	public static void main(String args[]) throws SQLException  {

		//String url = "jdbc:mysql://192.168.56.101:3306/m";
		String url = "jdbc:mysql://192.168.56.101:3306/mysql?allowPublicKeyRetrieval=true&useSSL=false";
		DriverManager.getConnection(url, "root", "1234");
		System.out.println("Connection establised....");
	}
}
