package com.c2t.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

public class PrepareStatementMySql {
	public static void main(String[] args) throws SQLException {
		String url = "jdbc:mysql://localhost:8888/testdb";
		String user = "naresh";
		String password = "password";

		String sql = "update edureka set course = ? where id= ? ";

		Connection conn = DriverManager.getConnection(url, user, password);
		PreparedStatement ps = conn.prepareStatement(sql);

		for (int i = 0; i < 2; i++) {
			ps.setString(1, "Java");
			ps.setString(2, "edu00" + (i + 1));
			ps.execute();
		}

		conn.close();
		
		System.out.println("done!!!");

	}
}
