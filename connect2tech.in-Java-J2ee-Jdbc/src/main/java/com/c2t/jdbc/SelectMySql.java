package com.c2t.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SelectMySql {

	public static void main(String[] args) throws SQLException {

		String url = "jdbc:mysql://localhost:8888/testdb";
		String user = "naresh";
		String password = "password";
		String sql = "select * from edureka";

		Connection connection = DriverManager.getConnection(url, user, password);
		System.out.println("Connection Successful !!!");

		Statement statement = connection.createStatement();
		System.out.println("Statement successfull !!!");

		ResultSet rs = statement.executeQuery(sql);
		System.out.println("Execute Query !!!");
		System.out.println("");

		while (rs.next()) {
			int id = rs.getInt("age");
			String name = rs.getString(2);
			String course = rs.getString(3);
			
			//strongly typed language
			

			System.out.println("id = " + id);
			System.out.println("name = " + name);
			System.out.println("course = " + course);
			
			System.out.println("");
		}

		connection.close();
		System.out.println("Connection close !!!");

	}

}
