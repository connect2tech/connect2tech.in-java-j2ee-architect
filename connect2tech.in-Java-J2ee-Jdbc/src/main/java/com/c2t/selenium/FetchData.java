package com.c2t.selenium;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.HashMap;
import java.util.*;

public class FetchData {
	public static void main(String[] args) throws Exception {

		String sql = "select * from employee";

		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jan2019", "root", "password");
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);

		while(rs.next()){
			String name = rs.getString("NAME");
			String dept = rs.getString("DEPARTMENT");
			int age = rs.getInt("SALARY");
			
			System.out.println(name);
			System.out.println(dept);
			System.out.println(age);
			System.out.println("---------------------");
		}
		
		
	}
}
