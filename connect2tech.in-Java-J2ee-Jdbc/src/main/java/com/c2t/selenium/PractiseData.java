package com.c2t.selenium;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class PractiseData implements Comparable<PractiseData> {

	String measure_id;
	String measure_name;
	String registry__benchmark;
	String cms__benchmark;
	String performance__width__10;

	int cms;

	public String getMeasure_id() {
		return measure_id;
	}

	public void setMeasure_id(String measure_id) {
		this.measure_id = measure_id;
	}

	public String getMeasure_name() {
		return measure_name;
	}

	public void setMeasure_name(String measure_name) {
		this.measure_name = measure_name;
	}

	public String getRegistry__benchmark() {
		return registry__benchmark;
	}

	public void setRegistry__benchmark(String registry__benchmark) {
		this.registry__benchmark = registry__benchmark;
	}

	public String getCms__benchmark() {
		return cms__benchmark;
	}

	public void setCms__benchmark(String cms__benchmark) {
		this.cms__benchmark = cms__benchmark;
	}

	public String getPerformance__width__10() {
		return performance__width__10;
	}

	public void setPerformance__width__10(String performance__width__10) {
		this.performance__width__10 = performance__width__10;
	}

	@Override
	public int compareTo(PractiseData o) {

		int val = 0;

		int i1 = this.measure_id.compareTo(o.measure_id);
		int i2 = this.measure_name.compareTo(o.measure_name);
		
		int i3 = 0;
				
				if(this.cms > o.cms)
					i3 = 1;
				else if (this.cms == o.cms)
					i3 = 0;
					else
						i3= -1;

		if ((i1 == 0) && (i2 == 0) && (i3 == 0)) {
			return val;
		}

		return val;

	}

}
