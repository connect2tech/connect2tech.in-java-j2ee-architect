package com.c2t.webinar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class UpdateDbRecord {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/edureka1";
		String sql = "update employee set salary = 2000 where id = 2";
		Connection conn = null;
		Statement stmt;
		
		try {
			conn = DriverManager.getConnection(url, "root", "password");
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Update done...");
		
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
