package com.c2t.webinar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertIntoDb {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/Webinar";
		String sql = "insert into employee(id, name, age, salary) values (1, 'java', 10, 1000)";
		Connection conn = null;
		Statement stmt;
		
		try {
			conn = DriverManager.getConnection(url, "root", "password");
			stmt = conn.createStatement();
			stmt.execute(sql);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Insertion done...");
		
		
	}
}
