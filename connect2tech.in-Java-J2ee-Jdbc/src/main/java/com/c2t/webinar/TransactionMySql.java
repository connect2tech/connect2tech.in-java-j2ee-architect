package com.c2t.webinar;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * It should be logic transaction.
 * Debit from one account and credit to another account.
 */
public class TransactionMySql {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		
		// Create connection object/ create connection
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/edureka1", "root", "password");

		String sql1 = "insert into employee(id, name, age, salary) values (1, 'java', 10, 1000)";
		String sql2 = "insert into employee(id, name, age, salary1) values (1, 'java', 10, 1000)";
		
		Statement stmt = connection.createStatement();
		
		//connection.setAutoCommit(false);

		try {
			stmt.execute(sql1);//ATM deposit
			stmt.execute(sql2);//update the backend server..
		} catch (Exception e) {
			//connection.rollback();
			e.printStackTrace();
		}

		//connection.commit();
		
		connection.close();
		System.out.println("done....");

	}

}
