package com.c2t.edureka.wknd.ist800;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class FetechDataFromDb {
	public static void main(String[] args) throws Exception {

		String sql = "select * from employee";
		String url = "jdbc:mysql://localhost:3306/edureka_dec18";
		Connection conn = DriverManager.getConnection(url, "root", "password");

		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while(rs.next()){
			int id = rs.getInt("iD");
			String name = rs.getString(2);
			int age = rs.getInt(3);
			String dept = rs.getString(4);
			
			System.out.println("id="+id);
			System.out.println("name="+name);
			System.out.println("age="+age);
			System.out.println("dept="+dept);
			
			System.out.println("---------------");
		}

		System.out.println("Done....");
	}
}
