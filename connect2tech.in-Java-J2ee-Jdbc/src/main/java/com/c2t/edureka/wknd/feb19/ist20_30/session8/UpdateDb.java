package com.c2t.edureka.wknd.feb19.ist20_30.session8;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class UpdateDb {
	public static void main(String[] args) throws SQLException {
		
		String query = "update student set name='IT student' where id=100";

		String url = "jdbc:mysql://localhost:3306/edureka_feb19";
		Connection conn = DriverManager.getConnection(url, "root", "password");
		Statement stmt = conn.createStatement();
		
		stmt.executeUpdate(query);
		
		conn.close();

		System.out.println("Update Done...");
	}
}
