package com.c2t.edureka.wknd.feb19.ist20_30.session8;

import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectToDb {
	public static void main(String[] args) throws SQLException {
		
		String url = "jdbc:mysql://localhost:3306/edureka_feb19";
		DriverManager.getConnection(url, "root", "password");
		System.out.println("Connection successfull.....");
	}
}
