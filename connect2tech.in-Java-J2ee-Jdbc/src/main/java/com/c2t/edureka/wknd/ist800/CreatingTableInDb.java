package com.c2t.edureka.wknd.ist800;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class CreatingTableInDb {
	public static void main(String[] args) throws Exception {

		String sql = "create table employee(id int , name varchar(20), age int, department varchar(20))";
		String url = "jdbc:mysql://localhost:3306/edureka_dec18";
		Connection conn = DriverManager.getConnection(url, "root", "password");

		Statement stmt = conn.createStatement();
		stmt.execute(sql);

		System.out.println("Done....");
	}
}
