package in.connect2tech.api.wknd.ist800;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class UpdateDb {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/api_automation";

		String query = "update employee set designation = 'Test Lead' where salary > 1000";

		try {
			Connection conn = DriverManager.getConnection(url, "root", "password");
			Statement stmt = conn.createStatement();
			
			stmt.executeUpdate(query);
			
			System.out.println("Update done!!!");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
