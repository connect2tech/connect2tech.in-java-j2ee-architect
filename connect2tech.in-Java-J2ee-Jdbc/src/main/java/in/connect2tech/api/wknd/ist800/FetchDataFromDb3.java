package in.connect2tech.api.wknd.ist800;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class FetchDataFromDb3 {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/api_automation";

		String query = "select count(salary) salary_count from employee";

		try {
			Connection conn = DriverManager.getConnection(url, "root", "password");
			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(query);
			
			while(rs.next()){
				int salary = rs.getInt("salary_count");
				
				System.out.println("salary="+salary);
				System.out.println("---------------------------------");
			}

			System.out.println("Fetching done...");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
