package in.connect2tech.api.wknd.ist800;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect2DataBase {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/api_automation";
		try {
			DriverManager.getConnection(url,"root","password");
			
			System.out.println("Db connection successful...");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
