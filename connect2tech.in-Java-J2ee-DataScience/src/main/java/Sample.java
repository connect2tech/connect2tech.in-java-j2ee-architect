import tech.tablesaw.api.DoubleColumn;

public class Sample {

	public static void main(String[] args) {
		double[] numbers = { 1, 2, 3, 4 };
		DoubleColumn nc = DoubleColumn.create("nc", numbers);
		System.out.println(nc.print());

		DoubleColumn nc2 = nc.multiply(4);
		System.out.println(nc2.print());
	}

}
