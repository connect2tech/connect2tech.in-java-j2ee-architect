package in.connect2tech.basics;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import tech.tablesaw.api.Row;
import tech.tablesaw.api.StringColumn;
import tech.tablesaw.api.Table;
import tech.tablesaw.io.xlsx.XlsxReadOptions;
import tech.tablesaw.io.xlsx.XlsxReader;

public class ExcelData {

	Table tab;

	@BeforeClass
	public void excelSetUp() {
		XlsxReader reader = new XlsxReader();
		XlsxReadOptions options = XlsxReadOptions.builder("excel/Excel.xlsx").build();
		try {
			tab = reader.read(options);
			// System.out.println(tab.print());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(enabled = false)
	public void getStructure() {
		System.out.println(tab.structure());
	}

	@Test(enabled = false)
	public void first_3() {
		System.out.println(tab.first(3));
	}

	@Test(enabled = false)
	public void last_3() {
		System.out.println(tab.last(3));
	}

	@Test(enabled = false)
	public void columnNames() {
		System.out.println(tab.columnNames());
	}

	@Test(enabled = false)
	public void retainColumns() {
		System.out.println(tab.retainColumns("First Name", "Last Name"));
	}

	@Test(enabled = false)
	public void gettingColumns() {
		StringColumn col = tab.stringColumn("First Name");
		System.out.println(col.print());
	}

	@Test(enabled = false)
	public void rowOperation1() {
		for (Row row : tab) {
			System.out.println(row.getString("First Name") + "  : " + row.getString("Last Name"));
		}
	}

	@Test(enabled = false)
	public void rowOperation_Java8() {

		Map<String, String> m = new HashMap<String, String>();

//		tab.stream().forEach(row -> {
//			System.out.println(row.getString("First Name") + "  : " + row.getString("Last Name"));
//			m.put(row.getString("First Name"), row.getString("Last Name"));
//		});

	/*	tab.stream().forEach(row -> {
			System.out.println(row.getString("First Name") + "  : " + row.getString("Last Name"));
			m.put(row.getString("First Name"), row.getString("Last Name"));

		});*/

	}

	/*
	 * @Test(enabled = true) public void rowOperation_Java80() { List l =
	 * tab.stream().filter(row ->
	 * row.getString("First Name").equals("Vincenza")).collect(Collectors.toList());
	 * System.out.println(l); }
	 */

}
