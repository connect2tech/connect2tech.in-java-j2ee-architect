package in.connect2tech.basics;

import org.testng.annotations.Test;

import tech.tablesaw.api.DoubleColumn;
import tech.tablesaw.api.Table;
import tech.tablesaw.io.xlsx.XlsxReadOptions;
import tech.tablesaw.io.xlsx.XlsxReader;

public class Test1 {

	@Test(enabled = false)
	public void test1() {
		double[] numbers = { 1, 2, 3, 4 };
		DoubleColumn nc = DoubleColumn.create("nc", numbers);
		System.out.println(nc.print());

		System.out.println(nc.get(2));

		DoubleColumn nc2 = nc.multiply(4);
		System.out.println(nc2.print());
	}

}
