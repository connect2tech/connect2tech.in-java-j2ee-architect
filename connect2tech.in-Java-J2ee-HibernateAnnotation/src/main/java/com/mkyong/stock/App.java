package com.mkyong.stock;

import java.util.Date;
import org.hibernate.Session;


public class App {
	public static void main(String[] args) {
		System.out.println("Hibernate one to many (Annotation)");
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Stock stock = new Stock();
        stock.setStockCode("7052");
        stock.setStockName("PADINI");
        session.save(stock);
        
        StockDailyRecord stockDailyRecords = new StockDailyRecord();
        stockDailyRecords.setPriceOpen(new Float("1.2"));
        stockDailyRecords.setPriceClose(new Float("1.1"));
        stockDailyRecords.setPriceChange(new Float("10.0"));
        stockDailyRecords.setVolume(3000000L);
        stockDailyRecords.setDate(new Date());
        
        stockDailyRecords.setStock(stock);        
        stock.getStockDailyRecords().add(stockDailyRecords);

        session.save(stockDailyRecords);
        
        StockDailyRecord stockDailyRecords2 = new StockDailyRecord();
        stockDailyRecords2.setPriceOpen(new Float("1.3"));
        stockDailyRecords2.setPriceClose(new Float("1.3"));
        stockDailyRecords2.setPriceChange(new Float("10.3"));
        stockDailyRecords2.setVolume(3000000L);
        Date someDate = new Date();
        someDate.setDate(someDate.getDate() + 1); 
        stockDailyRecords2.setDate(someDate);
        
        stockDailyRecords2.setStock(stock);        
        stock.getStockDailyRecords().add(stockDailyRecords2);

        session.save(stockDailyRecords2);

		session.getTransaction().commit();
		System.out.println("Done");
	}
}
