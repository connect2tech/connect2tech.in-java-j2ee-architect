package in.connect2tech.soap.document;
 
import javax.xml.ws.Endpoint;
 
//Endpoint publisher
public class HelloWorldPublisher{
 
	public static void main(String[] args) {
	   Endpoint.publish("http://localhost:8888/ws/hello", new HelloWorldImpl());
	   System.out.println("SOAP Service Up and Running...");
    }
 
}