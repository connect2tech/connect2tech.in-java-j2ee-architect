# CoreJava

# Java8-Features

## Pluralsight

### Lambdas

* Lambdas are written to make instance of anonymous classes easier to write and read.

![Lambad1](Lambda1.png)

---

**Comparator Interface**

```
Comparator<String> compare = new Comparator<String>() {
	@Override
	public int compare(String o1, String o2) {
		return o1.compareTo(o2);
	}
};
System.out.println(compare.compare("o1", "o2"));
```

```
Comparator<String> compareLambda = (String o1, String o2) -> {
	return o1.compareTo(o2);
};
System.out.println(compareLambda.compare("o1", "o1"));
```

```
Comparator<String> compareLambda2 = (o1, o2) -> {
	return o1.compareTo(o2);
};
System.out.println(compareLambda2.compare("o1", "o1"));
```

```
Comparator<String> compareLambda3 = (o1, o2) -> o1.compareTo(o2);
System.out.println(compareLambda3.compare("o1", "o1"));
```

---

 * What is type of lambda expression: It is functional interface. 
 * Can  a lambda be put in variable: Yes. 
 * Is a lambda expression an object:
    * While creating object with ```new``` the overhead of ```new``` is involved. 
    * In case of Lambdas, no overhead is involved.
 ![Lambad2](Lambda2.png)
  
    * A lambda can be taken as a method parameter, and can be returned by a method. 
    * Is lambda an object? It is not object. You cannot do ```equals``` etc.  Performance is good.
    * Lambda is an object without an identity.
    * Lambda is a piece of code that can be moved around. It should not be treated as traditional object.

---

**Functional Interface**

 * 4 Categories
 * **Supplier**: It does not take any object but provides and object
 
```
@FunctionalInterface
public interface Supplier<T>{
	T get()
}
```
 * **Consumer/BiConsumer**: It takes an object but does not return anything.

```
@FunctionalInterface
public interface Consumer<T>{
	void accept(T t)
}
```

```
@FunctionalInterface
publicinterfaceBiConsumer<T, U> {
	void accept(T t, U u);
}
```  

* **Predicate/BiPredicate**: It takes an object and returns boolean.

```
@FunctionalInterface
public interface Predicate<T> {
	boolean test(T t);
}
```

```
@FunctionalInterface
public interface BiPredicate<T, U> {
	boolean test(T t, U u);
}
```

* **Function/BiFunction**: It takes an object and returns an object.

```
@FunctionalInterface
public interface Function<T, R> {
	R apply(T t);
}

//It takes an object, and returns object of same kind.
@FunctionalInterface
public interface UnaryOperator<T> extends Function<T, T> {
}
```

```
@Functional Interface
public interface BiFunction<T, U, R> {
	R apply(T t, U u);
}

//It takes 2 objects of same kind and returns an object of the same kind.
@FunctionalInterface
public interface BinaryOperator<T> extends BiFunction<T, T, T> {
}

```
    
---

**Method References**

```
Comparator<Integer> compare2 = new Comparator<Integer>() {
	public int compare(Integer o1, Integer o2) {
		return Integer.compare(o1, o2);
	}
};

// Method References
Comparator<Integer> compareLambda4 = Integer::compare;
compareLambda4.compare(10, 20);
```

**Consumer Example**
```
public class ConsumerLambda {
	public void method() {

		Consumer<String> consumer = new Consumer<String>() {
			@Override
			public void accept(String t) {
				System.out.println(t);
			}
		};

		consumer.accept("welcome to anonymous class");

		Consumer<String> lambdaConsumer = (String t) -> {
			System.out.println(t);
		};

		lambdaConsumer.accept("welcome to lambdas!!!");

		Consumer<String> lambdaConsumer_2 = (String t) -> System.out.println(t);
		lambdaConsumer_2.accept("welcome to lambdas1!!!");

		Consumer<String> lambdaConsumer_3 = (t) -> System.out.println(t);
		lambdaConsumer_3.accept("welcome to lambdas1!!!");
		
		
		List<String> list = new ArrayList<String>();
		list.add("A");
		list.add("B");
		
		System.out.println("Displaying list using consumer!!!");
		list.forEach(lambdaConsumer_3);
		
		//Method References
		Consumer<String> lambdaConsumer_4 = System.out::println;
		lambdaConsumer_4.accept("welcome to lambdas1!!!");

	}

	// Test the file filtering
	public static void main(String[] args) {
		ConsumerLambda consumer = new ConsumerLambda();
		consumer.method();
	}
}
```
---

**Processing collection data using Lambdas**

```
public class ForEach {
	public static void main(String[] args) {
		List<String> l = new ArrayList<String>();
		l.add("A");
		l.add("B");
		l.add("C");
		l.add("D");
		// default void forEach(Consumer<? super T> action)

		l.forEach(t -> System.out.println(t));
		System.out.println("-----------------------------------");
		l.forEach(System.out::println);
		System.out.println("-----------------------------------");

		Consumer<String> consumerA = t -> System.out.println(t);
		Consumer<String> consumerB = t -> System.out.println(t.toLowerCase());
		l.forEach(consumerA.andThen(consumerB));

	}
}
```

```
public interface Iterable<T> {
    default void forEach(Consumer<? super T> action) {
        Objects.requireNonNull(action);
        for (T t : this) {
            action.accept(t);
        }
    }
}

public interface Collection<E> extends Iterable<E> {}
public interface List<E> extends Collection<E> {}

```

**New Patterns**

```
Predicate<String> predicate = new Predicate<String>() {
	@Override
	public boolean test(String t) {
		return t.length() > 10;
	}
};

System.out.println(predicate.test("Naresh"));

Predicate<String> predicateLambda = (String t) -> t.length() > 10;
System.out.println(predicateLambda.test("My Heer"));

Predicate<String> predicateLambda2 = (t) -> t.length() < 20;
System.out.println(predicateLambda2.test("You are My Heer"));

Predicate<String> predicateLambda3 = predicateLambda.and(predicateLambda2);
System.out.println(predicateLambda3.test("Heer, Really"));

//Predicate<String> predicateLambda5 = Objects::isNull;

Predicate<String> predicateLambda4 = Predicate.isEqual("Hello");
System.out.println(predicateLambda4.test("Hello"));

```

```
static <T> Predicate<T> isEqual(Object targetRef) {
	return (null == targetRef)
			? Objects::isNull
			: object -> targetRef.equals(object);
}
```
