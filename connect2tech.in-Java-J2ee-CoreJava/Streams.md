# CoreJava

# Java8-Features

## Pluralsight

### Streams

**Map/Filter/Reduce: Problem Statement**

 - List<Person> list = new ArrayList<>();
 - Suppose we want to compute average age of people older than 20.
 - **Step1: Mapping**
   - Mapping takes a ```List<Person>``` and returns a ```List<Integer>```
   - The size of both the lists is same
   - Order of elements is important
 - **Step2: Filtering**
   - The filtering step takes a List<Integer> and returns a List<Integer>
   - But some elements has been filtered out in the process
 - **Step3: Reduce**
   - Take average etc.
   - This is reduction step, equivalent to SQL aggregation.
   
```
public interface Stream<T> extends BaseStream<T, Stream<T>> {
}
```

 - It gives ways to **efficiently**- process large amount of data (and also smaller ones) in parallel.
 - **efficiently**:
   - In parallel, to leverage the computing power of multicore CPU.
   - Pipelined, to avoid unnecessary intermediary computations.
   
**What is Stream**
 - Stream is an object on which you can define operations.
 - It is an object that does not hold data.
 - An object that should not change data that it processes.
 - An object able to process data in one pass (does not create intermediate list to process data).
 - An object optimized from the algorithm point of view, and able to process data in parallel.
 
<img src="img/Filter1.png" alt="drawing" width="500" width="500"/>

**Summary**
 - The Stream API defines intermediary operations.    
 - forEach(Consumer) - Not Lazy
 - peek(Consumer) - Lazy
 - Filter(Predicate) - Lazy
 
**This code will not do anything** 

```
Stream<String> stream = Stream.of("one", "two", "three", "four", "five");

Predicate<String> p1 = Predicate.isEqual("two");
Predicate<String> p2 = Predicate.isEqual("three");

List<String> list = new ArrayList<>();

stream
        .peek(System.out::println)
        .filter(p1.or(p2))
        .peek(list::add);
```    

# Pluralsight: From Collections to Streams in Java 8 Using Lambda Expressions            
 