# Eclipse Guided Tour for Java By Tod Gentille: Pluralsight

 - Tools built on Eclipse
 - Work space is collection of projects.
 - Opening Perspective
 - Right click on icon and show text of the perspective.
 - Save Perspective As. Awesome !!!
 - Deleting Perspective from perferences.
 - Double Click Maximizes the window.
 - You can also customize the perspective.
 - Build automatically option: General/Workspace.
 - quick access: awesome
 - Assigning keyboard keys to an operation.
 - We can do formatting at all levels
 - navigator view to see .classpath file.
 - Link to Eclipse help
 - Revisit
 - Revisit
 - Nice feature to use. How many editors are open, and pin a given editor.
 - Tasks, TODO, Bookmarks
 - Block access. Accessing block on code, like in notepad++.

---

# 8 Eclipse Shortcut Keys for Code Refactoring

**Resource Link**: <a href="https://www.codejava.net/ides/eclipse/8-eclipse-shortcut-keys-for-code-refactoring" target="_blank">8-eclipse-shortcut-keys-for-code-refactoring</a>

 - **Alt + Shift + R**: Renames a variable, a method, a class or even a package name. This is the most frequently used shortcut in code refactoring. Select whole name of the class, method or variable you want to rename, and then press this shortcut. Type new name and press Enter when done, Eclipse automatically updates all related references for the new name, including ones found in other classes.
 
 - **Ctrl + 2, R**: Renames a variable, a method or a class name locally in the current file. Eclipse doesn’t search outside references hence this renaming is faster than the Alt + Shift +R shortcut. However, use this shortcut with care: only for names used locally in the current file
 
 -**Alt + Shift + M**: Extracts a selection to a method. This helps you move a selected block of code to a separate method with ease
 
 -**Alt + Shift + V**: Moves a class, method to another destination. For example, select a class name and press this shortcut, the Move dialog appears. Choose a destination and then click OK
 
 -**Alt + Shift + T**: Shows refactor context menu. This shortcut allows you to access a full list of refactoring operations which are possible for current context


