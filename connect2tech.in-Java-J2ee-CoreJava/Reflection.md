# CoreJava: Reflection

## Pluralsight

There are several classes

* the class Class
* the class Field
* the class Method
* the class Constructor

---

<img src="img/Reflection1.png" width="700" height="300"/>

```
int modifiers = field.getModifiers();
int modifier = Modifier.isPubic(modifiers);
```

---

<img src="img/Reflection2.png" width="700" height="300"/>

---

```
If you use setAccessible(true), then it is possible to access private members.
```

---

<img src="img/Reflection3-EntityManager.png" width="700" height="300"/>
