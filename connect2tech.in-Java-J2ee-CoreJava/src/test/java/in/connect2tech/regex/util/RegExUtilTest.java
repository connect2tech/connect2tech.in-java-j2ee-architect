package in.connect2tech.regex.util;

import org.junit.Test;

public class RegExUtilTest {

	String filePath = "D:\\naresh.chaurasia\\Java-Architect\\connect2tech.in-Java-J2ee-Architect\\connect2tech.in-Java-J2ee-CoreJava\\src\\main\\java\\in\\connect2tech\\regex\\";

	/**
	 * foo.bar - Period is any character
	 * 
	 */
	@Test
	public void testPeriod() {
		final String REGEX_PATTERN = "foo.bar";
		final String inputFileName = filePath + "regex01.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN, inputFileName);
	}

	/**
	 * foo.*bar - 0 or more characters in between foo and bar
	 */
	@Test
	public void testPeriodAsterix() {
		final String REGEX_PATTERN = "foo.*bar";
		final String inputFileName = filePath + "regex01.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN, inputFileName);
	}

	/**
	 * foo\\s+bar - one or more white spaces in between foo and bar
	 */
	@Test
	public void testWhiteSpace() {
		final String REGEX_PATTERN = "foo\\s+bar";
		final String inputFileName = filePath + "regex04.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN, inputFileName);
	}

	/**
	 * [fcl]o* - Any character [fcl] followed by 0 or more occurrences of o
	 */
	@Test
	public void testCharacterFollowedByAnotherCharacterOccurance() {
		final String REGEX_PATTERN = "[fcl]o*";
		final String inputFileName = filePath + "regex05.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN, inputFileName);
	}

	/**
	 * ^[^mh]o* - First ^ means that it should start with, and second ^ negates it.
	 */
	@Test
	public void testStringPatternNegate() {
		// Is not mh in the beginning
		final String REGEX_PATTERN = "^[^mh]o*";
		final String REGEX_PATTERN_2 = "^[mh]o*";
		final String inputFileName = filePath + "regex05.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN, inputFileName);
	}

	/**
	 * ^[j-mz]o* - Starts with any character from j-m or z, followed by 0 or more
	 * occurrences of o
	 */
	@Test
	public void testStringPatternRange() {
		// Is not mh in the beginning
		final String REGEX_PATTERN = "^[j-mz]o*";
		final String inputFileName = filePath + "regex08.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN, inputFileName);
	}

	/**
	 * ^[j-mJ-Mz]o* - Starts with and character from j-m or J-M or z, followed by 0
	 * or more occurrences of o
	 */
	@Test
	public void testStringPatternRangeCaseInsensitive() {
		// Is not mh in the beginning
		final String REGEX_PATTERN = "^[j-mJ-Mz]o*";
		final String inputFileName = filePath + "regex08.2.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN, inputFileName);
	}

	/**
	 * x*\\.y* - 0 or more x followed by . and then 0 or more y
	 */
	@Test
	public void testPeriodEscape() {
		// Is not mh in the beginning
		final String REGEX_PATTERN = "x*\\.y*";
		final String inputFileName = filePath + "regex11.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN, inputFileName);
	}

	@Test
	public void testSpecialCharactersPeriod() {
		// Is not mh in the beginning
		final String REGEX_PATTERN = "x[#:.]y";
		final String inputFileName = "D:\\naresh.chaurasia\\Java-Architect\\connect2tech.in-Java-J2ee-Architect\\connect2tech.in-Java-J2ee-CoreJava\\src\\main\\java\\in\\connect2tech\\regex\\regex12.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN, inputFileName);
	}

	@Test
	public void testSpecialCharactersCaret() {
		final String REGEX_PATTERN = "x[#:\\^]y";
		final String inputFileName = "D:\\naresh.chaurasia\\Java-Architect\\connect2tech.in-Java-J2ee-Architect\\connect2tech.in-Java-J2ee-CoreJava\\src\\main\\java\\in\\connect2tech\\regex\\regex13.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN, inputFileName);
	}

	@Test
	public void testSpecialCharactersBackslash() {
		final String REGEX_PATTERN = "x[#\\^\\\\]y";
		final String inputFileName = "D:\\naresh.chaurasia\\Java-Architect\\connect2tech.in-Java-J2ee-Architect\\connect2tech.in-Java-J2ee-CoreJava\\src\\main\\java\\in\\connect2tech\\regex\\regex14.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN, inputFileName);
	}

	@Test
	public void testString_3() {
		final String REGEX_PATTERN = "^foo\\s(bar|baz)";
		final String REGEX_PATTERN_2 = "^foo\\s(ba(r|z))";
		final String REGEX_PATTERN_3 = "^foo.*";
		final String inputFileName = "D:\\naresh.chaurasia\\Java-Architect\\connect2tech.in-Java-J2ee-Architect\\connect2tech.in-Java-J2ee-CoreJava\\src\\main\\java\\in\\connect2tech\\regex\\regex15.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN_3, inputFileName);
	}

	@Test
	public void testString_$() {
		final String REGEX_PATTERN = ".*bar$";
		final String inputFileName = filePath + "regex15.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN, inputFileName);
	}

	@Test
	public void testString() {
		final String REGEX_PATTERN = "^foo$";
		final String inputFileName = filePath + "regex17.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN, inputFileName);
	}

	/**
	 * ^\\d{3}$ - ^ anchor for start and $ anchor for end. The count of d must be 3.
	 * ^[0-9][0-9][0-9] - First 3 digits must be numbers, and after that it can be
	 * anything.
	 */
	@Test
	public void testAnchorsCaretDollar() {
		final String REGEX_PATTERN = "^\\d\\d\\d$";
		final String REGEX_PATTERN_2 = "^\\d{3}$";
		final String REGEX_PATTERN_3 = "^[0-9][0-9][0-9]";
		final String inputFileName = filePath + "regex18.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN_3, inputFileName);
	}

	/**
	 * ^[a-z]{4,6}$ - Start with any character from [a-z] and count of characters
	 * should be 4 or 5 or 6. ^[a-z]{4,6} - If $ is removed at the end, then there
	 * is no restriction at the end.
	 */
	@Test
	public void testStringDifferentSizes() {
		final String REGEX_PATTERN = "^[a-z]{4}$|^[a-z]{5}$|^[a-z]{6}$";
		String REGEX_PATTERN_2 = "^[a-z]{4,6}$";
		final String inputFileName = filePath + "regex19.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN_2, inputFileName);
	}

	/**
	 * (ha) - circle braces treats ha as a unit. ^(ha)+ - Starts with ha, after that
	 * it could be anything. ^(ha)+$ - starts with ha, there can be one or more
	 * occurrences of ha and it should end with ha. ^(ha){2}$ - starts with ha, 2
	 * occurrences of ha and should end with ha
	 */
	@Test
	public void testStringRepeatingPattern() {
		final String REGEX_PATTERN = "^(ha)+";
		final String REGEX_PATTERN_2 = "^(ha)+$";
		final String REGEX_PATTERN_3 = "^(ha){2}$";
		final String REGEX_PATTERN_4 = "^(ha){2,}$";
		final String REGEX_PATTERN_5 = "^(ha){,2}$";
		final String inputFileName = filePath + "regex20.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN_5, inputFileName);
	}

	
	@Test
	public void replaceRegExMonitorTest() {
		final String REGEX_PATTERN = "([0-9]+)x([0-9]+)";
		String INPUT = "1280x720";
		RegExUtil.replaceRegExMonitor(REGEX_PATTERN, INPUT);
	}

	/**
	 * Naresh Chaurasia=>Chaurasia , Naresh
	 */
	@Test
	public void changeFNameLNameOrderTest() {
		final String REGEX_PATTERN = "([a-zA-Z]+)\\s([a-zA-Z]+)";
		String INPUT = "Naresh Chaurasia";
		RegExUtil.changeFNameLNameOrder(REGEX_PATTERN, INPUT);
	}

	/**
	 * 7:32 to 32 mins past 7
	 */
	@Test
	public void changeClockTest() {
		final String REGEX_PATTERN = "([0-9]{1,2}+):([0-9]{2}+)";
		String INPUT = "7:32";
		RegExUtil.changeClock(REGEX_PATTERN, INPUT);
	}

	/**
	 * 914.582.3013 : xxx.xxx.3013
	 */
	@Test
	public void phoneMaskingTest() {
		final String REGEX_PATTERN = "[0-9]{3}\\.[0-9]{3}\\.([0-9]{4})";
		String INPUT = "914.582.3013";
		RegExUtil.phoneMasking(REGEX_PATTERN, INPUT);
	}

	/**
	 * (914).582.1234 : 914.582.1234
	 */
	@Test
	public void phoneFormatTest() {
		final String REGEX_PATTERN = "\\(([0-9]{3})\\)(\\.[0-9]{3}\\.[0-9]{4})";
		String INPUT = "(914).582.1234";
		RegExUtil.phoneFormat(REGEX_PATTERN, INPUT);

	}

	/**
	 * Jan 5th 1987 : 5-Jan-87
	 */
	@Test
	public void changeDateFormatTest() {
		final String REGEX_PATTERN = "([a-zA-Z]{3})\\s([0-9]{1,2})[a-z]{2}\\s[0-9]{2}([0-9]{2})";
		String INPUT = "Jan 5th 1987";
		RegExUtil.changeDateFormat(REGEX_PATTERN, INPUT);
	}
	
	@Test
	public void emailChallenge() {
		//final String REGEX_PATTERN = "(([a-z]*[\\._-][a-z0-9]*)@[a-z]*\\.[a-z]*)|([a-z]*@[a-z]*\\.[a-z]*)";
		final String REGEX_PATTERN = "[a-z]*@[a-z]*\\.[a-z]*";
		final String inputFileName = filePath + "email.txt";
		RegExUtil.matchRegExPattern(REGEX_PATTERN, inputFileName);
	}

	
}
