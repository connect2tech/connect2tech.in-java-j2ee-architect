//Comment
public class VariablesScope {
	
	static int a = 10;

	public static void main(String[] args) {
	
		int b = 20;
		int a = 100;
		
		System.out.println(b);
		System.out.println(a);
	
	}
	
	static void display(){
		System.out.println(a);
		//System.out.println(b);
	}
}
