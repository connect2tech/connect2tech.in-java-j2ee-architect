package com.figmd.session4;

import com.figmd.session3.File3;

class BankDetails{
	private int balance;
	
	void showBalance(){
		System.out.println(balance);
	}
	
	private void show(){
		System.out.println(balance);
	}
}


public class File4 {
	public static void main(String[] args) {
		BankDetails bank = new BankDetails();
		bank.showBalance();
	}
}
