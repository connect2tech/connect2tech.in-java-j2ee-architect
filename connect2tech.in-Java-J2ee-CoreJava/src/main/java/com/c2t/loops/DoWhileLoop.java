package com.c2t.loops;

public class DoWhileLoop {
	public static void main(String[] args) {
		int a = 5;
		
		do{
			
			System.out.println("a="+a);
			--a;
			
		}while(a>0);
		
	}
}
