package com.c2t.functions;

public class MethodReturnValues {
	public static void main(String[] args) {
		startCoffeeMachine();
		
		String s = "Stack example";
		
		int a = 10;
		
	}

	public static void startCoffeeMachine() {
		System.out.println("Start machine....");
		getCoffee(2);
	}

	public static String getCoffee(int dollar) {

		if (dollar > 2) {
			return "cappichuino";
		} else {
			return "latte";
		}

	}
}
