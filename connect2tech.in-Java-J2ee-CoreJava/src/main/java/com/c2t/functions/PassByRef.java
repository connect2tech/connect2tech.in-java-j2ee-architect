package com.c2t.functions;

class Car {
	String color;
}

public class PassByRef {
	public static void main(String[] args) {
	
		Car c1 = new Car();
		c1.color = "red";
		
		changeColor(c1);
		
		System.out.println(c1.color);
		
	}

	public static void changeColor(Car c2){
		c2.color = "passion red";
	}

}
