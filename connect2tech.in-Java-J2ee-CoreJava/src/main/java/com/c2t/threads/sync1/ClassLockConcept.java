package com.c2t.threads.sync1;

class Student{
	String name;
	static String trainer;
	float marks;
	
	synchronized void m1(){
		
	}
	
	static synchronized void m2(){
		
	}
}

public class ClassLockConcept {
	public static void main(String[] args) {
		Student s1 = new  Student();
		Student s2 = new Student();
		
		s1.m1();
		Student.m2();
		
		s2.m1();
	}
}
