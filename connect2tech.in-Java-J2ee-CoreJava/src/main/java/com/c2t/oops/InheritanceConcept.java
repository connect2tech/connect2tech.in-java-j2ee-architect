package com.c2t.oops;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class Car{
	String color="red";
	
	public void startEngine(){
		System.out.println("I am inside start engine...");
	}
}

class Maruti extends Car{
	
	public void changeValue(String s){
		color = s;
	}
	
}



public class InheritanceConcept {
	public static void main(String[] args) {
		Maruti m = new Maruti();
		//m.changeValue("Blue");
		m.color = "Blue";
		System.out.println(m.color);
		
		
		System.out.println(m.color);
		m.startEngine();
		
		Maruti m2 = new Maruti();
	}
}
