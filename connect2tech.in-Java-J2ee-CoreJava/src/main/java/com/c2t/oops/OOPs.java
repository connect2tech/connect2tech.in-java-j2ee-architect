package com.c2t.oops;

class MeParent{
	String name;
	int age;
	String aadharNo;
	boolean isMale;
	
	public void display(){
		System.out.println("name="+name);
		System.out.println("age="+age);
		System.out.println("isMale="+isMale);
	}
}

class MeChild extends MeParent{
	
}


class MeSubChild extends MeChild{
	
}

public class OOPs {
	public static void main(String[] args) {
		MeChild child1 = new MeChild();
		child1.display();
		
		MeChild child2 = new MeChild();
		child2.display();
	}
}
