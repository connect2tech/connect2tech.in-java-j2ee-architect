package com.c2t.oops;


class Connect2TechStudents{
	int age;
	String name;
	String course;
	
	public void study(){
		System.out.println("i study.."+name);
	}
	
	public void eat(){
		System.out.println("eat...");
	}
}
public class HouseClass {
	public static void main(String[] args) {
		int a = 10;
		
		Connect2TechStudents c1 = new Connect2TechStudents();
		c1.age = 25;
		c1.name = "Java Selenium";
		c1.course = "java";
		
		System.out.println(c1.age);
		
		c1.study();
		c1.eat();
		
		Connect2TechStudents c = new Connect2TechStudents();
		c.age = 25;
		c.name = "Java Selenium";
		c.course = "java";
		
		System.out.println(c.age);
		
		c.study();
		c.eat();
	}
}
