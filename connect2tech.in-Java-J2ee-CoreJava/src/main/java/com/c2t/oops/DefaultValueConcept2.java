package com.c2t.oops;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class DefaultValueConcept2 {
	static int a;
	static String name;
	static boolean b;
	
	public static void main(String[] args) {
		
		int c=10;
		
		System.out.println("c="+c);
		
		/*System.out.println("a="+a);
		System.out.println("name="+name);
		System.out.println("b="+b);*/
	}
	
	
	public static void display(){
		System.out.println("a="+a);
		//System.out.println("c="+c);
	}
	
	
}
