package com.c2t.oops;

class Bank{
	float balance;
	String typeofAccount;
	
	public void display(){
		System.out.println(balance);
		System.out.println(typeofAccount);
	}
	
}

public class EncapsulationConcept {
	public static void main(String[] args) {
		Bank b = new Bank();
		b.display();
	}
}
