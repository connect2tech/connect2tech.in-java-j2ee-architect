package com.c2t.oops;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class DefaultValueConcept3 {
	
	static int a;

	
	
	public static void main(String[] args) {
		
		int a_local=10;
		
		System.out.println("a="+a);
		System.out.println("a_local="+a_local);
		
	}
	
	public static void method(){
		int b_local=20;
		System.out.println("a="+a);
		System.out.println(b_local);
		//System.out.println("a_local="+a_local);
	}
	

}
