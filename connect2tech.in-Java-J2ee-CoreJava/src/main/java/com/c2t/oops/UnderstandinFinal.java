package com.c2t.oops;

final class Fruit {
	final String color= "red";
	
	
	void changeColor(){
		//color = "pale red";
	}
	
	
	void displayNameOfFruit(){
		System.out.println(color);
		
	}
}


public class UnderstandinFinal {
	public static void main(String[] args) {
		Fruit f = new Fruit();
		f.displayNameOfFruit();
		f.changeColor();
		f.displayNameOfFruit();
	}
}
