package com.c2t.oops;

class Student {
	int age;
	String name;
	
	public void display(){
		System.out.println("I am dispaly...");
	}
	
}

public class StudentConcepts {
	public static void main(String[] args) {
		int a = 10;
		Student s = new Student();
		s.age = 20;
		s.name = "Java Student";
		
		System.out.println(s.age);
		System.out.println(s.name);
		
		s.display();
	}
}
