package com.c2t.oops;

class Maths{
	int sum(int x, int y){
		return x + y;
	}
}

public class AnonymousObject {
	public static void main(String[] args) {
		Maths m = new Maths();
		int s = m.sum(10,20);
		System.out.println(s);
	}
}
