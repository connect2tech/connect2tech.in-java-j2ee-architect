package com.c2t.arrays;

public class PassingArrayToMethod {

	public static void main(String[] args) {
		int c[] = { 1, 2, 3, 4, 5 };// declaration, instantiation and
		// initialization
		display(c);
	}

	static void display(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
	}

}
