package com.c2t.operators;

public class Operator_Bitwise {

	public static void main(String args[]) {

		int a = 60;
		int b = 30;
		int c;

		System.out.println("Bitwise Operators");


		String aVar = Integer.toBinaryString(a);
		System.out.println(aVar);
		
		c = ~a;
		System.out.println("\n~ a :" + c);

		c = a & b;
		System.out.println("\na & b : " + c);

		c = a | b;
		System.out.println("a | b : " + c);

		c = a ^ b;
		System.out.println("a ^ b : " + c);

		c = a << 1;
		System.out.println("\na << 2 : " + c);
		System.out.println(Integer.toBinaryString(c));

		/*c = a >> 2;
		System.out.println("a >> 2 : " + c);

		c = a >>> 2;
		System.out.println("\na >>> 2 : " + c);*/
	}
}