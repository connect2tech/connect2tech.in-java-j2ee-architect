package com.c2t.scjp7.resources;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;

public class Java7ResourceManagement2 {

	public static void main(String[] args) {
		try (BufferedReader br = new BufferedReader(new FileReader("C:\\journaldev.txt"));
				java.io.BufferedWriter writer = java.nio.file.Files.newBufferedWriter(
						FileSystems.getDefault().getPath("C:\\journaldev.txt"), Charset.defaultCharset())) {
			System.out.println(br.readLine());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}