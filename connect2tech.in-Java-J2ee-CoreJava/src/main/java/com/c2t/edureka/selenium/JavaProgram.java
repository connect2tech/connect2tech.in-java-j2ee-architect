package com.c2t.edureka.selenium;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class JavaProgram {
	public static void main(String[] args) {
		int a = 0;
		int b = 20;
		
		int arr [] = {10,20};
		
		try {
			System.out.println(arr[10]);
		} catch (Exception e) {
			System.out.println(e);
		}finally{
			System.out.println("i am in finally..");
		}
		
		
		System.out.println("after the division...");
	}
}
