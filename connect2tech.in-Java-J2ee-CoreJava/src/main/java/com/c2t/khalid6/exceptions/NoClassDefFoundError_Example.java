package com.c2t.khalid6.exceptions;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class A{
	
}

class B{
	A a = new A();
}

public class NoClassDefFoundError_Example {
	public static void main(String[] args) {
		B b = new B();
	}
}
