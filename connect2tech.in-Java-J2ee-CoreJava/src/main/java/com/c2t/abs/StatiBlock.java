package com.c2t.abs;


class Block{
	
	static{
		System.out.println("I am in static block...");
	}
	
	Block(){
		System.out.println("constructor...");
	}
	
	static void m2(){
		System.out.println("static method...");
		//System.out.println(name);
	}
	
}


public class StatiBlock {
	public static void main(String[] args) {
		Block.m2();
	}
}
