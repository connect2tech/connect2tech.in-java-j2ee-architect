package com.c2t.override;

import java.util.Scanner;

public class MainForCars {
	public static void main(String[] args) {

		System.out.println("Enter the value of a...");
		Scanner scanner = new Scanner(System.in);
		int a = 10;

		Car c1 = null;

		if (a > 10) {
			c1 = new Volvo();
		} else {
			c1 = new Hyundai();
		}

		c1.startEngine();
		System.out.println(c1.a);
	}
}
