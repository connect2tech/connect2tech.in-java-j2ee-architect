package com.c2t.inheritance;

class Car {
	public void startEngine() {
		System.out.println("startEngine..");
	}
}

class Maruti extends Car {

}

class BMW extends Car {
	public void startEngine() {
		System.out.println("startEngine with more power..");
	}
}

class Zen extends Maruti {

}

public class IneritanceConcept {
	public static void main(String[] args) {

		Car b = null;
		int a = 4;
		
		if(a>5){
			b = new BMW();
		}else{
			b = new Maruti();
		}
		
		b.startEngine();
		

	}
}
