package com.c2t.scjp.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

public class ReadFileLineByLineUsingBufferedReader {

	public static void main(String[] args) {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(
					"REST.txt"));
			String line = reader.readLine();
			StringBuffer sbuf = new StringBuffer();
			while (line != null) {
				sbuf.append(line);
				//System.out.println(line);
				// read next line
				line = reader.readLine();
			}
			reader.close();
			
			System.out.println(sbuf);
			
			String s = sbuf.toString();
			StringTokenizer token = new StringTokenizer(s, "|");
			
			while (token.hasMoreTokens()) {
				System.out.println(token.nextToken());
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}