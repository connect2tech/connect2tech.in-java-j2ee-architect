package com.c2t.scjp.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ReadingFromFileCheckedException {

	public static void main(String[] args) throws IOException{
		readFromFile();
	}

	static void readFromFile() throws IOException{
		File file = new File("D:/Sample.txt");


			FileInputStream fis = new FileInputStream(file);

			int val = fis.read();
			System.out.println(val);
			System.out.println((char) val);


	}
}
