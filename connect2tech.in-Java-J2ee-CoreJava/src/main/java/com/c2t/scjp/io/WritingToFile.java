package com.c2t.scjp.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class WritingToFile {
	public static void main(String[] args) {

		File f = new File("D:/newfile.txt");

		try {
			FileOutputStream fos = new FileOutputStream(f);
			
			fos.write(67);//A, B, C

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			
			System.out.println(e);
		} catch (IOException e2) {
			System.out.println(e2);
		}
		
		System.out.println("Done...");

	}
}
