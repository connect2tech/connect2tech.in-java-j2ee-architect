package com.c2t.scjp.oops;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class SuperClass {

	public SuperClass() {
	}

	public SuperClass(int i) {
	}

	public void test() {
		System.out.println("super class test method");
	}
}

public class ChildClass extends SuperClass {

	public ChildClass(String str) {
		// access super class constructor with super keyword
		super();

		// access child class method
		test();

		// use super to access super class method
		super.test();
	}

	@Override
	public void test() {
		System.out.println("child class test method");
	}

	public static void main(String[] args) {
		ChildClass cc = new ChildClass("CC");
	}
}
