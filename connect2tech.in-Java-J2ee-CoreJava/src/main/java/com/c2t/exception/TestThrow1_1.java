package com.c2t.exception;

public class TestThrow1_1 {

	public static void main(String[] args) {
		int balance = 0;

		try {
			if (balance <= 0) {
				// System.out.println("insufficient balance...");
				throw new Exception("Alert. Insufficient balance !!!");
			} else {
				System.out.println("Welcome to menu...");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		

		System.out.println("Done with the code above...");
	}

}
