package com.c2t.exception;

import java.io.*;

class M1 {
	
	public void m1() throws Exception{
		
		int salary = 500;
		
		if(salary>1000){
			System.out.println("Withdraw...");
		}else{
			throw new Exception("Insufficient balance...");
		}
		
	}
	
	public void m2() throws Exception{
		
		m1();
	}
	
	
	
}

public class TestThrows3 {
	public static void main(String args[]) {// declare
			
		M1 m = new M1();// exception
		
	
	}
}