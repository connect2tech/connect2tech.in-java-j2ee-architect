package com.c2t.exception;

class CustomArithmeticExcetion extends RuntimeException {

	String errMsg;

	CustomArithmeticExcetion(String s) {
		super(s);
		errMsg = s;
	}

	public void displayMessage() {
		System.out.println(errMsg);
	}

}

public class CustomExceptions {
	public static void main(String[] args) {
		try {
			int a = 10 / 0;
		} catch (Exception e) {
			// TODO: handle exception
			CustomArithmeticExcetion ex = new CustomArithmeticExcetion("exception happened. Take corrective actions");
			System.out.println(ex.errMsg);

		}
	}
}