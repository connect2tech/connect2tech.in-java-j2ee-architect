package com.c2t.exception;

public class ExceptionWithFinally {

	public static void main(String[] args) {

		int a = 10;
		int b = 20;
		int c = 0;
		int d = 0;

		System.out.println("Performing division");
		int x[] = { 1, 2 };

		int y = getValue();
		if(y == 100){
			System.out.println("value is from finally block!!!. Division Failed...");
		}

		System.out.println("Value of d = " + d);

		// java.lang.ArithmeticException

	}

	static int getValue() {
		int a = 0;
		try {
			a = 10 / 0;
		} catch(Exception e){
			System.out.println(e);
		}
		finally {
			if (a == 0)
				return 100;
		}

		return a;
	}

}
