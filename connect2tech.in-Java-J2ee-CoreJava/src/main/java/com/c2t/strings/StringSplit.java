package com.c2t.strings;

import java.util.Arrays;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class StringSplit {
	public static void main(String[] args) {
		String s = "abcadada";
		System.out.println(Arrays.toString(s.split("a")));
	}
}
