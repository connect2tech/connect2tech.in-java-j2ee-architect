package org.paumard.stream;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 *
 * @author José
 */
public class FlatMapExample {

    public static void main(String... args) {
        
        List<Integer> list1 = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
        List<Integer> list2 = Arrays.asList(20, 40, 60);
        List<Integer> list3 = Arrays.asList(30, 50, 70);
        
        List<List<Integer>> list = Arrays.asList(list1, list2, list3);
        
        System.out.println(list);
        System.out.println("----------------------------------------------");
        
        Function<? super List<Integer>, ? extends Integer> mapper1 = l -> l.size();
        Function<List<Integer>, Integer> mapper2 = l -> l.size();
        //<R> Stream<R> map(Function<? super T, ? extends R> mapper);
        //list.stream().map(mapper1).forEach(System.out::println);
        
        
        //Function<? super List<Integer>, ? extends Stream<? extends Integer>> flatMapper = l -> l.stream();
        //Function<List<Integer>, Stream<? extends Integer>> flatMapper = l -> l.stream();
        Function<List<Integer>, Stream<Integer>> flatMapper = l -> l.stream();
        System.out.println(list.stream().flatMap(flatMapper).distinct().count());
        
        System.out.println("----------------------------------------------");
        
        list.stream().flatMap(flatMapper).forEach(System.out::println);
        
        //<R> Stream<R> flatMap(Function<? super T, ? extends Stream<? extends R>> mapper);
        
        //list.stream()
        //.flatMap();
        
        /*Function<List<?>, Integer> size = List::size;
        Function<List<Integer>, Stream<Integer>> flatmapper = 
                l -> l.stream();
        
        list.stream()
                .flatMap(flatmapper)
                .forEach(System.out::println);*/
	}
}
