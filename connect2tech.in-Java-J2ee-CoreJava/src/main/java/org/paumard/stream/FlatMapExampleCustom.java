package org.paumard.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import in.connect2tech.java8.paumard.stream.Person;

/**
 *
 * @author José
 */
public class FlatMapExampleCustom {

	public static void main(String... args) {

		List<Integer> list1 = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
		List<Integer> list2 = Arrays.asList(20, 40, 60);
		List<Integer> list3 = Arrays.asList(30, 50, 70);

		Person p1 = new Person("A", 10);
		Person p2 = new Person("B", 20);
		Person p3 = new Person("C", 30);

		List<Person> persons = new ArrayList<Person>();
		persons.add(p1);
		persons.add(p2);
		persons.add(p3);

		List<List<Integer>> list = Arrays.asList(list1, list2, list3);

		System.out.println(list);
		System.out.println("----------------------------------------------");

		Function<? super List<Integer>, ? extends Integer> mapper1 = l -> l.size();
		Function<List<Integer>, Integer> mapper2 = l -> l.size();
		// <R> Stream<R> map(Function<? super T, ? extends R> mapper);
		// list.stream().map(mapper1).forEach(System.out::println);

		Function<Person, Integer> personAge = person -> person.getAge();
		Integer minGt10 = persons.stream().map(personAge).filter(age -> age > 10).min(Comparator.naturalOrder()).get();
		System.out.println("minGt10==>"+minGt10);

		// -----------------------------------------------------------

		// Function<? super List<Integer>, ? extends Stream<? extends Integer>>
		// flatMapper = l -> l.stream();
		// Function<List<Integer>, Stream<? extends Integer>> flatMapper = l ->
		// l.stream();
		Function<List<Integer>, Stream<Integer>> flatMapper = l -> l.stream();
		System.out.println("list.stream().flatMap(flatMapper).distinct().count()==>"
				+ list.stream().flatMap(flatMapper).distinct().count());

		Comparator<Integer> comparator = new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				// TODO Auto-generated method stub
				return Integer.compare(o1, o2);
			}

		};

		System.out.println("list.stream().flatMap(flatMapper).min(comparator)==>"
				+ list.stream().flatMap(flatMapper).min(comparator).get());

		System.out.println("----------------------------------------------");

		list.stream().flatMap(flatMapper).forEach(System.out::println);

	}
}
