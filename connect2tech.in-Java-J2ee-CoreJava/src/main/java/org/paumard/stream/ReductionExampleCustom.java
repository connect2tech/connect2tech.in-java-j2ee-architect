package org.paumard.stream;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author José
 */
public class ReductionExampleCustom {

	public static void main(String... args) {

		List<Integer> list = new ArrayList<Integer>();
		list.add(10);
		list.add(20);

		Integer red = list.stream().reduce(0, (a, b) -> a + b);

		System.out.println("red = " + red);

	}
}
