package org.paumard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author José
 */
public class ChainConsumers {

	public static void main(String... args) {

		List<String> list = Arrays.asList("one", "two", "three", "four", "five");

		//Consumer<String> c1 = System.out::println;
		//matchConsumer<String> c2 = result::add;

		// strings.forEach(c1.andThen(c2));
		// System.out.println("size of result = " + result.size());


		Predicate<String> predicate = Predicate.isEqual("two");
		List<String> listOfStream = list.stream().filter(predicate).collect(Collectors.toList());

		System.out.println(listOfStream);
	}
}
