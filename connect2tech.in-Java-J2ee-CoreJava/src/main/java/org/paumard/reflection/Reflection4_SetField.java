package org.paumard.reflection;

import java.lang.reflect.Field;

public class Reflection4_SetField {

	public static void main(String[] args)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

		Person p = new Person();
		Class<?> c = p.getClass();

		Field name = c.getDeclaredField("name");
		name.setAccessible(true);
		name.set(p, "NC");

		System.out.println(p.getName());
	}

}
