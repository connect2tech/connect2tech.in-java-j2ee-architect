package org.paumard.reflection;

public class Reflection2 {

	public static void main(String[] args) {

		Class<?> c1 = "hello".getClass();
		Class<? extends String> c2 = "hello".getClass();
		Class<? extends Object> c3 = "hello".getClass();
		Class<? extends Integer> c4 = new Integer(10).getClass();
		Class<?> strC1 = String.class;

		String className = "java.lang.String";
		try {
			Class<?> strC2 = Class.forName(className);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
