package org.paumard.reflection;

public class Reflection1 {

	public static void main(String[] args) {
		String hello = "Hello";
		String world = "World";
		
		Class c1 = hello.getClass();
		Class c2 = world.getClass();
		
		if(c1 == c2) {
			System.out.println(c1 == c2);
		}

	}

}
