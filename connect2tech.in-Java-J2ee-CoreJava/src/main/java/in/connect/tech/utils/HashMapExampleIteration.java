package in.connect.tech.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class HashMapExampleIteration {
	public static void main(String[] args) {

		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("key1", "value1");
		hm.put("key2", "value2");
		
		
		Set <String> s = hm.keySet();
		
		System.out.println("s="+s);
		
		Iterator<String> iter = s.iterator();

		while (iter.hasNext()) {
			String key = iter.next();
			System.out.println("key="+key);
			
			System.out.println("val="+hm.get(key));
		}
		
		

	}
}
