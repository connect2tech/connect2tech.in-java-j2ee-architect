package in.connect.tech.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class HashMapExample {
	public static void main(String[] args) {

		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("key1", "value1");
		hm.put("key2", "value2");
		
		System.out.println(hm);
		
		
		Set <String> s = hm.keySet();
		
		Iterator <String> iter = s.iterator();
		while(iter.hasNext()){
			String key = iter.next();
			String value = hm.get(key);
			
			System.out.println("key="+key);
			System.out.println("value="+value);
			System.out.println("---------------------");
		}
		

	}
}
