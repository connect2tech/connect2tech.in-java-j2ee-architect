package in.connect.tech.utils;

import java.util.*;

class Employee {

	String name;
	String company;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
}

public class MyFirstHashMap {
	public static void main(String[] args) {

		Map<String, Employee> m = new HashMap<String, Employee>();
		
		Employee employee1 = new Employee();
		employee1.name="A";
		employee1.company="A Ltd";
		
		Employee employee2 = new Employee();
		employee2.name="B";
		employee2.company="B Ltd";
		
		
		m.put("emp1", employee1);
		m.put("emp2", employee2);
		
		
		Employee temp = m.get("emp1");
		/*System.out.println(temp.company);
		System.out.println(temp.name);*/
		
		Set <String> set = m.keySet();
		
		Iterator <String> iter = set.iterator();
		
		while(iter.hasNext()){
			
			String key = iter.next();
			
			Employee emp  = m.get(key);
			
			System.out.println(emp.company);
			System.out.println(emp.name);
			
		}
		
		m.containsKey("emp1");
		
		
		
		

		/*
		 * Set<String> keySet = loans.keySet();
		 * 
		 * Iterator<String> keySetIterator = keySet.iterator();
		 * 
		 * while (keySetIterator.hasNext()) {
		 * System.out.println("------------------------------------------------"
		 * ); System.out.println("Iterating Map in Java using KeySet Iterator");
		 * String key = keySetIterator.next(); System.out.println("key: " + key
		 * + " value: " + loans.get(key)); }
		 * 
		 */

		
		
		/*Set<Map.Entry<String, Employee>> entrySet = m.entrySet();

		for (Map.Entry entry : entrySet) {
			System.out.println("------------------------------------------------");
			System.out.println("looping HashMap in Java using EntrySet and java5 for loop");
			System.out.println("key: " + entry.getKey() + " value: " + entry.getValue());
		}*/

		/*
		 * Set<Map.Entry<String, String>> entrySet = loans.entrySet();
		 * 
		 * for (Map.Entry entry : entrySet) {
		 * System.out.println("------------------------------------------------"
		 * ); System.out.
		 * println("looping HashMap in Java using EntrySet and java5 for loop");
		 * System.out.println("key: " + entry.getKey() + " value: " +
		 * entry.getValue()); }
		 */

		/*
		 * Set<Map.Entry<String, Employee>> entrySet1 = m.entrySet();
		 * 
		 * Iterator<Map.Entry<String, Employee>> entrySetIterator =
		 * entrySet1.iterator();
		 * 
		 * while (entrySetIterator.hasNext()) {
		 * System.out.println("------------------------------------------------"
		 * ); System.out.
		 * println("Iterating HashMap in Java using EntrySet and Java iterator"
		 * ); Map.Entry entry = entrySetIterator.next();
		 * System.out.println("key: " + entry.getKey() + " value: " +
		 * entry.getValue()); }
		 */
	}
}
