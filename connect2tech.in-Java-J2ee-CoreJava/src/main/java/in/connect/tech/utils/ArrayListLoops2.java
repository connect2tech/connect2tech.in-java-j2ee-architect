package in.connect.tech.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;

public class ArrayListLoops2 {
	public static void main(String[] args) {
		
		
		ArrayList <String> list = new ArrayList<String>();
		list.add("A");
		list.add("B");
		list.add("C");
		list.add("A");
		System.out.println(list);
		
		for(int i=0;i<list.size();i++){
			System.out.println(list.get(i));
		}
		
		Iterator<String> iter = list.iterator();
		
		while(iter.hasNext()){
			String s = iter.next();
			System.out.println(s);
		}
		
	}
}
