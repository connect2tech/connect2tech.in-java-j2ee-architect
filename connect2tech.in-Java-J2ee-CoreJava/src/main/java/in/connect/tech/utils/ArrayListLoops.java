package in.connect.tech.utils;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayListLoops {
	public static void main(String[] args) {
		
		ArrayList <String> al  = new ArrayList<String>();
		
		al.add("A");
		al.add("B");
		al.add("A");
		
		System.out.println(al);
		
		//For loop
		/*for(int i=0;i<al.size();i++){
			System.out.println(al.get(i));
		}
*/		
		
		/*Iterator <String> iter = al.iterator();
		while(iter.hasNext()){
			String str = iter.next();
			System.out.println("str="+str);
		}*/
		
	}
}
