package in.connect.tech.utils;

import java.util.*;

public class MapIteration1 {
	public static void main(String[] args) {
		HashMap<String, String> students = new HashMap<String, String>();
		students.put("student1", "Prema");
		students.put("student2", "Leslie");
		students.put("student2", "Testing");
		System.out.println(students);
		
		
		/*Set <String> keys = students.keySet();
		Iterator <String> iterator = keys.iterator();
		
		while(iterator.hasNext()){
			String key = iterator.next();
			String value = students.get(key);
			
			System.out.println("Key="+key);
			System.out.println("Value = "+value);
		}*/
		
		Iterator iter = students.entrySet().iterator();
		while(iter.hasNext()){
			Map.Entry m = (Map.Entry)iter.next();
			System.out.println(m);
			System.out.println(m.getKey());
			System.out.println(m.getValue());
		}
		
		
		
		/*String s = students.get("student2");
		
		Set<String> setKeys = students.keySet();
		
		Iterator<String> keys = setKeys.iterator();
		
		while(keys.hasNext()){
			String key = keys.next();
			System.out.println("key="+key);
			System.out.println(students.get(key));
		}*/
		
		/*Set<String> keySet = loans.keySet();

		for (String key : keySet) {
			System.out.println("key::" + key + " , value::" + loans.get(key));
		}

		System.out.println("==============================================");

		Iterator<String> iter = keySet.iterator();
		while (iter.hasNext()) {
			String key = iter.next();
			System.out.println("key::" + key + " , value::" + loans.get(key));
		}*/
	}
}
