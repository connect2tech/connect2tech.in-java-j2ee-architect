package in.connect.tech.utils;

import java.util.*;

public class MyTreeSet {
	public static void main(String[] args) {
		Set <String> s1 = new TreeSet<String>();
		
		s1.add("Anamika");
		s1.add("Anamika");
		s1.add("Chandana");
		s1.add("Sagar");
		s1.add("Arpita");
		s1.add("Arpita");
		
		System.out.println(s1);
		
		Iterator <String> iter = s1.iterator();
		
		while(iter.hasNext()){
			String val = iter.next();
			System.out.println(val);
		}
		
	}
}
