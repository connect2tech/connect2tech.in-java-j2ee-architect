package in.connect2tech.assignments;

import java.util.Scanner;

public class ReverseString {
     public static void main(String[] args) {
        Scanner ip = new Scanner(System.in);
        System.out.print("Input a string: ");
        char[] letters = ip.nextLine().toCharArray();
        System.out.print("Reverse string: ");
        for (int i = letters.length - 1; i >= 0; i--) {
            System.out.print(letters[i]);
            ip.close();
        }
        System.out.print("\n");
    }
}