package in.connect2tech.assignments;

import java.util.Scanner;

public class SumDigits {

	public static void main(String[] args) {
		int sum=0;
		Scanner ip=new Scanner(System.in);
		System.out.println("Enter the number: ");
		int num=ip.nextInt();
		while (num>0)
		{
			int rem=num%10;
			sum=sum+rem;
			num=num/10;
			
		}
		
		System.out.println("The sum of digits of the number entered: "+  sum );
		ip.close();
	}

}
