package in.connect2tech.assignments;

public class SwapWithout {

	public static void main(String[] args) {
		
		int a=10;
		int b=9;
		System.out.println("The two numbers are: " + a + " and " + b);
		a = a+b;
        b=a-b;
        a=a-b;
        System.out.println("The two numbers after swap are: " + a + " and " + b);
	}

}
