package in.connect2tech.assignments;

import java.util.Scanner;

public class AreaCircle {

	public static void main(String[] args) {
		double pi = 3.14159;
		int r;
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the radius of the circle:");
		r = s.nextInt();

		double area = pi * r * r;
		System.out.println("Area of the circle is:" + area);
		double perimeter = 2 * pi * r;
		System.out.println("Perimeter of the circle is:" + perimeter);

		s.close();

	}
}
