package in.connect2tech.java.selenium.module1.primitives;

public class PrimitiveDataTypes {

	public static void main(String[] args) {
		//byte -128 to +127
		
		byte b1 = 10;
		byte b2 = -100;
		//byte b3 = 200;
		
		double d1 = 10.5;
		float f1 = 100.2f;
		
		boolean bool = true;
		char c1 ='A';

	}

}
