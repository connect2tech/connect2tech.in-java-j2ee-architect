package in.connect2tech.java.selenium.module2.functions;

public class CallByValue {

	public static void main(String[] args) {
	
		int a = 10;
		int b = 20;
		
		System.out.println("-------------Display before call a,b-----------------");
		
		System.out.println(a);
		System.out.println(b);
		sum(a, b);
		
		System.out.println("-------------Display after call a,b-----------------");
		
		System.out.println(a);
		System.out.println(b);

	}
	
	static void sum(int x, int y) {
		x = x + 10;
		y = y + 20;
		
		System.out.println("-----------x,y-------------");
		
		System.out.println(x);
		System.out.println(y);
		
	}

}
