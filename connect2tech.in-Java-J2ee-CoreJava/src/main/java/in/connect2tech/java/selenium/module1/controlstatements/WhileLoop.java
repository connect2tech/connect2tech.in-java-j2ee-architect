package in.connect2tech.java.selenium.module1.controlstatements;

public class WhileLoop {

	public static void main(String[] args) {

		int i = 5;
		while (i >= 0) {
			System.out.println(i);
			--i;
		}

		System.out.println("End of while loop.");
	}

}
