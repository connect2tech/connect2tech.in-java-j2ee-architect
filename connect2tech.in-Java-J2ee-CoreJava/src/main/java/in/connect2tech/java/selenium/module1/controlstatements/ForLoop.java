package in.connect2tech.java.selenium.module1.controlstatements;

public class ForLoop {

	public static void main(String[] args) {
		for (int i = 1; i < 5; i++) {
			System.out.println(i);
		}
	}

}
