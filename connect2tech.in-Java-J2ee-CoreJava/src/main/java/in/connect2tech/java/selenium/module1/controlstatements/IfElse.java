package in.connect2tech.java.selenium.module1.controlstatements;

public class IfElse {

	public static void main(String[] args) {
		int age = 10;

		if (age >= 18) {
			System.out.println("You are allowed to vote!!!");
		} else {
			System.out.println("Wait until you are 18 years old.");
		}
	}
}
