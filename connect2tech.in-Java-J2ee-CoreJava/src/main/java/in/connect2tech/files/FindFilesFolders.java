package in.connect2tech.files;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FindFilesFolders {
	public static void main(String[] args) {

		List<String> directories = new ArrayList<String>();
		// directories.add("D:\\nc\\Java-Architect");
		// directories.add("D:\\nc\\eclipse-workspace");
		// directories.add("D:\\nc\\Java-Architect\\connect2tech.in-java-j2ee-architect");
		//directories.add("C:\\Users\\naresh\\Downloads");
		directories.add("D:\\nchaurasia\\Java-Architect");
		

		for (int i = 0; i < directories.size(); i++) {
			getFolders(directories.get(i));

		}

	}

	public static void getFolders(String dir) {

		boolean displayFolders = true;
		boolean displayFiles = true;

		int count = 0;

		File folder = new File(dir);
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile() && displayFiles) {
				System.out.println(listOfFiles[i].getName());
			} else if (listOfFiles[i].isDirectory() && displayFolders) {

				String dirName = listOfFiles[i].getName();
				if (!dirName.startsWith(".")) {

					if (count == 0) {
						System.out.println("### " + listOfFiles[i].getParent() + " ###");
						++count;
					}

					System.out.println(" * " + listOfFiles[i].getName());
				}
			}
		}

		System.out.println("---");

	}
}
