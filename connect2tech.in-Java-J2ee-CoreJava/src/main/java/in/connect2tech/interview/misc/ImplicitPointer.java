package in.connect2tech.interview.misc;

class Animal{
	
	void method(){
		
		System.out.println("----inside method-----");
		System.out.println(this);
		
	}
	
}

public class ImplicitPointer {
	public static void main(String[] args) {
		Animal animal = new Animal();
		System.out.println("----1---"+animal);
		
		animal.method();
		
	}
}
