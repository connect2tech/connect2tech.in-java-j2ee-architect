package in.connect2tech.interview.java8.lambda;

public class Lambda_Thread {
	public static void main(String[] args) {

		Runnable r = () -> System.out.println("Starting new thread");

		new Thread(r).start();
	}

}
