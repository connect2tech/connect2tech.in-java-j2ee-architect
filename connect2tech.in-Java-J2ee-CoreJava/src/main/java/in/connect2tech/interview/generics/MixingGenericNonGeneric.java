package in.connect2tech.interview.generics;

import java.util.*;

/**
 * 
 * @author Naresh Chaurasia
 * 
 *         <p>
 *         When assigning non-generic to generic Note, the following warning is
 *         generate at run time.
 * 
 *         <p>
 *         MixingGenericNonGeneric.java uses unchecked or unsafe operations.
 *         Note: Recompile with -Xlint:unchecked for details.
 *
 */
public class MixingGenericNonGeneric {
	public static void foo1(Collection c) {
	}

	public static void foo2(Collection<Integer> c) {
	}

	public static void main(String[] args) {

		// List <String> s = new ArrayList();

		Collection<Integer> coll = new ArrayList<Integer>();
		foo1(coll);

		/*
		 * ArrayList lst = new ArrayList(); foo2(lst);
		 */
	}
}