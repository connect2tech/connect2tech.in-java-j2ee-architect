package in.connect2tech.interview.generics;

import java.util.*;

class Music implements Comparable<Music>, Comparator<Music>{
	String name;

	Music(String musicName) {
		name = musicName;
	}

	void display() {
		System.out.println("name=" + name);
	}

	public String toString() {
		return name;
	}

	@Override
	public int compareTo(Music o) {
		// TODO Auto-generated method stub

		return this.name.compareTo(o.name);

	}

	@Override
	public int compare(Music o1, Music o2) {
		// TODO Auto-generated method stub
		return 0;
	}
}

class SortByName implements Comparator<Music> {

	@Override
	public int compare(Music o1, Music o2) {
		return o1.name.compareTo(o2.name);
	}

}

public class PlayWithGenerics {
	public static void main(String[] args) {
		//sortString();
		//sortMusic();
		
		simpleGeneric();
	}
	
	private static void callSimpleGeneric(List <String> s){
		s.add("Boo");
		System.out.println(s);
	}
	
	private static void simpleGeneric(){
		List<String> myList = new ArrayList<String>();
		myList.add("Fred");
		callSimpleGeneric(myList);
	}

	private static void sortString() {

		List<String> strings = new ArrayList<String>();
		strings.add("one");
		strings.add("two");

		System.out.println("strings before sorting==>" + strings);

		Collections.sort(strings);

		System.out.println("strings after sorting==>" + strings);
	}

	private static void sortMusic() {

		List<Music> music = new ArrayList<Music>();
		
		Set<Music> set = new HashSet<Music>();

		Music music1 = new Music("Atif Aslam");
		Music music2 = new Music("Mohit Chauhan");
		Music music3 = new Music("Atif Aslam");

		music.add(music2);
		music.add(music1);
		music.add(music3);

		System.out.println("music=>" + music);
		Collections.sort(music);
		
		SortByName sort = new SortByName();
		
		Collections.sort(music, sort);
		System.out.println("music=>" + music);
		
		set.addAll(music);
		
		System.out.println("set=>"+set);
	}
}
