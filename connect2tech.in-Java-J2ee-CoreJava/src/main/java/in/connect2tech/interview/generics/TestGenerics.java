package in.connect2tech.interview.generics;

public class TestGenerics<T> { // as the class type
	T anInstance; // as an instance variable type
	T[] anArrayOfTs; // as an array type

	TestGenerics(T anInstance) { // as an argument type
		this.anInstance = anInstance;
	}

	T getT() { // as a return type
		return anInstance;
	}
}