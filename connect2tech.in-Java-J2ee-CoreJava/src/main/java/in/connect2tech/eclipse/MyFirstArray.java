package in.connect2tech.eclipse;

public class MyFirstArray {
	public static void main(String[] args) {
		
		int a[] = new int[5]; // 4*5
		
		for(int i=0;	i<5;	i++){
			a[i] = i + 10;
			System.out.println(a[i]);
		}
		
	}
}
