package in.connect2tech.eclipse;

import java.util.*;

class AnimalClass {
	public void eat() {

	}
}

class Cat1 extends AnimalClass {

}

class Dog1 extends AnimalClass {

}

public class Arrays {
	public static void main(String[] args) {
		AnimalClass Animal1[] = { new Dog1() };
		sortingArray(Animal1);

		/*List<Dog1> Dog1s = new ArrayList<Dog1>();
		Dog1s.add(new Dog1());
		sortingList(Dog1s);*/

	}

	private static void sortingArray(AnimalClass Animal1[]) {
		Animal1[0] = new Cat1();
		System.out.println(Animal1[0]);
	}

	private static void sortingList(List<? extends AnimalClass> Animal1s) {
		//Animal1s.add(new Dog1());
		System.out.println(Animal1s.get(0));
	}
}