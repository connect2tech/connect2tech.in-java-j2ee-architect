package in.connect2tech.eclipse;

import java.util.*;

/**
 * 
 * @author Naresh Chaurasia
 *         <p>
 *         Passing a type safe List as argument to a non-type safe List to
 *         method void insert(List list). The code will compile successfully,
 *         without any compilation warning.
 * 
 *         <p>
 *         Passing a type safe List (e.g. Integer) as argument to a non-type
 *         safe List to method void insert(List list). If in the insert method,
 *         we try to add any value, then it will give compile time warning.
 * 
 *         <p>
 *         We can pass generic list to non-generic. We can pass non-generic list to
 *         generic.
 * 
 */
public class TestBadLegacy {
	public static void main(String[] args) {
		
		Inserter in = new Inserter();
		
		List<Integer> myList = new ArrayList<Integer>();
		myList.add(4);
		myList.add(6);
		in.addIllegal(myList);
		//in.addLegal(myList);

		/*List l = new ArrayList();
		l.add(10);
		l.add(20);
		in.passNonSafeToSafe(l);*/
	}
}

class Inserter {
	// method with a non-generic List argument
	void addIllegal(List list) {
		//list.add(new String("42")); // adds to the incoming list
	}

	/*void addLegal(List list) {
		list.add(10); // adds to the incoming list
	}

	void passNonSafeToSafe(List<String> list) {

		for(String s: list){
			System.out.println(s);
		}

	}*/
}