package in.connect2tech.eclipse;

import java.util.TreeSet;

class Dance{
	
}

/**
 * @author nchaurasia
 *
 */
public class TreeSetGenerics {
	public static void main(String[] args) {
		TreeSet<Dance> set = new TreeSet<Dance>();
		
		Dance d1 = new Dance();
		Dance d2 = new Dance();
		set.add(d1);
		set.add(d2);
		System.out.println(set);
	}
}
