package in.connect2tech.youtube.java5.generic;

import java.util.ArrayList;
import java.util.List;

public class NonGenericExample {
	public static void main(String[] args) {
		List languages = new ArrayList();
		languages.add("Java");
		languages.add("Python");
		//Typecasting
		String value = (String)languages.get(0);
		System.out.println(value);
	}
}
