package in.connect2tech.scjp18.ch2;

interface Shape {
	public abstract void draw();

	public default void color() {
		System.out.println("Color me Red!!!");
	}

	public static void chooseShape() {
		System.out.println("Enter your shape choice...");
	}
}

interface Shape2 {

	public default void color() {
		System.out.println("Color me Red!!!");
	}

}

class Circle implements Shape, Shape2 {

	@Override
	public void draw() {
		System.out.println("I am circle...");
	}

	public static void chooseShape() {
		System.out.println("Enter your shape choice2...");
	}
	
	public void color() {
		System.out.println("I am not default color...");
	}
}

public class Interfaces2 {
	public static void main(String[] args) {
		Shape s = new Circle();
		s.color();
		s.draw();
		Shape.chooseShape();
	}
}
