package in.connect2tech.enums;

enum UserStatus {
	PENDING, ACTIVE, INACTIVE, DELETED;
}

enum WhoisRIR {
	ARIN("whois.arin.net"), RIPE("whois.ripe.net"), APNIC("whois.apnic.net"), AFRINIC("whois.afrinic.net"),
	LACNIC("whois.lacnic.net"), JPNIC("whois.nic.ad.jp"), KRNIC("whois.nic.or.kr"), CNNIC("ipwhois.cnnic.cn"),
	UNKNOWN("");

	private String url;

	WhoisRIR(String url) {
		this.url = url;
	}

	public String url() {
		return url;
	}
}

public class Enum1 {
	public static void main(String[] args) {

		// ACTIVE
		System.out.println(UserStatus.ACTIVE);
		System.out.println(WhoisRIR.ARIN.url());

		WhoisRIR rir = WhoisRIR.RIPE;

		switch (rir) {
		case ARIN:
			System.out.println("This is ARIN");
			break;
		case APNIC:
			System.out.println("This is APNIC");
			break;
		case RIPE:
			System.out.println("This is RIPE");
			break;
		default:
			throw new AssertionError("Unknown RIR " + rir);

		}
	}
}
