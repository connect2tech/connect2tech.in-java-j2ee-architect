package in.connect2tech.java8.stream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Java8StreamToList {

	public static void main(String args[]) throws IOException {
		Stream<String> streamOfString = Stream.of("Java", "C++", "JavaScript", "Scala", "Python");

		// converting Stream to List using Collectors.toList() method
		streamOfString = Stream.of("code", "logic", "program", "review", "skill");
		List<String> listOfStream = streamOfString.collect(Collectors.toList());

		System.out.println("Java 8 Stream to List, 1st example : " + listOfStream);

		// Java 8 Stream to ArrayList using Collectors.toCollection method
		streamOfString = Stream.of("one", "two", "three", "four", "five");
		listOfStream = streamOfString.collect(Collectors.toCollection(ArrayList::new));
		System.out.println("Java 8 Stream to List, 2nd Way : " + listOfStream);

		// 3rd way to convert Stream to List in Java 8
		streamOfString = Stream.of("abc", "cde", "efg", "jkd", "res");
		ArrayList<String> list = new ArrayList<>();
		streamOfString.forEach(list::add);
		System.out.println("Java 8 Stream to List,3rd Way : " + list);
		
		

	}
}