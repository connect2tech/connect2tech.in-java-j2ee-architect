package in.connect2tech.java8;

interface MyInterface1 {
	public abstract void method_abstract();

	public default void method_default() {
		System.out.println("I am method_default2");
	}

	public static void method_static() {
		System.out.println("I am method_static");
	}
}

class MyClass1 implements MyInterface1{

	@Override
	public void method_abstract() {
		System.out.println("overrident method_abstract");
		
	}
	
}

public class Interface_Basic {
	public static void main(String[] args) {
		MyInterface1.method_static();
		
		MyInterface1 c1 = new MyClass1();
		c1.method_abstract();
		
		c1.method_default();
	}
}
