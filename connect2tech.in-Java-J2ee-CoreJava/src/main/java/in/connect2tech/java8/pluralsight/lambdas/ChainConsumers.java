package in.connect2tech.java8.pluralsight.lambdas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ChainConsumers {
	public static void main(String... args) {

		List<String> strings = Arrays.asList("one", "two", 
				"three", "four", "five");

		List<String> list = new ArrayList<>();

		Consumer<String> c1 = System.out::println;
		Consumer<String> c2 = list::add;

		//strings.forEach(c1.andThen(c2));
		
		strings.stream().forEach(c1.andThen(c2));
		System.out.println(list);
	}
}


