package in.connect2tech.java8.pluralsight.lambdas;

import java.util.Comparator;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.IntBinaryOperator;

public class ConsumerLambdaCustom {

	public static <T> void methodRef(T t) {
		System.out.println(t);
	}

	public static void method() {

		Consumer<String> custom_consumer = ConsumerLambdaCustom::methodRef;
		custom_consumer.accept("Lambdas");

		// It should return BiFunction
		Comparator<Integer> c = Integer::compare;
		System.out.println(c.compare(10, 10));

		IntBinaryOperator c2 = Integer::compare;
		System.out.println(c2.applyAsInt(10, 20));

		BinaryOperator<Integer> c3 = Integer::compare;
		System.out.println(c3.apply(10, 20));


	}

	// Test the file filtering
	public static void main(String[] args) {
		ConsumerLambdaCustom.method();
	}
}