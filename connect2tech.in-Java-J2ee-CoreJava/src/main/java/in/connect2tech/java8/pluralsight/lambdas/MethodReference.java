package in.connect2tech.java8.pluralsight.lambdas;

import java.util.ArrayList;
import java.util.List;
import java.util.function.*;

import in.connect2tech.java8.paumard.stream.Person;

public class MethodReference {

	public static void main(String[] args) {

		List<Person> list = new ArrayList<Person>();

		list.add(new Person("A", 10));
		list.add(new Person("B", 20));

		Function<Person, Integer> f1 = Person::getAge;
		Function<Person, Integer> f2 = p -> p.getAge();
		method(f1, list.get(0));

		BinaryOperator<Integer> bi1 = (I1, I2) -> I1 + I2;
		BinaryOperator<Integer> bi2 = (I1, I2) -> Integer.sum(I1, I2);
		BinaryOperator<Integer> bi3 = Integer::sum;

	}

	static void method(Function<Person, Integer> f, Person p) {
		System.out.println(f.apply(p));
	}

}
