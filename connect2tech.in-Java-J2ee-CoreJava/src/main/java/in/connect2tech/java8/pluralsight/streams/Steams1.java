package in.connect2tech.java8.pluralsight.streams;

import java.util.*;
import java.util.stream.Stream;

/**
 * List->Steam->forEach(Lambda)
 */
public class Steams1 {
	public static void main(String str[]) {
		List<String> list = new ArrayList<>();
		list.add("A");
		list.add("B");

		// This statement opens a stream on list
		Stream<String> stream = list.stream();
		stream.forEach(s -> System.out.println(s));
		System.out.println("--------------------------------");
		stream.forEach(System.out::println);

	}
}
