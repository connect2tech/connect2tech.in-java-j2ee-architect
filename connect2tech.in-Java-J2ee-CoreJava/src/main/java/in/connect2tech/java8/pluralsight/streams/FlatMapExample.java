package in.connect2tech.java8.pluralsight.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import in.connect2tech.java8.paumard.stream.Person;

/**
 *
 * @author José
 */
public class FlatMapExample {

	public static void main(String... args) {
		
		Person p1 = new Person("A",10);
		Person p2 = new Person("B",20);
		
		List <Person> personList = new ArrayList<Person>();
		personList.add(p1);
		personList.add(p2);
		
		//map(Function<? super Person, ? extends String> mapper)
		Stream <Person> stream = personList.stream();
		stream.map(person->person.getName()).forEach(System.out::println);
		
		System.out.println("-----------------------------------------------");
		
		
		List<Integer> list1 = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
		List<Integer> list2 = Arrays.asList(2, 4, 6);
		List<Integer> list3 = Arrays.asList(3, 5, 7);

		List<List<Integer>> list = Arrays.asList(list1, list2, list3);

		// List<?> - Input to the function
		// Integer - Return from the function
		Function<List<?>, Integer> size = l -> l.size();
		Function<List<?>, Integer> size2 = List::size;
		
		list.stream().map(size).forEach(System.out::println);
		System.out.println("-----------------------------------------------");
		
		Function<List<Integer>, Stream<Integer>> flatMapper = l->l.stream();
		list.stream().flatMap(flatMapper).forEach(System.out::println);
		
		
		
	}
}
