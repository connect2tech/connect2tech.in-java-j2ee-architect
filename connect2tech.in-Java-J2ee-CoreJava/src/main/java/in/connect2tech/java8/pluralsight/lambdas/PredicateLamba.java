package in.connect2tech.java8.pluralsight.lambdas;

import java.util.Objects;
import java.util.function.Predicate;

public class PredicateLamba {
	public void method() {

		Predicate<String> predicate = new Predicate<String>() {
			@Override
			public boolean test(String t) {
				return t.length() > 10;
			}
		};

		System.out.println(predicate.test("Naresh"));

		Predicate<String> predicateLambda = (String t) -> t.length() > 10;
		System.out.println(predicateLambda.test("My Heer"));
		
		Predicate<String> predicateLambda2 = (t) -> t.length() < 20;
		System.out.println(predicateLambda2.test("You are My Heer"));

		Predicate<String> predicateLambda3 = predicateLambda.and(predicateLambda2);
		System.out.println(predicateLambda3.test("Heer, Really"));

		//Predicate<String> predicateLambda5 = Objects::isNull;
		
		Predicate<String> predicateLambda4 = Predicate.isEqual("Hello");
		System.out.println(predicateLambda4.test("Hello"));

	}

	// Test the file filtering
	public static void main(String[] args) {
		PredicateLamba consumer = new PredicateLamba();
		consumer.method();
	}
}