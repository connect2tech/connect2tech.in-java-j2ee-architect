package in.connect2tech.java8.pluralsight.lambdas;

import java.util.Comparator;

public class ComparatorLambda {
	public void compareValues() {

		Comparator<String> compare = new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		};
		
		
		System.out.println(compare.compare("o1", "o2"));

		Comparator<String> compareLambda = (String o1, String o2) -> {
			return o1.compareTo(o2);
		};
		
		
		System.out.println(compareLambda.compare("o1", "o1"));

		Comparator<String> compareLambda2 = (o1, o2) -> {
			return o1.compareTo(o2);
		};
		
		Comparator<String> co = (o1, o2) -> o1.compareTo(o2);
		
		System.out.println(compareLambda2.compare("o1", "o1"));

		Comparator<String> compareLambda3 = (o1, o2) -> o1.compareTo(o2);
		System.out.println(compareLambda3.compare("o1", "o1"));

		// Method References
		Comparator<Integer> compareLambda4 = Integer::compare;
		compareLambda4.compare(10, 20);

	}

	// Test the file filtering
	public static void main(String[] args) {
		ComparatorLambda obj = new ComparatorLambda();
		obj.compareValues();
	}
}