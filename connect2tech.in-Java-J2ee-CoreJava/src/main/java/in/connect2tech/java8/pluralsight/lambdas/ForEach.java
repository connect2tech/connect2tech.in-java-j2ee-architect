package in.connect2tech.java8.pluralsight.lambdas;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ForEach {
	public static void main(String[] args) {
		List<String> l = new ArrayList<String>();
		l.add("A");
		l.add("B");
		l.add("C");
		l.add("D");
		// default void forEach(Consumer<? super T> action)

		l.forEach(t -> System.out.println(t));
		System.out.println("-----------------------------------");
		l.forEach(System.out::println);
		System.out.println("-----------------------------------");

		Consumer<String> consumerA = t -> System.out.println(t);
		Consumer<String> consumerB = t -> System.out.println(t.toLowerCase());
		l.forEach(consumerA.andThen(consumerB));

	}
}


