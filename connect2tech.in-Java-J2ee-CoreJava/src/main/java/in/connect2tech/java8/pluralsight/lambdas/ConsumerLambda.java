package in.connect2tech.java8.pluralsight.lambdas;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

public class ConsumerLambda {
	
	
	public void method() {

		Consumer<String> consumer = new Consumer<String>() {
			@Override
			public void accept(String t) {
				System.out.println(t);
			}
		};

		consumer.accept("welcome to anonymous class");

		Consumer<String> lambdaConsumer = (String t) -> {
			System.out.println(t);
		};

		lambdaConsumer.accept("welcome to lambdas!!!");

		Consumer<String> lambdaConsumer_2 = (String t) -> System.out.println(t);
		lambdaConsumer_2.accept("welcome to lambdas1!!!");

		Consumer<String> lambdaConsumer_3 = (t) -> System.out.println(t);
		lambdaConsumer_3.accept("welcome to lambdas1!!!");
		
		//Method References
		Consumer<String> lambdaConsumer_4 = System.out::println;
		lambdaConsumer_4.accept("welcome to lambdas1!!!");
		
		List<String> list = new ArrayList<String>();
		list.add("A");
		list.add("B");
		
		System.out.println("Displaying list using consumer!!!");
		list.forEach(lambdaConsumer_4);
		


	}

	// Test the file filtering
	public static void main(String[] args) {
		ConsumerLambda consumer = new ConsumerLambda();
		consumer.method();
	}
}