package in.connect2tech.java8.pluralsight.lambdas;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import in.connect2tech.java8.paumard.stream.Person;

public class FilterOperation {
	public static void main(String... args) {

		List<Person> persons = new ArrayList<Person>();

		Person p1 = new Person("A", 10);
		Person p2 = new Person("B", 20);
		
		persons.add(p1);
		persons.add(p2);

		Predicate<Person> predicate = (Person p) -> {
			return p.getAge() > 10;
		};

		Stream <Person> filteredSteam = persons.stream().filter(predicate);

		filteredSteam.forEach(System.out::println);

	}
}
