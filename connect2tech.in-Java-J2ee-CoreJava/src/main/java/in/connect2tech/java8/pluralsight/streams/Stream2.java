package in.connect2tech.java8.pluralsight.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;


/**
 * Predicate->List->Steam->filter(Predicate)
 * Stream.forEach(Lambda)
 */
public class Stream2 {
	public static void main(String str[]) {
		List<String> list = new ArrayList<>();
		list.add("AA");
		list.add("B");
		list.add("A");

		
		Predicate <String> p1 = s -> s.contains("A");
		Predicate <String> p2 = s -> s.equals("AA");
		Predicate <String> p3 = p1.or(p2);
		
		Stream<String> stream = list.stream();
		Stream<String> fileterdStream = stream.filter(p3);
		fileterdStream.forEach(System.out::println);
	}
}
