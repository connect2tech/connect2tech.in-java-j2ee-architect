package in.connect2tech.java8.pluralsight.lambdas;

public class RunnableLambda {
	public void createThread() {

		Runnable runnable = new Runnable() {
			@Override
			public void run() {
			}
		};

		Runnable runnableLambda = () -> {
			for (int i = 0; i < 5; i++) {
				System.out.println(i);
			}
		};

		Thread t = new Thread(runnableLambda);
		t.start();
	}

	// Test the file filtering
	public static void main(String[] args) {
		RunnableLambda run = new RunnableLambda();
		run.createThread();
	}
}