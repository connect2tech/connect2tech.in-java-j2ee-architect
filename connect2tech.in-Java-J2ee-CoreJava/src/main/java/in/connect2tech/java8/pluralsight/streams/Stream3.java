package in.connect2tech.java8.pluralsight.streams;

import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 *  Predicate.isEqual->stream.filter(predicate).forEach(lambda)
 */
public class Stream3 {
	public static void main(String str[]) {
		Predicate<String> p = Predicate.isEqual("two");
		Stream<String> stream1 = Stream.of("one", "two", "three");
		Stream<String> stream2 = stream1.filter(p);
		stream2.forEach(System.out::println);
	}
}
