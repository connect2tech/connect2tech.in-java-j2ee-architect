package in.connect2tech.java8;

interface I1 {

	public default void m1_default() {
		System.out.println("I1 m1_default");
	}

}

interface I2 {
	public default void m1_default() {
		System.out.println("I2 m1_default");
	}
}

class C2 {
	public void m1_default() {
		System.out.println("C2 m1_default");
	}
}

class C1 extends C2 implements I1, I2 {

}

public class Interface2 {
	public static void main(String[] args) {
		I1 c1 = new C1();
		c1.m1_default();
	}
}
