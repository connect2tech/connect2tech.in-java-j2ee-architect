package in.connect2tech.java8.practical_coding;

import java.io.File;
import java.io.FileFilter;

public class FileFilterLambda {
	public void getFiles(String dir) {
		File directory = new File(dir);
		// Verify if it is a valid file name
		if (!directory.exists()) {
			System.out.println(String.format("Directory %s does not exist", dir));
			return;
		}
		// Verify if it is a directory and not a file path
		if (!directory.isDirectory()) {
			System.out.println(String.format("Provided value %s is not a directory", dir));
			return;
		}
		File[] files = directory.listFiles(lambdaFileFilter);
		// Let's list out the filtered files
		for (File f : files) {
			System.out.println(f.getName());
		}
	}

	// create a FileFilter and override its accept-method
	FileFilter fileFilter = new FileFilter() {
		// Override accept method
		public boolean accept(File file) {
			// if the file extension is .log return true, else false
			if (file.getName().endsWith(".txt")) {
				return true;
			}
			return false;
		}
	};

	FileFilter lambdaFileFilter = (File file) -> file.getName().endsWith(".txt");

	// Test the file filtering
	public static void main(String[] args) {
		FileFilterLambda ioUtils = new FileFilterLambda();
		ioUtils.getFiles("C:\\temp");
	}
}