package in.connect2tech.java8.practical_coding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ComparingTwoLists {
	public static void main(String[] args) {
		List<String> small = Arrays.asList("AA", "BB");
		List<String> big = Arrays.asList("a.b.AA", "a.b.BB", "a.b.CC");
		List<String> list = new ArrayList<String>();

		list = big.stream().filter(s -> small.stream().anyMatch(s::endsWith)).collect(Collectors.toList());

		System.out.println(list);
	}
}
