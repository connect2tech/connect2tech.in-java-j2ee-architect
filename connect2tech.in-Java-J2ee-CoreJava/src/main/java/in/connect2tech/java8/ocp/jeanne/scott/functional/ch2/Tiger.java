package in.connect2tech.java8.ocp.jeanne.scott.functional.ch2;

public class Tiger implements Sprint {
	public void sprint(Animal animal) {
		System.out.println("Animal is sprinting fast! " + animal.toString());
	}
}
