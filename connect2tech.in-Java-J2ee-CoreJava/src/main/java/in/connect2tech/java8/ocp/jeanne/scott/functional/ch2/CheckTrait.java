package in.connect2tech.java8.ocp.jeanne.scott.functional.ch2;

public interface CheckTrait {
	public boolean test(Animal a);
}