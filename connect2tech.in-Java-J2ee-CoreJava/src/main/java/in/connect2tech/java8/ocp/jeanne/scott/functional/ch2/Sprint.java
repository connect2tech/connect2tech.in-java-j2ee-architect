package in.connect2tech.java8.ocp.jeanne.scott.functional.ch2;

@FunctionalInterface
public interface Sprint {
	public void sprint(Animal animal);
}