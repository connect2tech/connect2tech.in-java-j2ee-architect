package in.connect2tech.java5.kathy.generics;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

import java.util.*;

class GenericsUtility {

	void addElementToList(List list) {
		list.add("hello");
	}

	int addListValues(List list) {
		// method with a non-generic List argument,
		// but assumes (with no guarantee) that it will be Integers
		Iterator it = list.iterator();
		int total = 0;
		while (it.hasNext()) {
			int i = ((Integer) it.next()).intValue();
			total += i;
		}
		return total;
	}
}

public class TestLegacyMixingGenericNonGeneric {
	public static void main(String[] args) {
		List<Integer> myList = new ArrayList<Integer>();
		//List myList2 = myList;
		
		myList.add(4);
		myList.add(6);
		GenericsUtility adder = new GenericsUtility();
		int total = adder.addListValues(myList);
		// pass it to an untyped argument
		System.out.println(total);
		
		adder.addElementToList(myList);
		adder.addListValues(myList);
		
	}
}
