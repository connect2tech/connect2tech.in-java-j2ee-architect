package in.connect2tech.java5.kathy.generics;

import java.util.*;

class Dogs{
	
}

public class TestWildcards {
	public static void main(String[] args) {
		List<Integer> myList = new ArrayList<Integer>();
		Bar bar = new Bar();
		//bar.doInsert(myList);
	}
}

class Bar {
	void doInsert(List<?> list) {
		list.add(null);
		System.out.println("Inside doInsert()");
	}
}