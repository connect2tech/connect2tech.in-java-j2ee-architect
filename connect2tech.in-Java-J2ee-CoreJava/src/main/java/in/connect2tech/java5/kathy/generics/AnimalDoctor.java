package in.connect2tech.java5.kathy.generics;

import java.util.*;

abstract class Animal {
	public abstract void checkup();
}

class Dog extends Animal {
	public void checkup() { // implement Dog-specific code
		System.out.println("Dog checkup");
	}
}

class Cat extends Animal {
	public void checkup() { // implement Cat-specific code
		System.out.println("Cat checkup");
	}
}

class Bird extends Animal {
	public void checkup() { // implement Bird-specific code
		System.out.println("Bird checkup");
	}
}

public class AnimalDoctor {
	// method takes an array of any animal subtype
	public void checkAnimals(Animal[] animals) {

		for (Animal a : animals) {
			a.checkup();
		}

		// Pass dog and change to cat
		// ArrayStoreException
		// animals[1] = new Cat();
		/*
		 * animals[0] = new Cat(); for (Animal a : animals) { a.checkup(); }
		 */

		/*
		 * animals[0] = new Cat(); System.out.println("I am here...");
		 */
	}

	public void checkAnimalsGeneric(List<Animal> animals) {
	}

	public void checkAnimalsGeneric2(List<? extends Animal> animals) {
		System.out.println("--Generic2--");
		// This is concious decision to mix them.
		// animals.add(new Dog());
		// animals.add(new Cat());
	}

	public static void nonGenericDogCatBirdArray() {
		// test it
		Dog[] dogs = { new Dog(), new Dog() };
		Cat[] cats = { new Cat(), new Cat(), new Cat() };
		Bird[] birds = { new Bird() };
		Animal[] mix = { new Bird(), new Dog(), new Cat() };
		AnimalDoctor doc = new AnimalDoctor();
		System.out.println("------------Dogs----------");
		doc.checkAnimals(dogs); // pass the Dog[]
		System.out.println("------------Cats----------");
		doc.checkAnimals(cats); // pass the Cat[]

		System.out.println("------------Birds----------");
		doc.checkAnimals(birds); // pass the Bird[]
		System.out.println("------------MIX----------");
		doc.checkAnimals(mix); // pass the Bird[]
	}

	public static void genericArray() {
		List<Dog> dogs = new ArrayList<Dog>();
		dogs.add(new Dog());
		dogs.add(new Dog());

		List<Cat> cats = new ArrayList<Cat>();
		cats.add(new Cat());
		cats.add(new Cat());

		List<Bird> birds = new ArrayList<Bird>();
		birds.add(new Bird());
		// this code is the same as the Array version
		AnimalDoctor doc = new AnimalDoctor();
		// this worked when we used arrays instead of ArrayLists

		// doc.checkAnimalsGeneric(dogs); // send a List<Dog>
		// doc.checkAnimalsGeneric(cats); // send a List<Cat>
		// doc.checkAnimalsGeneric(birds); // send a List<Bird>

//		List<Animal> animals = new ArrayList<Animal>();
//		animals.add(new Dog());
//		doc.checkAnimalsGeneric(animals);

		// Using wildcard, it can take sub-class.
		// doc.checkAnimalsGeneric2(birds);
	}

}