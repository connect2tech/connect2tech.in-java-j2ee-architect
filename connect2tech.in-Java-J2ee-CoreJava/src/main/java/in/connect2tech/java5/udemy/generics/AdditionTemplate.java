package in.connect2tech.java5.udemy.generics;

class Add<T> {
	<T> void add(T x, T y) {
		T sum;
		// sum = x + y;
	}
}

public class AdditionTemplate {

	public static void main(String[] args) {
		Add<Integer> add = new Add<>();
		add.add(10, 20);
	}

}
