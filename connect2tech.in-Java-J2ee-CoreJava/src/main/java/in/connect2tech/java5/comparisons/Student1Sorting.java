package in.connect2tech.java5.comparisons;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Student1Sorting {

	public static void main(String[] args) {
		// int id, String name, int age, int salary
		Student1 s1 = new Student1(12, "name", 20, 100);
		Student1 s2 = new Student1(11, "name", 21, 101);

		List<Student1> l = new ArrayList<>();
		l.add(s1);
		l.add(s2);

		System.out.println("---------------->"+l);

		Collections.sort(l, new Student1Comparator());

		System.out.println("---------------->"+l);
	}
}