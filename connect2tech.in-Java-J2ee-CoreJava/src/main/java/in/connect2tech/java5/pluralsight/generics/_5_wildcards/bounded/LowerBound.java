package in.connect2tech.java5.pluralsight.generics._5_wildcards.bounded;

import java.util.ArrayList;
import java.util.List;

public class LowerBound {

	static Partner donDraper = new Partner("Don Draper", 89);
	static Partner bertCooper = new Partner("Bert Cooper", 100);
	static Employee peggyOlson = new Employee("Peggy Olson", 65);

	public static void main(String[] args) {
		List<Object> persons = new ArrayList<>();
		// Adding Partner
		persons.add(donDraper);
		// Adding Employee
		persons.add(peggyOlson);
		loadAll(persons);
	}

	private static void loadAll(List<? super Person> persons) {
		// Put into the list is allowed.
		persons.add(donDraper);
	}

}


