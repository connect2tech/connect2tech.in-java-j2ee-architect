package in.connect2tech.java5.pluralsight.generic;

class MyClass {

}

public class UnboundedWildcards {
	public static void main(String[] args) throws ClassNotFoundException {
		String s = "MyClass.class";

		// get the Class instance using forName method
		Class <?> c1 = Class.forName("in.connect2tech.java5.pluralsight.generic.MyClass");

		System.out.print("Class represented by c1: " + c1.toString());
	}
}
