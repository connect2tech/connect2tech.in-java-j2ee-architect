package in.connect2tech.java5.pluralsight.generics._5_wildcards.bounded;

public class UnBoundedWildcard {

	public static void main(String[] args) throws ClassNotFoundException {

		Class<?> c = Class.forName("in.connect2tech.UpperBound");
		Class<? extends Object> c2 = UpperBound.class;
		
	}

}
