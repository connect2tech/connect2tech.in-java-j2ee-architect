package in.connect2tech.java5.pluralsight.non_generic;

public class StringStack {
	static final int MAX = 5;
	int top;
	String a[] = new String[MAX]; // Maximum size of Stack

	boolean isEmpty() {
		return (top < 0);
	}

	StringStack() {
		top = -1;
	}

	boolean push(String x) {
		if (top >= (MAX - 1)) {
			System.out.println("Stack Overflow");
			return false;
		} else {
			a[++top] = x;
			System.out.println(x + " pushed into stack");
			return true;
		}
	}

	String pop() {
		if (top < 0) {
			System.out.println("Stack Underflow");
			return null;
		} else {
			String x = a[top--];
			return x;
		}
	}

	String peek() {
		if (top < 0) {
			System.out.println("Stack Underflow");
			return null;
		} else {
			String x = a[top];
			return x;
		}
	}
}