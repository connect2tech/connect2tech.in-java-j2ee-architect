package in.connect2tech.java5.pluralsight.generics._5_wildcards.bounded;

import java.util.ArrayList;
import java.util.List;

public class UpperBound {
	public static void main(String[] args) {

		List<Partner> partners = new ArrayList<Partner>();
		savePerson1(partners);
		savePerson2(partners);

	}
	private static void savePerson1(List<? extends Person> persons) {
		for (Person person : persons) {
		}
	}

	private static <T extends Person> void savePerson2(List<T> persons) {
		for (Person person : persons) {
		}
	}
	static class PersonHolder<T extends Person> {
		T get() {
			return null;
		}
	}
}



