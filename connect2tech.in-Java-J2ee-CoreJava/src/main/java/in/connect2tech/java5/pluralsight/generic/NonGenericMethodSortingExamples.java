package in.connect2tech.java5.pluralsight.generic;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class NonGenericMethodSortingExamples
{
    public static void main(String[] args)
    {
        Person donDraper = new Person("Don Draper", 89);
        Person peggyOlson = new Person("Peggy Olson", 65);
        Person bertCooper = new Person("Bert Cooper", 100);

        List<Person> madMen = new ArrayList<>();
        madMen.add(donDraper);
        madMen.add(peggyOlson);
        madMen.add(bertCooper);

        //final Person youngestCastMember = (Person)min(madMen, new AgeComparator());
        final Person youngestCastMember = (Person)min(madMen, new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				// TODO Auto-generated method stub
				return 0;
			}
        	
		});

        System.out.println(youngestCastMember);
    }

    public static Object min(List values, Comparator comparator)
    {
        if (values.isEmpty())
        {
            throw new IllegalArgumentException("Unable to find the minimum of an empty list");
        }

        Object lowestFound = values.get(0);

        for (int i = 1; i < values.size(); i++)
        {
            final Object element = values.get(i);
            if (comparator.compare(element, lowestFound) < 0)
            {
                lowestFound = element;
            }
        }

        return lowestFound;
    }

}
