package in.connect2tech.java5.pluralsight.generic;

import java.util.Arrays;

public class EmployeeSortingComparator {

	/**
	 * This class shows how to sort primitive arrays, Wrapper classes Object Arrays
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Employee[] empArr = new Employee[4];
		empArr[0] = new Employee(10, "Mikey", 25, 10000);
		empArr[1] = new Employee(20, "Arun", 29, 20000);
		empArr[2] = new Employee(5, "Lisa", 35, 5000);
		empArr[3] = new Employee(1, "Pankaj", 32, 50000);

		// Arrays.sort(empArr);
		// System.out.println(Arrays.toString(empArr));

		EmployeeComparator sortAge = new EmployeeComparator();

		Arrays.sort(empArr, sortAge);
		System.out.println(Arrays.toString(empArr));
	}
}