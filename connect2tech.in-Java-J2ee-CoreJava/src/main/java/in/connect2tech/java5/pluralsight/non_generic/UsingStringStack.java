package in.connect2tech.java5.pluralsight.non_generic;

public class UsingStringStack {
	public static void main(String args[]) {
		StringStack s = new StringStack();
		s.push("10");
		/*s.push(20);
		s.push(30);*/
		System.out.println(s.pop() + " Popped from stack");
	}
}