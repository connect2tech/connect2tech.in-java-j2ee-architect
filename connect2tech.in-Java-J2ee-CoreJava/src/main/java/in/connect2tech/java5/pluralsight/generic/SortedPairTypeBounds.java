package in.connect2tech.java5.pluralsight.generic;

public class SortedPairTypeBounds<T extends Comparable<T>> {
	private T first;
	private T second;

	public SortedPairTypeBounds(T left, T right) {

		if (left.compareTo(right) < 0) {
			first = left;
			second = right;
		} else {
			first = right;
			second = left;
		}

	}

	public T getFirst() {
		return first;
	}

	public T getSecond() {
		return second;
	}

}
