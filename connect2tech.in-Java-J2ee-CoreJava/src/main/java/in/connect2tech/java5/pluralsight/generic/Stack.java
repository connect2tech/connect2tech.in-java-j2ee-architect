package in.connect2tech.java5.pluralsight.generic;

public class Stack<T> {
	static final int MAX = 5;
	int top;
	T a[] = (T[]) new Object[MAX]; // Casting our object array to T array

	boolean isEmpty() {
		return (top < 0);
	}

	Stack() {
		top = -1;
	}

	boolean push(T x) {
		if (top >= (MAX - 1)) {
			System.out.println("Stack Overflow");
			return false;
		} else {
			a[++top] = x;
			System.out.println(x + " pushed into stack");
			return true;
		}
	}

	Object pop() {
		if (top < 0) {
			System.out.println("Stack Underflow");
			return 0;
		} else {
			T x = a[top--];
			return x;
		}
	}

	Object peek() {
		if (top < 0) {
			System.out.println("Stack Underflow");
			return 0;
		} else {
			T x = a[top];
			return x;
		}
	}
}