package in.connect2tech.props;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadPropertiesFile {
	public static void main(String[] args) {

		try (InputStream input = new FileInputStream("src/main/java/in/connect2tech/props/config.properties")) {

			Properties prop = new Properties();

			// load a properties file
			prop.load(input);
			System.out.println(prop);

			
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}
}
