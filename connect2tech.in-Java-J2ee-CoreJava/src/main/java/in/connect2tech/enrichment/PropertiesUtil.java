package in.connect2tech.enrichment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

public class PropertiesUtil {

	public static void readArgumentFromPropertiesFile(String propFile) throws IOException {
		File f = new File(propFile);
		InputStream is = new FileInputStream(f);

		Properties prop = new Properties();
		prop.load(is);
		System.out.println(prop);

		Set keySet = prop.keySet();
		// keySet.forEach(System.out::println);

		RULE_ARGUMENTS args = RULE_ARGUMENTS.SOURCE_NODE;

		switch (args) {
		case SOURCE_NODE:
			System.out.println(RULE_ARGUMENTS.SOURCE_NODE.field());
			System.out.println(prop.get(RULE_ARGUMENTS.SOURCE_NODE.field()));
			constructArgumentValue((String) prop.get(RULE_ARGUMENTS.SOURCE_NODE.field()));
			break;
		case COMPARISON_CONSTANT_LIST:
			System.out.println("COMPARISON_CONSTANT_LIST");
			break;
		case DESTINATION_NODE:
			System.out.println("DESTINATION_NODE");
			break;
		case ASSIGNMENT_CONSTANT_LIST:
			System.out.println("ASSIGNMENT_CONSTANT_LIST");
			break;
		}
	}

	public static void constructArgumentValue(String value) {

		List l = new ArrayList();
		StringTokenizer tokens = new StringTokenizer(value, ":");

		while (tokens.hasMoreTokens()) {
			l.add(tokens.nextToken());
		}

		System.out.println("---------------------------------------");
		System.out.println(l);

	}

}
