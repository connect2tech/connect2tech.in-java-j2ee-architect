package in.connect2tech.enrichment;

public enum RULE_ARGUMENTS {
	SOURCE_NODE("if_else_rule_0.sourceNode"), COMPARISON_CONSTANT_LIST("if_else_rule_0.comparisonConstantList"),
	DESTINATION_NODE("if_else_rule_0.destinationNode"),
	ASSIGNMENT_CONSTANT_LIST("if_else_rule_0.assignmentConstantList");

	private String field;
	private String value;

	RULE_ARGUMENTS(String field) {
		this.field = field;
	}

	public String field() {
		return field;
	}

	public String toString() {
		return field;
	}
}
