package in.connect2tech.enrichment;

import java.util.List;

public class RuleEngine {
	
	
	public static void main(String[] args) {
		
	}

	public static boolean if_else_0_rule(String sourceNode, List<String> comparisonConstantList, String destinationNode,
			List<String> assignmentConstantList) {

		for (int i = 0; i < comparisonConstantList.size(); i++) {
			if (sourceNode.equals(comparisonConstantList.get(i))) {
				if (destinationNode.equals(assignmentConstantList.get(i))) {
					return true;
				}
			}
		}

		return false;
	}

}
