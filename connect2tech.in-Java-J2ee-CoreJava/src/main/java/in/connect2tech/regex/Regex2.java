package in.connect2tech.regex;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex2 {

	public static void main(String args[]) {
		// The regex pattern
		final String REGEX_PATTERN = "fooa*bar";
		final String inputFileName = "";
		// Create a Pattern object
		Pattern r = Pattern.compile(REGEX_PATTERN);

		// Read the input file line by line
		try {
			String line;
			FileReader fr = new FileReader("D:\\naresh.chaurasia\\Java-Architect\\connect2tech.in-Java-J2ee-Architect\\connect2tech.in-Java-J2ee-CoreJava\\src\\main\\java\\in\\connect2tech\\regex\\regex01.txt"); 
			BufferedReader br = new BufferedReader(fr);
			while ((line = br.readLine()) != null) {

				// Now create matcher object.
				Matcher m = r.matcher(line);

				// Apply the regex pattern to each line
				// If pattern matches, output the current line.
				if (m.find()) {
					System.out.println(line);
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
