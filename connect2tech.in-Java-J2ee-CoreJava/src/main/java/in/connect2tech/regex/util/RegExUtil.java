package in.connect2tech.regex.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExUtil {

	public static void matchRegExPattern(String REGEX_PATTERN, String inputFileName) {
		Pattern r = Pattern.compile(REGEX_PATTERN);

		// Read the input file line by line
		try {
			String line;
			FileReader fr = new FileReader(inputFileName);
			BufferedReader br = new BufferedReader(fr);
			while ((line = br.readLine()) != null) {

				// Now create matcher object.
				Matcher m = r.matcher(line);

				// Apply the regex pattern to each line
				// If pattern matches, output the current line.
				if (m.find()) {
					System.out.println("match =>"+line);
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	public static void replaceRegExMonitor(String REGEX_PATTERN, String INPUT) {
		Pattern p = Pattern.compile(REGEX_PATTERN);
		// get a matcher object
		Matcher m = p.matcher(INPUT);

		System.out.print(INPUT+"=>");
		if (m.find()) {
			INPUT = m.replaceAll(m.group(1) + " pix by " + m.group(2) + " pix");
		}

		System.out.println(INPUT);
	}

	public static void changeFNameLNameOrder(String REGEX_PATTERN, String INPUT) {
		Pattern p = Pattern.compile(REGEX_PATTERN);
		// get a matcher object
		Matcher m = p.matcher(INPUT);

		System.out.print(INPUT+"=>");
		if (m.find()) {
			INPUT = m.replaceAll(m.group(2) + " , " + m.group(1));
		}
		System.out.println(INPUT);
	}

	public static void changeClock(String REGEX_PATTERN, String INPUT) {
		Pattern p = Pattern.compile(REGEX_PATTERN);
		// get a matcher object
		Matcher m = p.matcher(INPUT);

		if (m.find()) {
			INPUT = m.replaceAll(m.group(2) + " mins past " + m.group(1));
		}

		System.out.println(INPUT);
	}

	public static void phoneMasking(String REGEX_PATTERN, String INPUT) {
		Pattern p = Pattern.compile(REGEX_PATTERN);
		// get a matcher object
		Matcher m = p.matcher(INPUT);

		if (m.find()) {
			INPUT = m.replaceAll("xxx.xxx." + m.group(1));
		}

		System.out.println(INPUT);
	}

	public static void phoneFormat(String REGEX_PATTERN, String INPUT) {

		Pattern p = Pattern.compile(REGEX_PATTERN);
		// get a matcher object
		Matcher m = p.matcher(INPUT);

		if (m.find()) {
			INPUT = m.replaceAll(m.group(1) + m.group(2));
		}

		System.out.println(INPUT);

	}

	public static void changeDateFormat(String REGEX_PATTERN, String INPUT) {
		Pattern p = Pattern.compile(REGEX_PATTERN);
		// get a matcher object
		Matcher m = p.matcher(INPUT);

		if (m.find()) {
			INPUT = m.replaceAll(m.group(2) + "-" + m.group(1) + "-" + m.group(3));
		}

		System.out.println(INPUT);
	}
}
