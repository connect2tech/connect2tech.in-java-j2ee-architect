enum playSetting {

	ONDATA("ondata"),

	OFFDATA("offdata"),

	STANDATA("standdata");

	private String settings;

	private playSetting(String settings) {
		this.settings = settings;
	}

	public String getValue() {
		return this.settings;
	}
}

public class Stack {
	public static void main(String[] args) {
		
		playSetting[] ps = playSetting.values();

		for (playSetting p : ps) {
			System.out.println(p);//ONDATA OFFDATA STANDATA
			//System.out.println(p.getValue());//ondata offdata standdata
		}
	}
}
