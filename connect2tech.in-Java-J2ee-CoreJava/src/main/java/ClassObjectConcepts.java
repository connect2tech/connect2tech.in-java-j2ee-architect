
/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class Person{
	String name;
	
	public void eat(){
		System.out.println("I can eat");
	}
}

public class ClassObjectConcepts {
	public static void main(String[] args) {
		
		Person p = new Person();
		p.name = "Java Selenium";
		p.eat();
		System.out.println(p.name);
		
		
	}
}
