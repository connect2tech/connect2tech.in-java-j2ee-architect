package connect2tech.jboss.drools.firealarm;

public class Room {
	private String name;
	
	Room(String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
