package connect2tech.jboss.drools.firealarm;

public class Fire {
	private Room room;
	
	Fire(Room room){
		this.room = room;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}
	
	
}
