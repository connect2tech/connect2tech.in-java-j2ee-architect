package in.connect2tec.csv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.opencsv.CSVWriter;

public class WriteCsv {

	public static void main(String str[]) throws Exception {
		// writeDataLineByLine("my.csv");
		String file = readFileAsString("Question1.cpp");
		String[] data = splitString(file);
		String csv_name = "cpp" + new Date().getTime() + ".xlsx";
		// writeDataLineByLine(csv_name, data);
		writeToExcel(csv_name, data);

		System.out.println("Done...");
	}

	public static String readFileAsString(String file) throws Exception {
		InputStream is = new FileInputStream(file);
		BufferedReader buf = new BufferedReader(new InputStreamReader(is));

		String line = buf.readLine();
		StringBuilder sb = new StringBuilder();

		while (line != null) {
			sb.append(line).append("\n");
			line = buf.readLine();
		}

		String fileAsString = sb.toString();
		System.out.println("Contents : " + fileAsString);

		return fileAsString;
	}

	public static void writeToExcel(String filePath, String[] data) throws FileNotFoundException, IOException {

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Java Books");

		int rowCount = 0;
		int columnCount = 0;

		Row row = sheet.createRow(rowCount);
		for (int i = 0; i < data.length; i++) {

			Cell cell = row.createCell(columnCount);

			sheet.setColumnWidth(i, 5000*2);
			CellStyle style = workbook.createCellStyle(); // Create new style
			style.setWrapText(true); // Set wordwrap

			String field = data[i];
			cell.setCellValue(field);
			++columnCount;
		}

		try (FileOutputStream outputStream = new FileOutputStream(filePath)) {
			workbook.write(outputStream);
		}

	}

	public static String[] splitString(String file) {

		String data[] = new String[9];

		System.out.println("--------------------------");

		data[0] = "0";

		String title = "//question_title";
		int question_title1 = file.indexOf(title);
		int question_title2 = file.lastIndexOf(title);

		String question_title = file.substring(question_title1 + title.length(), question_title2);
		System.out.println(question_title);
		data[1] = question_title;

		String description = "//question_description";
		int question_description1 = file.indexOf(description);
		int question_description2 = file.lastIndexOf(description);

		String question_description = file.substring(question_description1 + description.length(),
				question_description2);
		System.out.println(question_description);

		data[2] = question_description;

		String option_1 = "//option_1";
		int option_1_1 = file.indexOf(option_1);
		int option_1_2 = file.lastIndexOf(option_1);

		String opt_1 = file.substring(option_1_1 + option_1.length(), option_1_2);
		System.out.println(opt_1);
		data[3] = opt_1;

		String option_2 = "//option_2";
		int option_2_1 = file.indexOf(option_2);
		int option_2_2 = file.lastIndexOf(option_2);

		String opt_2 = file.substring(option_2_1 + option_2.length(), option_2_2);
		System.out.println(opt_2);
		data[4] = opt_2;

		String option_3 = "//option_3";
		int option_3_1 = file.indexOf(option_3);
		int option_3_2 = file.lastIndexOf(option_3);

		String opt_3 = file.substring(option_3_1 + option_3.length(), option_3_2);
		System.out.println(opt_3);
		data[5] = opt_3;

		String option_4 = "//option_4";
		int option_4_1 = file.indexOf(option_4);
		int option_4_2 = file.lastIndexOf(option_4);

		String opt_4 = file.substring(option_4_1 + option_4.length(), option_4_2);
		System.out.println(opt_4);
		data[6] = opt_4;

		String correct_answers = "//correct_answers";
		int correct_answers_1 = file.indexOf(correct_answers);
		int correct_answers_2 = file.lastIndexOf(correct_answers);

		String corr_answers = file.substring(correct_answers_1 + correct_answers.length(), correct_answers_2);
		System.out.println(corr_answers);
		data[7] = corr_answers;

		String solution = "//solution";
		int solution_1 = file.indexOf(solution);
		int solution_2 = file.lastIndexOf(solution);

		String solu = file.substring(solution_1 + solution.length(), solution_2);
		System.out.println(solu);
		data[8] = solu;

		return data;

	}

	public static void writeDataLineByLine(String filePath, String[] data) {
		// first create file object for file placed at location
		// specified by filepath
		File file = new File(filePath);
		try {
			// create FileWriter object with file as parameter
			FileWriter outputfile = new FileWriter(file);

			// create CSVWriter object filewriter object as parameter
			CSVWriter writer = new CSVWriter(outputfile);

			// no_of_questions question_title question_description option_1 option_2
			// option_3 option_4 correct_answers solution
			// adding header to csv
			String[] header = { "no_of_questions", "question_title", "question_description", "option_1", "option_2",
					"option_3", "option_4", "correct_answers", "solution" };
			// writer.writeNext(header);

			// add data to csv
			String[] data1 = { "no_of_questions", "question_title", "question_description", "option_1", "option_2",
					"option_3", "option_4", "correct_answers", "solution" };
			System.out.println(data.length);

			for (int i = 0; i < data.length; i++) {
				System.out.println(data[i]);
			}

			writer.writeNext(data);
//			String[] data2 = { "no_of_questions", "question_title", "question_description", "option_1", "option_2",
//					"option_3", "option_4", "correct_answers", "solution" };
//			writer.writeNext(data2);

			// closing writer connection
			writer.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
