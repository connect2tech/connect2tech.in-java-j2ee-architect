package in.connect2tech.msexcel.utils;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

public class ExcelReader {

	public static void main(String[] args) {
		String FILE_NAME = "Excel.xlsx";
		readExcelWithEmptyCells(FILE_NAME);
	}

	public static void readExcelWithEmptyCells(String FILE_NAME) {

		try {

			FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet sheet = workbook.getSheetAt(0);
			
			System.out.println(sheet.getFirstRowNum());

			/*for (int rn = sheet.getFirstRowNum(); rn <= sheet.getLastRowNum(); rn++) {
				Row row = sheet.getRow(rn);
				if (row == null) {
					// There is no data in this row, handle as needed
				} else {
					// Row "rn" has data
					for (int cn = 0; cn < row.getLastCellNum(); cn++) {
						Cell cell = row.getCell(cn);
						if (cell == null) {
							// This cell is empty/blank/un-used, handle as needed
							System.out.println("cell is empty!!!");
						} else {
							// String cellStr = fmt.formatCell(cell);
							// Do something with the value
							System.out.println("cell==>" + cell);
						}
					}
				}
			}*/
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}