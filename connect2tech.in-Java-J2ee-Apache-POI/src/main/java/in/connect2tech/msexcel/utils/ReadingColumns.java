package in.connect2tech.msexcel.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadingColumns {

	private static final String FILE_NAME = "InputData.xlsx";

	public static void main(String[] args) throws IOException {

		List<String> columns = new ArrayList<String>();
		List<String> keyVal = new ArrayList<String>();

		try {

			String col1 = "Key";
			String col2 = "Value";

			FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();

			int rowNum = 0;

			while (iterator.hasNext()) {

				Row currentRow = iterator.next();
				Iterator<Cell> cellIterator = currentRow.iterator();

				String value1 = null;
				String value2 = null;

				while (cellIterator.hasNext()) {

					Cell currentCell = cellIterator.next();
					if (rowNum == 0) {
						columns.add(currentCell.toString());
					} else {

						Object val1 = currentRow.getCell(columns.indexOf(col1));
						Object val2 = currentRow.getCell(columns.indexOf(col2));

						if (val1 != null) {
							value1 = val1.toString();
						}

						if (val2 != null) {
							value2 = val2.toString();
						}

					}
				}
				if (rowNum > 0) {
					String props = value1 + "=" + value2;
					System.out.println(props);
					keyVal.add(props);
				}
				++rowNum;
			}
			// System.out.println(columns);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		writeToPropertiesFile(keyVal);
	}

	public static void writeToPropertiesFile(List<String> columns) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter("props.properties", true));

		for (int i = 0; i < columns.size(); i++) {
			writer.append(columns.get(i));
			writer.append("\r\n");
		}

		writer.close();
	}
}