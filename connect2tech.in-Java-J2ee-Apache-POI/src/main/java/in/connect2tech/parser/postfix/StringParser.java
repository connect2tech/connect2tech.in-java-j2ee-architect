package in.connect2tech.parser.postfix;


/**
 * This is StringParser
 * @author 794827
 *
 */
public class StringParser {

	/**
	 * If Parser 
	 * @param rule : String value
	 */
	public static void ifParser(String rule) {
		// validate ()
		// validate {}
		// multiple assignment must be separated by commas

		int start = rule.indexOf("(");
		int end = rule.indexOf(")");

		String expression = rule.substring(start + 1, end);
		System.out.println(expression);

		String[] operator_operand = expression.split(" ");

		for (int i = 0; i < operator_operand.length; i++) {
			System.out.println(operator_operand[i]);
		}

		switch (operator_operand[1].toUpperCase()) {
		case "EQUALS":
			if (operator_operand[0].startsWith("str") && operator_operand[2].startsWith("str")) {
				boolean string_equality = stringEquality(operator_operand[0], operator_operand[2]);
				System.out.println("string_equality===>" + string_equality);
			}
		}

	}

	/**
	 * This method will take care of following conditions
	 * <ul>
	 * <li>equal :: if equal(val1, val2, string) : Compare val1, val2 for string
	 * equality</li>
	 * <li>equals:: if equals(val1, val2, string) : Compare val1, val2 for string
	 * equality</li>
	 * <li>EQUALS :: if EQUALS(val1, val2, string) : Compare val1, val2 for string
	 * equality</li>
	 * <li>EQUAL:: if EQUAL(val1, val2, string) : Compare val1, val2 for string
	 * equality</li>
	 * </ul>
	 * 
	 * @param str1 string value
	 * @param str2 string value
	 * @return boolean
	 */
	public static boolean stringEquality(String str1, String str2) {

		str1 = str1.substring(3);
		System.out.println("str1==========================>" + str1);
		str2 = str2.substring(3);

		return str1.equalsIgnoreCase(str2);
	}

	public static void main(String[] args) {
		String rule_0 = "if (str:string1 equals str:string2){str:name='naresh'}";
		// equal, EQUAL, EQUALS
		String rule_1 = "if equals(val1, val2, string)";
		// notequal, not-equal, not_equal
		String rule_2 = "if notequals(val1, val2, string)";
		StringParser.ifParser(rule_0);
	}

}
