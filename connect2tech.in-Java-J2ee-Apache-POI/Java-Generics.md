# connect2tech.in-jjs-CoreJava

# Generics

## SCJP 1.6 Kathy Sierra


**The Coding Flow**
- in.connect2tech.java5.generics.scjp.TestLegacyMixingGenericNonGeneric


---

**Concepts**

- Declaring a List with a type parameter of <Object> makes a collection that works in almost the same way as the original pre-Java 5, non-generic collection—you can put ANY Object type into the collection. You'll see a little later that non-generic collections and collections of type <Object> aren't entirely the same, but most of the time the differences do not matter.

- In fact the compiler will warn you that you're taking a big, big risk sending your nice protected ArrayList<Integer> into a dangerous method that can have its way with your list and put in Floats, Strings, or even Dogs. 
```
in.connect2tech.java5.generics.scjp.TestLegacyMixingGenericNonGeneric
```

- Back to our example with the legacy code that does an insert, keep in mind that for BOTH versions of the insert() method (one that adds an Integer and one that adds a String) the compiler issues warnings. The compiler does NOT know whether the insert() method is adding the right thing (Integer) or wrong thing (String). The reason the compiler produces a warning is because the method is ADDING something to the collection! In other words, the compiler knows there's a chance the method might add the wrong thing to a collection the caller thinks is type safe. 

- There's one Big Truth you need to know to understand why it runs without problems—the JVM has no idea that your ArrayList was supposed to hold only Integers. The typing information does not exist at runtime! All your generic code is strictly for the compiler. Through a process called "type erasure," the compiler does all of its verifications on your generic code and then strips the type information out of the class bytecode. At runtime, ALL collection code—both legacy and new Java 5 code you write using generics—looks exactly like the pre-generic version of collections. None of your typing information exists at runtime. 

- This is a little different from arrays, which give you BOTH compile-time protection and runtime protection. Why did they do generics this way? Why is there no type information at runtime? To support legacy code. At runtime, collections are collections just like the old days. What you gain from using generics is compile time protection that guarantees that you won't put the wrong thing into a typed collection, and it also eliminates the need for a cast when you get something out, since the compiler already knows that only an Integer is coming out of an Integer list. 

```
public class ArrayStoreExceptionExample {
	public static void main(String args[]) {
		
		// Since Double class extends Number class
		// only Double type numbers
		// can be stored in this array
		Number[] a = new Double[2];
		// Trying to store an integer value
		// in this Double type array. It will not work
		a[0] = new Integer(4);
		
		//This works as you have created place to store numbers.
		Number[] b = new Number[2];
		b[0] = new Integer(10);
		
		System.out.println("If it mixed array, then it is allowed!!!");
	}
}
```

- Just remember that the moment you turn that type safe collection over to older, non-type safe code, your protection vanishes. 

**This works** 

```
List<Integer> myList = new ArrayList<Integer>(); 
List myList2 = myList; 
//How else it will be compatible with non-generics?
```

--- 

### Polymorphism and Generics 

You've already seen that polymorphism applies to the "base" type of the collection: 

```
List<Integer> myList = new ArrayList<Integer>(); - Works 
List<Parent> myList = new ArrayList<Child>(); - Does not work. 
List<JButton> myList = new ArrayList<JButton>(); 
```

For now, just burn it into your brain that polymorphism does not work the same way for generics as it does with arrays. 

### Generics Methods 

**Does not work** 

```
public void checkAnimalsGeneric(List<Animal> animals) {...} 
doc.checkAnimalsGeneric(dogs); // send a List<Dog> 
```

**Works** 

```
Animal[] animals = new Animal[3];
animals[0] = new Cat();
animals[1] = new Dog();

List<Animal> animals = new ArrayList<Animal>(); 
animals.add(new Cat()); // OK 
animals.add(new Dog()); // OK 
```
 
- So this part works with both arrays and generic collections—we can add an instance of a subtype into an array or collection declared with a supertype. You can add Dogs and Cats to an Animal array (Animal[]) or an Animal collection (ArrayList<Animal>). 

- The reason the compiler won't let you pass an ArrayList<Dog> into a method that takes an ArrayList<Animal>, is because within the method, that parameter is of type ArrayList<Animal>, and that means you could put any kind of Animal into it. There would be no way for the compiler to stop you from putting a Dog into a List that was originally declared as <Cat>, but is now referenced from the <Animal> parameter. 

- The reason you can get away with compiling this for arrays is because there is a runtime exception (ArrayStoreException) that will prevent you from putting the wrong type of object into an array. If you send a Dog array into the method that takes an Animal array, and you add only Dogs (including Dog subtypes, of course) into the array now referenced by Animal, no problem. But if you DO try to add a Cat to the object that is actually a Dog array, you'll get the exception. 

- But there IS no equivalent exception for generics, because of type erasure! In other words, at runtime the JVM KNOWS the type of arrays, but does NOT know the type of a collection. All the generic type information is removed during compilation, so by the time it gets to the JVM, there is simply no way to recognize the disaster of putting a Cat into an ArrayList<Dog> and vice versa (and it becomes exactly like the problems you have when you use legacy, non-type safe code). 

- It's the reference type, not the actual object type that matters 

- Hey, I'm using the collection passed in just to invoke methods on the elements—and I promise not to ADD anything into the collection." And there IS a mechanism to tell the compiler that you can take any generic subtype of the declared argument type because you won't be putting anything in the collection. And that mechanism is the wildcard <?>. 

```
public void addAnimal(List<Animal> animals) 
to 
public void addAnimal(List<? extends Animal> animals) 
```
 
**This is allowed**

```
public void checkAnimalsGeneric(List<Animal> animals) { 
	//This is conscious decision to mix them. 
	animals.add(new Dog()); 
	animals.add(new Cat()); 
} 
```

- By saying <? extends Animal>, we're saying, "I can be assigned a collection that is a subtype of List and typed for <Animal> or anything that extends Animal. And oh yes, I SWEAR that I will not ADD anything into the collection." (There's a little more to the story, but we'll get there.) 

```
public void addAnimal(List<? extends Animal> animals) { 
	animals.add(new Dog()); // NO! Can't add if we 
} 
```
 
- First, the <? extends Animal> means that you can take any subtype of Animal; however, that subtype can be EITHER a subclass of a class (abstract or concrete) OR a type that implements the interface after the word extends. In other words, the keyword extends in the context of a wildcard represents BOTH subclasses and interface implementations.  

- There is no <? implements Serializable> syntax. If you want to declare a method that takes anything that is of a type that implements Serializable, you'd still use extends like this: 

```
void foo(List<? extends Serializable> list){ 
	// odd, but correct 
	// to use "extends" 
}
```

```
public void addAnimal(List<? super Dog> animals) { 
	animals.add(new Dog()); // adding is sometimes OK with super 
} 
```

- Hey compiler, please accept any List with a generic type that is of type Dog, or a supertype of Dog. Nothing lower in the inheritance tree can come in, but anything higher than Dog is OK." 

``` 
public class TestWildcards {
	public static void main(String[] args) {
		List<Integer> myList = new ArrayList<Integer>();
		Bar bar = new Bar();
		bar.doInsert(myList);
	}
}

class Bar {
	void doInsert(List<?> list) {
		list.add(new Dogs());
		System.out.println("Inside doInsert()");
	}
}
```

- The problem is in the list.add() method within doInsert(). The <?>
wildcard allows a list of ANY type to be passed to the method, but the add()
method is not valid, for the reasons we explored earlier (that you could put the
wrong kind of thing into the collection). So this time, the TestWildcards class is
fine, but the Bar class won't compile because it does an add() in a method that uses
a wildcard (without super). What if we change the doInsert() method to this:
 

- **By the way, ```List<? extends Object>``` and ```List<?>``` are absolutely identical!**

It means you cannot do the following: i.e cannot add Dog

```
class Bar {
	void doInsert(List<?> list) {
		list.add(new Dogs()); // Compile time error.
		list.add(null); // Ok.
		System.out.println("Inside doInsert()");
	}
}
```

```List<?> foo = new ArrayList<? extends Animal>();```
- you cannot use wildcard notation in the object creation. So the will not compile.


### Generic Declarations

**Creating Generic Methods**

```
class CreateAnArrayList {
	public <T> void makeArrayList(T t) {
		List<T> list = new ArrayList<T>();
		list.add(t);
	}
}
```

- The strangest thing about generic methods is that you must declare the type
variable BEFORE the return type of the method:

```public <T> void makeArrayList(T t)```

- The <T> before void simply defines what T is before you use it as a type in the
argument. You MUST declare the type like that unless the type is specified for the
class. In CreateAnArrayList, the class is not generic, so there's no type parameter
placeholder we can use.

- You're also free to put boundaries on the type you declare, for example, if you
want to restrict the makeArrayList() method to only Number or its subtypes
(Integer, Float, and so on) you would say

```public <T extends Number> void makeArrayList(T t)```


--- 

## Pluralsight

[Pluralsight](https://app.pluralsight.com/library/courses/java-generics/table-of-contents)

- Using Comparable
```
public class Student implements Comparable<Student>{
...
}
```

- Using Comparator
```
public class AgeComparator implements Comparator<Employee> {
...
}
```

- Reverse Comparator
```
public class ReverseComparator<T> implements Comparator<T> {
	Comparator<T> delegateComparator;
	ReverseComparator(Comparator<T> delegateComparator) {
		this.delegateComparator = delegateComparator;
	}
	@Override
	public int compare(T left, T right) {
		return -1 * delegateComparator.compare(left, right);
	}
}
```

**Type Bound**

```
public class SortedPairTypeBounds<T extends Comparable<T>> {
...
}
```

**Generic Method**

```
static <T> void genericDisplay(T element) {
...
}
```

**Upper Bounded**

```
public void saveAll(List <? extends Person> person){
}

Or.

public <? extends Person> void saveAll(List <T> person){
}
```

**This works** 

```
	List l = new ArrayList();
	l.add("hello");
	l.add(10);
	List <String> lString = l; 
```

- We can assign raw type to generic and generic type to raw type.