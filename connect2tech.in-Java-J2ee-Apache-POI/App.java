package com.example.excel_manipulation;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 *Program reads data from excel sheet colums key and value = col index 0 and 1 respectively
 *Stores unique key value pairs in a map
 */
public class App 
{
	//Excel sheet which stores input data
	private static final String FILE_NAME = "E:\\SFTRAutomationFrameworkDesignCognizant\\excel_manipulation\\InputData.xlsx"; 
	
	//Map to store unique Key,Value pairs from excel
	private static Map< String,String> hm =  new HashMap< String,String>(); 
	
	
    public static void main( String[] args )
    { 
    	String key="",value="";
    	Row row=null;Cell cell=null;
    	int rowcount=0;
    	try { 
    		InputStream inp = new FileInputStream(FILE_NAME); 
    	    Workbook wb = WorkbookFactory.create(inp); 
    	    Sheet sheet = wb.getSheetAt(0); 
    	  
    	    // Iterate through each rows one by one 
            Iterator<Row> rowIterator = sheet.iterator(); 
            ArrayList<String> colNames = new ArrayList<String>();
            int colKeyIndex=0,colValueIndex=0;
          
            while (rowIterator.hasNext()) { 
            		
             row = rowIterator.next();
             
             if(rowcount==0) {
             //first row data = column names 
             //Fetch indexes of required columns
             int colCount = sheet.getRow(0).getLastCellNum();
             
           
             for(int i=0;i<colCount;i++) {
            	 cell=row.getCell(i);
            	 colNames.add(cell.getStringCellValue());
            	
             }
             
             //Provide column names for which you want to extract data
             colKeyIndex=colNames.indexOf("Key");
             colValueIndex=colNames.indexOf("Value");
         
             }
           
             if(rowcount>0) //condition added to skip title heading row
             {
            	 //store keys from column Key 
            	 if(isCellEmpty(row.getCell(colKeyIndex)))
         	     {
         	    	key="null";
         	     }
         	    else {
         	        cell=row.getCell(colKeyIndex); //getting the cell representing the given column  
         	        key=cell.getStringCellValue();    //getting cell value  
         	    
         	     }
            	 
            	 //store values from column Value - col index =1
            	 if(isCellEmpty(row.getCell(colValueIndex)))
         	     {
         	    	value="null";
         	     }
         	    else {
         	        cell=row.getCell(colValueIndex); //getting the cell representing the given column  
         	        value=cell.getStringCellValue();    //getting cell value  
         	    
         	    }
            	 
            	//Store unique key value pair in map
            	if(!key.equals("null") && !value.equals("null"))
            	{
            		if(hm.containsKey(key)==false) {
            			hm.put(key, value);
            		}
            	}
            	
             }
            
             rowcount++;
          
            }//one row rendered
    	    
            //Printing the result -- hashmap hm stores unique key value pairs
            System.out.println("*****************\n\tRESULT=\n\tUnique key-value pairs are =\n\t"+hm);
    	    
    	    inp.close(); 
        } 
        catch (Exception e) { 
        	e.printStackTrace(); 
        } 
    }//end main
    
    public static boolean isCellEmpty(final Cell cell) {
        if (cell == null) { 
            return true;
        }

        if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
            return true;
        }

        if (cell.getCellType() == Cell.CELL_TYPE_STRING && cell.getStringCellValue().trim().isEmpty()) {
            return true;
        }

        return false;
    }
}

