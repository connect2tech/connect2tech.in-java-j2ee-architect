//question_title
#include <iostream>
using namespace std;
template<class T> 
T cube(T a)
{
    T result;
    result = a * a * a;
    return result;
};
int main()
{
    int i, j;
    float x, y;
    i = 3;
    x = 3.3;
    j = cube(i);
    cout <<"Cube of "<< i << " = "  << j << endl;
    y = cube(x);
    cout <<"Cube of "<< x << " = "  << y << endl;
}
//question_title

/*

//question_description
Predict the output.
//question_description

//option_1
A) Cube of 3 = 27
   Cube of 3.3 = 35.937
//option_1

//option_2
B) Cube of 3 = 27
//option_2

//option_3
C) Cube of 3.3 = 27
//option_3

//option_4
D) Error
//option_4

//correct_answers
Answer: A)Cube of 3 = 27
          Cube of 3.3 = 35.937
//correct_answers		  

//solution
Explanation: Both the values 3 and 3.3 are are sent as parameters to cube() and their respective cubes are calculated using function templates.
//solution
*/   