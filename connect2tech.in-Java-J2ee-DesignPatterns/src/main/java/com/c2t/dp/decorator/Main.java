package com.c2t.dp.decorator;

public class Main {
	public static void main(String[] args) {
		Food f = new VegFood();
		/*System.out.println(f.prepareFood());
		System.out.println(f.foodPrice());*/
		
		Food f2 = new NonVegFood2(f);
		System.out.println(f2.prepareFood());
	}
}
