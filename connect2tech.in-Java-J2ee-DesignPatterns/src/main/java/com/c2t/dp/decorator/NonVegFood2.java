package com.c2t.dp.decorator;

public class NonVegFood2 implements Food {
	
	Food f;
	
	NonVegFood2(Food vegFood){
		f = vegFood;
	}

	public String prepareFood() {
		// TODO Auto-generated method stub
		String nonVeg = f.prepareFood() + " Add chicken curry";
		return nonVeg;
	}

	public double foodPrice() {
		// TODO Auto-generated method stub
		double newPrice = f.foodPrice() + 25.5d;
		return newPrice;
	}
	
	
}