package com.c2t.hf.strategy;

public interface QuackBehavior {
	public void quack();
}
