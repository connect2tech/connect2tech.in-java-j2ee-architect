package com.c2t.hf.factory.factory;

public interface Sauce {
	public String toString();
}
