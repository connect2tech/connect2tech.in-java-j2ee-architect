package com.c2t.oodesigns.strategy;

public interface IBehaviour {
	public int moveCommand();
}
