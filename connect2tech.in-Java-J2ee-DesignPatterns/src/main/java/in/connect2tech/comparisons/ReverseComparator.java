package in.connect2tech.comparisons;

import java.util.Comparator;

public class ReverseComparator<T> implements Comparator<T> {

	Comparator<T> delegateComparator;

	ReverseComparator(Comparator<T> delegateComparator) {
		this.delegateComparator = delegateComparator;
	}

	@Override
	public int compare(T left, T right) {
		return -1 * delegateComparator.compare(left, right);
	}

}
