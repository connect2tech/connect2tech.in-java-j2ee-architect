package in.connect2tech.comparisons;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class JavaObjectSortingArrays {

	/**
	 * This class shows how to sort primitive arrays, Wrapper classes Object
	 * Arrays
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// sort primitives array like int array
		int[] intArr = { 5, 9, 1, 10 };
		Arrays.sort(intArr);
		System.out.println(Arrays.toString(intArr));

		// sorting String array
		String[] strArr = { "A", "C", "B", "Z", "E" };
		Arrays.sort(strArr);
		System.out.println(Arrays.toString(strArr));
		
		int arr2 [] = {10,5,2,1};
		Arrays.sort(arr2);
		System.out.println(Arrays.toString(arr2));

		// sorting list of objects of Wrapper classes
		List<String> strList = new ArrayList<String>();
		strList.add("A");
		strList.add("C");
		strList.add("B");
		strList.add("Z");
		strList.add("E");
		Collections.sort(strList);
		for (String str : strList)
			System.out.print(" " + str);
	}
}