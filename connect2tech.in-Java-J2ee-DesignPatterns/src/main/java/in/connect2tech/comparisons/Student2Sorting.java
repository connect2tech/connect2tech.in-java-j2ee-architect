package in.connect2tech.comparisons;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Student2Sorting {

	public static void main(String[] args) {
		// int id, String name, int age, int salary
		Student2 s1 = new Student2(12, "name", 20, 100);
		Student2 s2 = new Student2(11, "name", 21, 101);

		List<Student2> l = new ArrayList<>();
		l.add(s1);
		l.add(s2);

		System.out.println("---------------->"+l);

		Collections.sort(l);

		System.out.println("---------------->"+l);
	}
}