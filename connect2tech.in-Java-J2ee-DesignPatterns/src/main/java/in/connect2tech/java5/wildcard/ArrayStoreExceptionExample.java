package in.connect2tech.java5.wildcard;

public class ArrayStoreExceptionExample {
	public static void main(String args[]) {
		
		// Since Double class extends Number class
		// only Double type numbers
		// can be stored in this array
		Number[] a = {10,10.5};

		// Trying to store an integer value
		// in this Double type array
		a[0] = new Integer(4);
		
		System.out.println("If it mixed array, then it is allowed!!!");
	}
}
