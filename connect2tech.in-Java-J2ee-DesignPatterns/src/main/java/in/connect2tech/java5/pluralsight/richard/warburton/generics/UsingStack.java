package in.connect2tech.java5.pluralsight.richard.warburton.generics;

public class UsingStack {
	public static void main(String args[]) {
		Stack<String> s = new Stack<String>();
		System.out.println(s.pop() + " Popped from stack");
	}
}