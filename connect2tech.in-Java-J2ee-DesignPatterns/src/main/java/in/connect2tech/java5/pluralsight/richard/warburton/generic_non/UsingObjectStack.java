package in.connect2tech.java5.pluralsight.richard.warburton.generic_non;

public class UsingObjectStack {
	public static void main(String args[]) {
		ObjectStack s = new ObjectStack();
		s.push(10);
		s.push(20);
		s.push(30);
		System.out.println(s.pop() + " Popped from stack");
	}
}