package in.connect2tech.dp.pluralsight.singleton;

public class LoggerFile {

	static LoggerFile logger = new LoggerFile();
	
	private LoggerFile() {
	}

	static LoggerFile getLogger() {
		return logger;
	}
	
	public static void doLogging(){
		
	}
	
}
