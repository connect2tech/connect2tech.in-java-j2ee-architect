package in.connect2tech.dp.pluralsight.singleton;

public class MySingleton {

	private static MySingleton mySingleton;

	private MySingleton() {
	}

	/**
	 * This is lazy loading concept.
	 * 
	 * @return
	 */
	public static MySingleton getSingleton() {

		if (mySingleton == null) {
			mySingleton = new MySingleton();
			return mySingleton;
		} else {
			return mySingleton;
		}

	}

}
