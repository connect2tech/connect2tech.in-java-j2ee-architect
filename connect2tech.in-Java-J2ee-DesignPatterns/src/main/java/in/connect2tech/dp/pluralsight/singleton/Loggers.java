package in.connect2tech.dp.pluralsight.singleton;

public class Loggers {
	
	private static Loggers log = new Loggers();
	
	private Loggers(){
	}
	
	public static Loggers getLogger(){
		return log;
	}
}
