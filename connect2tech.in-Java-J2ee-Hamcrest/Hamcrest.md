
https://www.vogella.com/tutorials/Hamcrest/article.html#hamcrest_installation

## Purpose of the Hamcrest matcher framework

Hamcrest is a framework for software tests. Hamcrest allows checking for conditions in your code via existing matchers classes. It also allows you to define your custom matcher implementations.

Hamcrest is typically viewed as a third generation matcher framework. The first generation used assert(logical statement) but such tests were not easily readable. The second generation introduced special methods for assertions, e.g., assertEquals(). This approach leads to lots of assert methods. Hamcrest uses assertThat method with a matcher expression to determine if the test was succesful.

Hamcrest has the target to make tests as readable as possible. For example, the is method is a thin wrapper for equalTo(value).

## Using Hamcrest

allOf - matches if all matchers match (short circuits)

anyOf - matches if any matchers match (short circuits)

not - matches if the wrapped matcher doesn’t match and vice

equalTo - test object equality using the equals method

is - decorator for equalTo to improve readability

hasToString - test Object.toString

instanceOf, isCompatibleType - test type

notNullValue, nullValue - test for null

sameInstance - test object identity

hasEntry, hasKey, hasValue - test a map contains an entry, key or value

hasItem, hasItems - test a collection contains elements

hasItemInArray - test an array contains an element

closeTo - test floating point values are close to a given value

greaterThan, greaterThanOrEqualTo, lessThan, lessThanOrEqualTo

equalToIgnoringCase - test string equality ignoring case

equalToIgnoringWhiteSpace - test string equality ignoring differences in runs of whitespace

containsString, endsWith, startsWith - test string matching

---

4. Exercise - Using Hamcrests built-in matchers
5. Exercise - Writing a custom Hamcrest matcher using FeatureMatcher
6. Exercise: Writing your custom Hamcrest matcher using TypeSafeMatcher
7. Exercise: Combining matchers
8. Grouping your matchers for import
9. Hamcrest resources
10. vogella training and consulting support
Appendix A: Copyright, License and Source code