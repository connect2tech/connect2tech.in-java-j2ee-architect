package in.connect2tech.vogella.hamcrest;

import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.hamcrest.text.IsEmptyString.isEmptyString;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

public class StringMatcherTest {
	@Test
	public void isStringEmpty() {
		String stringToTest = "";
		assertThat(stringToTest, isEmptyString());
	}

	@Test
	public void isStringEmptyOfNull() {
		String stringToTest = "";
		assertThat(stringToTest, isEmptyOrNullString());
	}

}
