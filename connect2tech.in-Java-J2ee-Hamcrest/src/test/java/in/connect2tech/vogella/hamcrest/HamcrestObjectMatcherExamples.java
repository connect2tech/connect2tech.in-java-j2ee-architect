package in.connect2tech.vogella.hamcrest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.samePropertyValuesAs;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

public class HamcrestObjectMatcherExamples {

	@Test
	public void objectHasSummaryProperty() {
		Todo todo = new Todo(1, "Learn Hamcrest", "Important");
		assertThat(todo, hasProperty("summary"));
	}

	@Test
	public void objectHasCorrectSummaryValue() {
		Todo todo = new Todo(1, "i am summary", "i am description");

		Map<String, String> expectedValues = new HashMap();
		expectedValues.put("summary", "i am summary");
		//expectedValues.put("description", "i am description");

		Set<String> keys = expectedValues.keySet();

		Iterator<String> iter = keys.iterator();

		while (iter.hasNext()) {
			
			String key = iter.next();
			String value = expectedValues.get(key);
			assertThat(todo, hasProperty(key, equalTo(value)));
		}

	}

	@Test
	public void objectHasSameProperties() {
		Todo todo1 = new Todo(1, "Learn Hamcrest", "Important");
		Todo todo2 = new Todo(1, "Learn Hamcrest", "Important");
		assertThat(todo1, samePropertyValuesAs(todo2));
	}
}