package in.connect2tech.vogella.hamcrest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.arrayWithSize;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.hamcrest.Matcher;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Test;

public class HamcrestListMatcherExamples {
	@Test
	public void listShouldInitiallyBeEmpty() {
		List<Integer> list = Arrays.asList(5, 2, 4);

		assertThat(list, hasSize(3));
		// ensure the order is correct
		assertThat(list, contains(5, 2, 4));
		assertThat(list, containsInAnyOrder(2, 4, 5));
		assertThat(list, everyItem(greaterThan(1)));

	}

	public static <T> void assertThat1(T actual, Matcher<? super T> matcher) {
		// assertThat("", actual, matcher);
	}

	public static <E> Matcher<Collection<? extends E>> hasSize1(int size) {
		Matcher<? super Integer> matcher = equalTo(size);
		return IsCollectionWithSize.<E>hasSize(matcher);
	}

	@Test
	public void arrayHasSizeOf4() {
		Integer[] ints = new Integer[] { 7, 5, 12, 16 };
		assertThat(ints, arrayWithSize(4));
		assertThat(ints, arrayContaining(7, 5, 12, 16));
	}

}